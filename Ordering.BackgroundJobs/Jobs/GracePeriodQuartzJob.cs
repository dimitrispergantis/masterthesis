﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Ordering.BackgroundJobs.Configuration;
using Ordering.BackgroundJobs.IntegrationEvents;
using Quartz;
using RabbitMQEventBus.Interfaces;

namespace Ordering.BackgroundJobs.Jobs
{
    [DisallowConcurrentExecution]
    public class GracePeriodQuartzJob: IJob
    {
        private readonly ILogger<GracePeriodQuartzJob> _logger;
        private readonly BackgroundTaskSettings _settings;
        private readonly IEventBus _eventBus;

        public GracePeriodQuartzJob(ILogger<GracePeriodQuartzJob> logger, IOptions<BackgroundTaskSettings> settings, IEventBus eventBus)
        {
            _logger = logger;
            _settings = settings?.Value;
            _eventBus = eventBus;
        }


        public async Task Execute(IJobExecutionContext context)
        {
            _logger.LogDebug("GracePeriodManagerService is starting.");

            context.CancellationToken.Register(() => _logger.LogDebug("#1 GracePeriodManagerService background task is stopping."));

            while (!context.CancellationToken.IsCancellationRequested)
            {
                _logger.LogDebug("GracePeriodManagerService background task is doing background work.");

                CheckConfirmedGracePeriodOrders();

                await Task.Delay(_settings.CheckUpdateTime, context.CancellationToken);
            }

            _logger.LogDebug("GracePeriodManagerService background task is stopping.");

            await Task.CompletedTask;
        }

        private void CheckConfirmedGracePeriodOrders()
        {
            _logger.LogDebug("Checking confirmed grace period orders");

            var orderIds = GetConfirmedGracePeriodOrders();

            foreach (var orderId in orderIds)
            {
                var confirmGracePeriodEvent = new GracePeriodConfirmedIntegrationEvent(orderId);

                _logger.LogInformation("----- Publishing integration event: {IntegrationEventId} from {AppName} - ({@IntegrationEvent})", confirmGracePeriodEvent.Id, Program.AppName, confirmGracePeriodEvent);

                _eventBus.Publish(confirmGracePeriodEvent);
            }
        }

        private IEnumerable<int> GetConfirmedGracePeriodOrders()
        {
            IEnumerable<int> orderIds = new List<int>();

            using (var conn = new SqlConnection(_settings.ConnectionString))
            {
                try
                {
                    conn.Open();
                    orderIds = conn.Query<int>(
                        @"SELECT Id FROM [dbo].[Orders] 
                            WHERE DATEDIFF(minute, [OrderDate], GETDATE()) >= @GracePeriodTime
                            AND [OrderStatusId] = 1",
                        new { GracePeriodTime = _settings.GracePeriodTime });
                }
                catch (SqlException exception)
                {
                    _logger.LogCritical(exception, "FATAL ERROR: Database connections could not be opened: {Message}", exception.Message);
                }

            }

            return orderIds;
        }
    }
}
