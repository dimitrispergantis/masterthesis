﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RabbitMQEventBus.Events;

namespace Payment.Notifications.IntegrationEvents.Events
{
    public class PaypalPaymentInitiatedIntegrationEvent: IntegrationEvent
    {
        public PaypalPaymentInitiatedIntegrationEvent(string paypalRedirect)
        {
            PaypalRedirect = paypalRedirect;
        }

        public string PaypalRedirect { get; }
    }
}
