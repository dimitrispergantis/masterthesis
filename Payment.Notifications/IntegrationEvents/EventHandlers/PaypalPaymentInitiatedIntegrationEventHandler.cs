﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Payment.Notifications.IntegrationEvents.Events;
using RabbitMQEventBus.Interfaces;
using Serilog.Context;

namespace Payment.Notifications.IntegrationEvents.EventHandlers
{
    public class PaypalPaymentInitiatedIntegrationEventHandler: IIntegrationEventHandler<PaypalPaymentInitiatedIntegrationEvent>
    {
        private readonly IHubContext<NotificationsHub> _hubContext;
        private readonly ILogger<PaypalPaymentInitiatedIntegrationEvent> _logger;

        public PaypalPaymentInitiatedIntegrationEventHandler(IHubContext<NotificationsHub> hubContext, ILogger<PaypalPaymentInitiatedIntegrationEvent> logger)
        {
            _hubContext = hubContext;
            _logger = logger;
        }

        public async Task Handle(PaypalPaymentInitiatedIntegrationEvent @event)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{@event.Id}-{Program.AppName}"))
            {
                _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, Program.AppName, @event);

                await _hubContext.Clients
                    .Group("PaypalPayment")
                    .SendAsync("PaypalRedirect", new { redirect = @event.PaypalRedirect });
            }
        }
    }
}
