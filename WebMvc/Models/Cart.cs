﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebMvc.ViewModels;

namespace WebMvc.Models
{
    public class Cart
    {
        public string CustomerId { get; set; }

        public Checkout Checkout { get; set; }

        public List<CartLine> LineCollection { get; set; } = new List<CartLine>();

        public Guid RequestId { get; set; }

        public byte PaymentType { get; set; }

        public virtual Task AddItem(int productId, int productVersionId, string productName, string photo, int quantity, float price)
        {
            CartLine line = LineCollection.SingleOrDefault(p => p.ProductId == productId && p.ProductVersionId == productVersionId);

            if (line == null)
            {
                LineCollection.Add(new CartLine {
                    ProductId = productId, ProductVersionId = productVersionId, ProductName = productName, PictureUrl = photo,
                    Quantity = quantity, Price = price
                });
            }
            else
            {
                line.Quantity += quantity;
            }

            return Task.CompletedTask;
        }

        public virtual Task UpdateItemQuantity(int productId, int productVersionId, int quantity)
        {
            CartLine line = LineCollection.SingleOrDefault(p => p.ProductId == productId && p.ProductVersionId == productVersionId);
            line.Quantity = quantity;

            return Task.CompletedTask;
        }

        public virtual Task RemoveItem(int productId, int productVersionId)
        {
            LineCollection.RemoveAll(l => l.ProductId == productId && l.ProductVersionId == productVersionId);

            return Task.CompletedTask;
        }

        public virtual float ComputeTotalValue() => LineCollection.Sum(e => e.Price  * e.Quantity) ;

        public virtual Task Clear()
        {
            LineCollection.Clear();

            return Task.CompletedTask;
        }

        public virtual Task UpdateCheckout(Checkout checkout)
        {
            Checkout = checkout;

            return Task.CompletedTask;
        }

    }
}
