﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebMvc.ViewModels
{
    public class Checkout
    {
        public string UserId { get; set; }

        [Display(Name = "Όνομα")]
        [Required(ErrorMessage = "Παρακαλώ συμπληρώστε το όνομα σας")]
        //[RegularExpression(@"^[Α-Ω]+[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώ""'\s-]*$", ErrorMessage = "Παρακάλω εισάγετε ενα εγκύρο όνομα")]
        public string FirstName { get; set; }

        [Display(Name = "Επίθετο")]
        [Required(ErrorMessage = "Παρακαλώ συμπληρώστε το επίθετο σας")]
        //[RegularExpression(@"^[Α-Ω]+[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώ""'\s-]*$", ErrorMessage = "Παρακάλω εισάγετε ενα εγκύρο επίθετο")]
        public string LastName { get; set; }

        [Display(Name = "Διεύθυνση e-mail")]
        [EmailAddress(ErrorMessage = "Παρακαλώ εισάγεται μια εγκύρη δίεύθυνση e-mail")]
        [Required(ErrorMessage = "Παρακαλώ συμπληρώστε το email επικοινωνίας σας")]
        public string Email { get; set; }

        [Display(Name = "Τηλέφωνο")]
        [Phone(ErrorMessage = "Παρακαλώ εισάγεται ενα εγκύρο τηλεφωνικό αριθμό")]
        [Required(ErrorMessage = "Παρακαλώ συμπληρώστε το τηλέφωνο επικοινωνίας σας")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Όνομα Επιχέιρησης")]
        public string CompanyName { get; set; }

        [Display(Name = "Όνομα Διεύθυνσης")]
        [Required(ErrorMessage = "Παρακαλώ συμπληρώστε την οδό και αριθμο της διευθυνσής αποστολής")]
        public string AddressName { get; set; }

        [Display(Name = "Επιπλέον Πληροφορίες")]
        public string AddressAdditionalInfo { get; set; }

        // Πολή
        [Display(Name = "Πόλη")]
        [Required(ErrorMessage = "Παρακαλώ συμπληρώστε την πόλη της διευθυνσής αποστολής")]
        public string AddressCity { get; set; }

        // Νομός
        [Display(Name = "Νομός")]
        [Required(ErrorMessage = "Παρακαλώ συμπληρώστε τον νόμο της διευθυνσής αποστολής")]
        public string AddressState { get; set; }

        // Χώρα
        [Display(Name = "Χώρα")]
        [Required(ErrorMessage = "Παρακαλώ εισάγετε την χώρα της διευθυνσής αποστολής")]
        public string AddressCountry { get; set; }

        // ΤΚ
        [Display(Name = "Ταχυδρομικός Κώδικας")]
        [Required(ErrorMessage = "Παρακαλώ εισάγετε τον ταχυδρομικό κώδικα της διεύθυνσης αποστολής")]
        public string ZipCode { get; set; }

        public virtual void SetCheckout(){}
    }
}
