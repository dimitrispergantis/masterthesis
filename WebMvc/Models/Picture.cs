﻿using System;

namespace WebMvc.Models
{
    public class Picture
    {
        public int Id { get; set; }

        public int PictureTypeId { get; set; }

        public string FullPictureUri { get; set; }

        public string ThumbnailPictureUri { get; set; }

        public const byte IsMainPhoto = 1;

        public const byte AreAdditionalPhotos = 2;

        public const byte AreDescriptionPhotos = 3;


    }
}
