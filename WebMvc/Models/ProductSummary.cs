﻿namespace WebMvc.Models
{
    public class ProductSummary
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CategoryTitle { get; set; }

        public string BrandTitle { get; set; }

        public bool IsDraft { get; set; }

        public bool IsBestProduct { get; set; }

        public int VersionsOnStock { get; set; }
    }
}
