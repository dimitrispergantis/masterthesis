﻿namespace WebMvc.Models
{
    public class ProductManufacturer
    {
        public int ProductManufacturerId { get; set; }
        public string Title { get; set; }
        public float? DiscountPercentage { get; set; }

    }
}
