﻿namespace WebMvc.Models
{
    public class CartLine
    {
        public int ProductId { get; set; }
        public int ProductVersionId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public float Price { get; set; }
        public string PictureUrl { get; set; }
    }
}
