﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMvc.Models
{
    public class ChildCategory
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}
