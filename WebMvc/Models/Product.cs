﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebMvc.Models
{
    public class Product
    {
        public int ProductId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string BulletDescription { get; set; }

        public int CategoryId { get; set; }

        public int BrandId{ get; set; }

        public bool IsBest { get; set; }

        public bool HasStock { get; set; }

        public List<Picture> Pictures { get; set; }

        public List<ProductVersion> Versions { get; set; }



    }
}
