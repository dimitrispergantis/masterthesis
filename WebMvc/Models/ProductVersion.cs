﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebMvc.Models
{
    public class ProductVersion
    {
        public int Id { get; set; }

        public string Specification { get; set; }

        public float InitialPrice { get; set; }

        public float Discount { get; set; }

        public bool HasStock { get; set; }

        public float GetPrice => (float) Math.Round(InitialPrice - (Discount * InitialPrice), 2);
    }
}
