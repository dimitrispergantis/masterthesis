﻿var slider = new Slider("#custom-slider",
    { tooltip: 'always', tooltip_position: 'bottom', range: true });

$(document).ready(function () {
    $('#pricebutton').on("click",
        function () {
           
            var query = $("#query").val();
            var minValue = slider.getValue()[0];
            var maxValue = slider.getValue()[1];
            var pageSize = $('#pageSize').find(":selected").val();
            var sort = $('#sort').find(":selected").val();

            if ($('input[name=category]:checked').attr("id") != null) {
                
                var id = $('input[name=category]:checked').attr("id").split('-')[1];
                window.location.replace("/Query?qString=" + query + "&categoryId=" + id + "&currentPriceMin=" + minValue + "&currentPriceMax=" + maxValue
                    + "&sortOrder=" + sort + "&pageSize=" + pageSize) ;
            } 
            else {
                window.location.replace("/Query?qString=" + query + "&currentPriceMin=" + minValue + "&currentPriceMax=" + maxValue + "&sortOrder=" + sort + "&pageSize=" + pageSize) ;
            }
           
        });

    $('#categoryDiv').on("click",
        ".js-category-item",
        function () {
            var id = $(this).attr("id").replace('category-', '');
            var query = $("#query").val();
            var pageSize = $('#pageSize').find(":selected").val();
            var sort = $('#sort').find(":selected").val();
            window.location.replace("/Query?qString=" + query +"&categoryId=" + id + "&sortOrder=" + sort + "&pageSize=" + pageSize) ;
        });
});