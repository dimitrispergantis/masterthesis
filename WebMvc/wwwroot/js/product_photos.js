﻿ $(document).ready(function() {
        $(document).on('click',
            '.delete-link',
            function() {
                event.preventDefault();
                var tr = $(this).closest('tr');

                tr.addClass("bg-danger");

                tr.fadeOut(500,
                    function() {
                        var table = tr.closest('table');
                        tr.remove();
                        updateIndexes(table);
                    });
            });

        $(document).on('click',
            '.delete-header-link',
            function() {
                event.preventDefault();
                var tr = $(this).closest('tr');

                tr.addClass("bg-danger");

                tr.fadeOut(500,
                    function() {
                        var table = tr.closest('table');
                        tr.remove();                       
                    });
                $("#btnAddPhotos").prop('disabled', false);
            });


        $("#btnAddPhotos").on("click",
            function(event) {
                event.preventDefault();
                addPhotosInput('photosTable');
                $("#btnAddPhotos").prop('disabled', true);
            });
    });

    function addPhotosInput(tableId) {
        var rowTemplate =
            '<tr>' +
                '<th colspan="3">' +
                    '<select class="form-control" data-val="true" data-val-required="Αυτο το πεδίο ειναι υποχρεωτικό." id="PhotoType" name="PhotoType" aria-describedby="PhotoType-error" class="valid" aria-invalid="false">'+
                        '<option disabled="" selected="">Επιλέξτε κατηγορία προϊοντος</option>'+
                        '<option value="1">Κεντρική Φωτογραφία</option>'+
                        '<option value="2">Φωτογραφία Gallery</option>'+
                        '<option value="3">Φωτογραφία Περιγραφής</option>'+
                    '</select>'+
                '</th>' +
                '<th colspan="1">' +
                    '<input name="formFiles" type="file" multiple id="FormFiles" required>' +
                '</th>' +
                '<th colspan="1">' +
                    '<a class="delete-header-link"> <i class="fa fa-times admin-icon" aria-hidden="true"></i></a>' +
                '</th>' +
            '</tr>';

        addRecord(tableId, rowTemplate);
    }

    // JQuery συνάρτηση για δυναμικο validation
    function fnValidateDynamicContent($element) {
        var $currForm = $element.closest("form");
        $currForm.removeData("validator");
        $currForm.removeData("unobtrusiveValidation");
        $.validator.unobtrusive.parse($currForm);
        $currForm.validate(); // This line is important and added for client side validation to trigger, without this it didn't fire client side errors.
    }


    function addRecord(tableId, rowTemplate) {
        var table = $("#" + tableId);
        var newRow = $(rowTemplate);
        newRow.hide();

        table.prepend(newRow);
        newRow.addClass("bg-success");
        newRow.fadeIn(500,
            function() {
                newRow.removeClass("bg-success");
            });
        fnValidateDynamicContent(newRow);
    }

    // This is where the magic happens
    function updateIndexes(table) {
        // get every tr element except the header
        table.find("tr:gt(0)").each(function(i, row) {
            // get every input and select elements
            $(row).find('input').each(function(j, input) {
                // check whether the id-attribute is of type _[index]__
                var id = input.id.match(/_\d+__/);

                // if it is an element necessary for the ModelBinder => update the name attribute
                if (id != null && id.length && id.length == 1) {
                    var attr = $(input).attr("name");
                    // replace the old index of the name attribute with the calculated index
                    var newName = attr.replace(attr.match(/\d+/), i);
                    $(input).attr("name", newName);
                }
            });         

        });
    }