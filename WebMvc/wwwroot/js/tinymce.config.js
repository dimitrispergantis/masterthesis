﻿tinymce.init({
    selector: 'textarea',
    toolbar: 'undo redo styleselect bold italic alignleft aligncenter alignright bullist numlist outdent indent code',
    plugins: 'code lists',
    entity_encoding: 'raw',
    //invalid_styles: 'background, color, font-size, font-family, border-color, background-color, max-width, max-height, width, height', 
    valid_styles: 'whitespace',
    extended_valid_elements: 'table',
    valid_classes: 'class001'
});
//By default, jquery validate ignores fields that are not visible. The default value for the ignore parameter is ':hidden'. 
//By setting it to an empty string, you are telling jquery validate to not ignore hidden inputs.
$.validator.setDefaults({
    ignore: ''
});
// sync tinymce content to validate before submitting form
$('form button[type=submit]').click(function () {
    tinymce.triggerSave();
});