$(document).ready(function(){


  (function ($) {
      $('.spinner .btn:first-of-type').on('click', function () {
          var p = $(this).closest(".spinner");
          $('input', p).val(parseInt($('input', p).val(), 10) - 1);
          if ($('input', p).val() <= 1 || !$.isNumeric($('input', p).val())) {
              $('input', p).val(1);
          };
      });
      $('.spinner .btn:last-of-type').on('click', function () {
          var p = $(this).closest(".spinner");
          $('input',p).val( parseInt($('input',p).val(), 10) + 1);
      });
      
    $(".quantity-text-box").on("blur",
        function () {
            if ($(this).val() <= 1 || !$.isNumeric($(this).val())) {
                $(this).val(1);
            };
        });
  })(jQuery);

  $(".user-dropdown a").click(function(){
    $('.user-dropdown-trigger').dropdown('toggle');
  });
  // currency dropdown
  $(".currency-dropdown label[for=dollarCurrency]").click(function(){
    $(".selected-currency").html("<i class='fa fa-dollar'></i>");
    $(".currency-trigger[data-toggle=dropdown]").dropdown("toggle");
  });
  $(".currency-dropdown label[for=euroCurrency]").click(function(){
    $(".selected-currency").html("<i class='fa fa-euro'></i>");
    $(".currency-trigger[data-toggle=dropdown]").dropdown("toggle");
  });
  $(".currency-dropdown label[for=yenCurrency]").click(function(){
    $(".selected-currency").html("<i class='fa fa-yen'></i>");
    $(".currency-trigger[data-toggle=dropdown]").dropdown("toggle");
  });
  $(".currency-dropdown label[for=gbpCurrency]").click(function(){
    $(".selected-currency").html("<i class='fa fa-gbp'></i>");
    $(".currency-trigger[data-toggle=dropdown]").dropdown("toggle");
  });

 $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

});

$(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
        $('.scrollup').fadeIn();
    } else {
        $('.scrollup').fadeOut();
    }
}); 

//Select the navigation menu base on URL.
$(function () {
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);
    if (path == '') {
        path = "/";
    }
    $(".nav a").each(function () {
        if ($(this).attr('href') == path) {
            $(this).parent().addClass("active");
        }
    });
});