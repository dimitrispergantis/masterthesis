﻿$(document).ready(function() {
    $(document).on('click',
        '.delete-link',
        function() {
            event.preventDefault();
            var tr = $(this).closest('tr');

            tr.addClass("bg-danger");

            tr.fadeOut(500,
                function() {

                    var table = tr.closest('table');
                    tr.remove();

                    var checkboxName = [];
                    tr.find('input:checkbox').each(function (j, input) {
                        checkboxName = $(input).attr("name");
                    });
                    $("input[name='" + checkboxName + "']").remove();

                    updateIndexes(table);
                });

           
        });

    $("#btnAddProduct").on("click",
        function(event) {
            event.preventDefault();
            addProductsRecord('productsTable');
        });
});

function addProductsRecord(tableId) {
    var rowTemplate =
        '<tr>' +
            '<td>' +
            '<input type="text" data-val="true" id="ProductVersionsList_{INDEX}__Mpn" name="ProductVersionsList[{INDEX}].Mpn" aria-describedby="ProductVersionsList_{INDEX}__Mpn-error" class="valid" aria-invalid="false">' +
            '</td>' +
            '<td>' +
            '<input type="text" data-val="true"  id="ProductVersionsList_{INDEX}__Size" name="ProductVersionsList[{INDEX}].Size" aria-describedby="ProductVersionsList_{INDEX}__Size-error" class="valid" aria-invalid="false">' +
            '</td>' +
            '<td>' +
            '<input type="text" data-val="true"  id="ProductVersionsList_{INDEX}__Color" name="ProductVersionsList[{INDEX}].Color" aria-describedby="ProductVersionsList_{INDEX}__Color-error" class="valid" aria-invalid="false">' +
            '</td>' +
            '<td>' +
            '<input type="text" data-val="true"  id="ProductVersionsList_{INDEX}__Weight" name="ProductVersionsList[{INDEX}].Weight" aria-describedby="ProductVersionsList_{INDEX}__Weight-error" class="valid" aria-invalid="false">' +
            '</td>' +
            '<td>' +
            '<input type="text" data-val="true" data-val-number="The field must be a number." data-val-required="Παρακαλώ συμπληρώστε την τιμή" id="ProductVersionsList_{INDEX}__Discount" name="ProductVersionsList[{INDEX}].Discount">' +
            '</td>' +
            '<td>' +
            '<input type="text" data-val="true" data-val-number="The field must be a number." data-val-required="Παρακαλώ συμπληρώστε την τιμή" id="ProductVersionsList_{INDEX}__InitialPrice" name="ProductVersionsList[{INDEX}].InitialPrice">' +
            '</td>' +
            '<td>' +
            '<input type="text" data-val="true" data-val-number="The field must be a number." data-val-required="Παρακαλώ συμπληρώστε την τιμή" id="ProductVersionsList_{INDEX}__AvailableStock" name="ProductVersionsList[{INDEX}].AvailableStock">' +
            '</td>' +
            '<td>' +
            '<input type="text" data-val="true" data-val-number="The field must be a number." data-val-required="Παρακαλώ συμπληρώστε την τιμή" id="ProductVersionsList_{INDEX}__RestockThreshold" name="ProductVersionsList[{INDEX}].RestockThreshold">' +
            '</td>' +
            '<td>' +
            '<input type="text" data-val="true" data-val-number="The field must be a number." data-val-required="Παρακαλώ συμπληρώστε την τιμή" id="ProductVersionsList_{INDEX}__MaxStockThreshold" name="ProductVersionsList[{INDEX}].MaxStockThreshold">' +
            '</td>' +
            '<td>' +
            '<a class="delete-link"> <i class="fa fa-trash-o admin-icon" aria-hidden="true"></i></a>' +
            '</td>' +
        '</tr>';

    addRecord(tableId, rowTemplate);
}

function addRecord(tableId, rowTemplate) {
    var table = $("#" + tableId);
    var newIndex = table.find("tr").length - 1;

    rowTemplate = rowTemplate.replace(/{INDEX}/g, newIndex);
    var newRow = $(rowTemplate);
    newRow.hide();

    table.append(newRow);
    newRow.addClass("bg-success");
    newRow.fadeIn(500,
        function() {
            newRow.removeClass("bg-success");
        });
    fnValidateDynamicContent(newRow);
}

function fnValidateDynamicContent($element) {
    var $currForm = $element.closest("form");
    $currForm.removeData("validator");
    $currForm.removeData("unobtrusiveValidation");
    $.validator.unobtrusive.parse($currForm);
    $currForm.validate(); // This line is important and added for client side validation to trigger, without this it didn't fire client side errors.
}

// This is where the magic happens
function updateIndexes(table) {
    // count the remaining rows
    var rowCounter = 0;
    // get every tr element except the header
    table.find("tr:gt(0)").each(function (i, row) {
        // get every input and select elements
        $(row).find('input, select').each(function(j, input) {
            // check whether the id-attribute is of type _[index]__
            var id = input.id.match(/_\d+__/);

            // if it is an element necessary for the ModelBinder => update the name attribute
            if (id != null && id.length && id.length == 1) {
                var attr = $(input).attr("name");
                // replace the old index of the name attribute with the calculated index
                var newName = attr.replace(attr.match(/\d+/), i);
                $(input).attr("name", newName);
            }
        });
        rowCounter++;
    });

    table.closest('form').find('input:hidden').each(function(i, input) {
        var nameArray = $(input).attr("name").match(/[\d+]/);
        if (nameArray != null) {
            var name = nameArray.input;
            rowCounter--;
            var newName = name.replace(name.match(/\d+/), rowCounter);
            $(input).attr("name", newName);
        }
    });
}

