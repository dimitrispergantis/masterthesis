/*

 Sudo Slider version 3.5.0 - jQuery plugin
 Written by Erik Krogh Kristensen erik@webbies.dk.
 http://webbies.dk/SudoSlider/

 Dual licensed under the MIT and GPL licenses.

 Based on EasySlider http://cssglobe.com/easy-slider-17-numeric-navigation-jquery-slider/
 But bear little resemblance at this point.

 Built for jQuery library
 http://jquery.com

*/
(function (g) { var ma = "object" == typeof self && self.self == self && self || "object" == typeof global && global.global == global && global; if ("function" === typeof define && define.amd) define(["jquery"], function (aa) { g(aa, ma) }); else if ("undefined" !== typeof exports) { var qa = require("jquery"); g(qa, ma) } else g(ma.jQuery, ma) })(function (g, ma) {
    function qa() {
        return {
            effect: "slide", speed: 1500, customLink: !1, controlsFadeSpeed: 400, controlsFade: !0, insertAfter: !0, vertical: !1, slideCount: 1, moveCount: 1, startSlide: 1, responsive: !0, ease: "swing",
            auto: !1, pause: 2E3, resumePause: !1, continuous: !1, prevNext: !0, numeric: !1, numericText: [], slices: 15, boxCols: 8, boxRows: 4, initCallback: Y, ajaxLoad: Y, beforeAnimation: Y, afterAnimation: Y, history: !1, autoHeight: !0, autoWidth: !0, updateBefore: !1, ajax: !1, preloadAjax: 100, loadingText: "", prevHtml: '<a href="#" class="prevBtn"> previous </a>', nextHtml: '<a href="#" class="nextBtn"> next </a>', controlsAttr: 'class="controls"', numericAttr: 'class="numericControls"', interruptible: !1, useCSS: !0, loadStart: Y, loadFinish: Y, touch: !1,
            touchHandle: !1, destroyCallback: Y, mouseTouch: !0, allowScroll: !0, CSSease: "swing", ajaxHasHTML: !1, performanceMode: 0
        }
    } function aa(b, a, e, f, A) { if (g.isFunction(a)) f ? aa(b, ["", "Up", "Right", "Down", "Left", a], e, 0, A) : b[e] = function (b) { var e = [b].concat(A), k = e.length - 1; if (0 === f && 0 == e[k]) { var g = b.diff; e[k] = b.options.vertical ? 0 > g ? 1 : 3 : 0 > g ? 2 : 4 } a.apply(this, e) }; else if (g.isArray(a)) for (var h = a.length - 1, u = a[h], m = 0; m < h; m++) { var r = A.slice(); r.push(m); aa(b, u, e + a[m], f, r) } else g.each(a, function (a, g) { aa(b, g, e + a, f, A) }) } function na(b,
        a, e, f, A, h, u, m, r) {
            var z = b.options, x = z.boxrows, k = z.boxcols, l = x * k, C = z.speed / (1 == l ? 1 : 2.5), v = Ha(b, k, x, !m), w = z = 0, E = 0, D = []; D[w] = []; a && [].reverse.call(v); A && ra(v); for (var n = 0; n < v.length; n++)D[w][E] = v[n], E++ , E == k && (e && [].reverse.call(D[w]), w++ , E = 0, D[w] = []); w = []; if (1 == h) for (v = 0; v < 2 * k + 1; v++) { l = v; n = []; for (h = 0; h < x; h++) { if (0 <= l && l < k) { E = D[h][l]; if (!E) return; n.push(E) } l-- } 0 != n.length && w.push(n) } else if (2 == h) {
                D = x / 2; E = a ? l : -1; var V = a ? -1 : 1; for (h = 0; h < D; h++) {
                    for (l = n = h; l < k - h - 1; l++)w[E += V] = v[n * k + l]; l = k - h - 1; for (n = h; n < x - h -
                        1; n++)w[E += V] = v[n * k + l]; n = x - h - 1; for (l = k - h - 1; l > h; l--)w[E += V] = v[n * k + l]; l = h; for (n = x - h - 1; n > h; n--)w[E += V] = v[n * k + l]
                }
            } else for (v = 0; v < x; v++)for (l = 0; l < k; l++)w.push([D[v][l]]); m && b.goToNext(); var G = 0; for (n = 0; n < w.length; n++) {
                x = w[n]; g.isArray(x) || (x = [x]); for (k = 0; k < x.length; k++)(function (g, k) {
                    function h(k) {
                        var h = g(), l = h.children(), x = h.width(), w = h.height(), n = x, v = w, z = parseFloat(h.css("left")), E = parseFloat(h.css("top")), D = z, J = E, V = parseFloat(l.css("left")), O = parseFloat(l.css("top")), R = V, p = O; if (u) {
                            if (A) {
                                var Q = sa([-n,
                                    n]); var L = sa([-v, v])
                            } else Q = a != e ? -n : n, L = a ? -v : v; m ? (D -= 1.5 * Q, J -= 1.5 * L) : h.css({ left: z + 1.5 * Q, top: E + 1.5 * L })
                        } f && (m ? (R -= n / 2, D += n / 2, p -= v / 2, J += v / 2, v = n = 0) : (h.css({ left: z + n / 2, top: E + v / 2 }), l.css({ left: V - n / 2, top: O - v / 2 }), h.width(0).height(0), r && h.css({ borderRadius: T(w, x) }))); m && h.css({ opacity: 1 }); G++; ya(function () { ab.ready(function () { W(l, { left: R, top: p }, C, !1, !1, b); W(h, { opacity: m ? 0 : 1, width: n, height: v, left: D, top: J, borderRadius: f && m && r ? T(w, x) : 0 }, C, !1, function () { G--; 0 == G && b.callback() }, b) }) }, k)
                    } m || 150 > k ? h(k) : ya(L(h,
                        [150]), k - 150)
                })(x[k], z); z += C / w.length * 1.5
            }
    } function Z(b, a, e, f, A, h, u, m) {
        var r = b.options, z = r.slices, x = r.speed / 2; r = b.slider; for (var k = Ha(b, a ? z : 1, a ? 1 : z, !m), l = g(), C = 0; C < k.length; C++)l = l.add(k[C]()); var v = 0, w = !1; e ? [].reverse.call(l) : g([].reverse.call(l.get())).appendTo(r); f && ra(l); l.each(function (f) {
            f *= x / z; var k = g(this), l = k.width(), r = k.height(), E = k.css("left"), C = k.css("top"), J = a ? E : C, G = k.children()[a ? "width" : "height"](); 1 == h ? J = 0 : 2 == h && (J = G / 2); e && (J = G - J); a ? k.css({ width: A || u ? l : 0, left: J }) : k.css({
                height: A ||
                    u ? r : 0, top: J
            }); m && (J = 1 == u ? -1 : 1, k.css({ top: C, left: E, width: l, height: r, opacity: 1 }), a ? C = J * r : E = J * l); u && (J = !0, 3 == u ? w = w ? J = !1 : !0 : 2 == u && (J = !1), a ? m ? C = (J ? -1 : 1) * r : k.css({ bottom: J ? 0 : r, top: J ? r : 0, height: m ? r : 0 }) : m ? E = (J ? -1 : 1) * l : k.css({ right: J ? 0 : l, left: J ? l : 0, width: m ? l : 0 })); v++; ya(L(W, [k, { width: l, height: r, opacity: m ? 0 : 1, left: E, top: C }, x, !1, function () { v--; 0 == v && b.callback() }, b]), f)
        }); m && b.goToNext()
    } function za(b, a, e) {
        var f = 2 == e || 4 == e; e = 2 == e || 3 == e ? 1 : -1; var g = b.options.speed, h = b.callback; if (a) {
            a = b.fromSlides; var u = fa(b,
                !0).hide(); b.slider.append(u); var m = T(u.height(), a.height()), r = T(u.width(), a.width()); u.css(f ? { left: e * r } : { top: e * m }).show(); W(u, { left: 0, top: 0 }, g, !1, h, b)
        } else a = fa(b, !1), b.slider.append(a), b.goToNext(), u = b.toSlides, u = -1 == e ? a : u, m = u.height(), r = u.width(), W(a, f ? { left: e * r } : { top: e * m }, g, !1, h, b)
    } function bb(b) { var a = Ka(b.slider), e = b.options.speed, f = b.target, g = f.left; f = f.top; b.options.usecss ? W(a, { transform: "translate(" + g + "px, " + f + "px)" }, e, !1, b.callback, b, !0) : W(a, { marginTop: f, marginLeft: g }, e, !1, b.callback, b) }
    function W(b, a, e, f, g, h, u) {
        function m() { if (!u) { var a = {}; a[k] = "0s"; a[l] = ""; a[x] = ""; b.css(a) } } var r = !h || h.options.usecss; if (!1 !== ha && r) {
            f || (f = h.options.cssease); var A = {}, x = ha + "transition"; r = La(a); A[x] = r.join(" ") + ("" == ha ? "" : " " + ha + r.join(" " + ha)); var k = x + "-duration"; A[k] = e + "ms"; var l = x + "-timing-function"; "swing" == f && (f = "ease-in-out"); A[l] = f; h && h.stopCallbacks.push(m); f = ha.replace(/\-/g, ""); var C = f + ((f ? "T" : "t") + "ransitionend") + " transitionend", v = !1, w = function () { v || (v = !0, b.unbind(C), m(), g && g()) }; if (20 >
                e) b.css(a), w(); else return p(function () { b.css(A); p(function () { b.css(a); var f = +new Date; b.on(C, function V(a) { b.is(a.target) && -100 < +new Date - f - e && (b.off(C, V), w()) }); setTimeout(w, e + 100) }) }), w
        } else f || (f = h.options.ease), b.animate(a, e, f, g)
    } function Ma(b, a) { var e = b.options; e.boxcols = 1; e.boxrows = 1; e.speed = a; na(b, !1) } function Ha(b, a, e, f) {
        function A(a, e) { m.push(function () { var g = fa(b, f); g = Aa(g, k * a, x * e, k, x); h.append(g); return g }) } var h = b.slider, u = b.options.vertical, m = []; var r = u ? f ? b.toSlides.width() : b.slider.width() :
            0; var z = u ? 0 : f ? b.toSlides.height() : b.slider.height(); b.toSlides.each(function () { var a = g(this); u ? z += a.height() : r += a.width() }); for (var x = Math.ceil(r / a), k = Math.ceil(z / e), l = 0; l < e; l++)for (var C = 0; C < a; C++)A(l, C); return m
    } function Aa(b, a, e, f, A) { b.css({ width: b.width(), height: b.height(), display: "block", top: -a, left: -e }); return g("<div>").css({ left: e, top: a, width: A, height: f, opacity: 0, overflow: "hidden", position: "absolute" }).append(b).addClass("sudo-box") } function fa(b, a) {
        var e = a ? b.toSlides : b.fromSlides, f = e.eq(0).position(),
        A = f.left, h = f.top, u = 0, m = 0, r = g("<div>").css({ position: "absolute", top: 0, left: 0 }).addClass("sudo-box"); e.each(function (a, b) { var e = g(b), f = e.outerWidth(!0), x = e.outerHeight(!0), v = e.clone(), w = e.position(); e = w.left - A; w = w.top - h; v.css({ position: "absolute", left: e, top: w, opacity: 1 }); u = T(u, w + x); m = T(m, e + f); r.append(v) }); r.width(m).height(u); return r
    } function L(b, a) { return function () { b.apply(ia, a) } } function ja(b, a, e) {
        if (b) {
            b = b.add(b.find("img")).filter("img"); var f = b.length; f ? b.each(function () {
                var b = g(this), h = function () {
                    b.off("load error",
                        h); f--; a ? 0 == f && e() : e()
                }; b.on("load error", h); if ("complete" == this.readyState) b.trigger("load"); else if (this.readyState) this.src = this.src; else if (this.complete) b.trigger("load"); else if (this.complete === ia) { var u = this.src; this.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw=="; this.src = u }
            }) : e()
        } else e()
    } function Bb(b) { for (var a = g.fn.jquery.split("."), e = a.length, f = 0; f < e; f++) { if (+a[f] < +b[f]) return !1; if (+a[f] > +b[f]) break } return !0 } function Ba(b) { return b.replace(/^\s+|\s+$/g, "") }
    function La(b) { var a = [], e; for (e in b) a.push(e); return a } function p(b) { setTimeout(b, 0) } function ya(b, a) { return setTimeout(b, a) } function Ka(b) { return b.children().not(".sudo-box") } function Ca(b) { var a = {}, e; for (e in b) a[e.toLowerCase()] = b[e]; return a } function ra(b) { for (var a, e, f = b.length; f; a = Math.random() * f | 0, e = b[--f], b[f] = b[a], b[a] = e); return b } function G(b) { return parseInt(b, 10) } function Da(b, a) { return (b % a + a) % a || 0 } function Q(b) { return 0 > b ? -b : b } function T(b, a) { return b > a ? b : a } function Na(b, a) {
        return b <
            a ? b : a
    } function Oa(b) { if (g.isArray(b)) return ka(b); if (g.isFunction(b)) return b; b = Ba(b); if (-1 != b.indexOf(",")) { var a = b.split(","); return ka(a) } var e = Ca(da); b = Ba(b.toLowerCase()); if (a = e[b]) return a; a = []; for (var f in e) f.match(new RegExp("^" + b.split("*").join(".*") + "$", "g")) && a.push(e[f]); return a.length ? ka(a) : bb } function ka(b) { return function (a) { var e = sa(b); return Oa(e)(a) } } function sa(b) { return b[ra(La(b))[0]] } function Cb(b) {
        var a = "bez_" + b.join("_").replace(/\./g, "p"), e = g.easing; if (!g.isFunction(e[a])) {
            var f =
                function (a, b) { function e(e, k) { h[k] = 3 * a[k]; g[k] = 3 * (b[k] - a[k]) - h[k]; f[k] = 1 - h[k] - g[k]; return e * (h[k] + e * (g[k] + e * f[k])) } var f = [0, 0], g = [0, 0], h = [0, 0]; return function (a) { for (var b = a, l = 0, m; 14 > ++l;) { m = e(b, 0) - a; if (.001 > Q(m)) break; b -= m / (h[0] + b * (2 * g[0] + 3 * f[0] * b)) } return e(b, 1) } }; e[a] = function (a, e, g, m, r) { return m * f([b[0], b[1]], [b[2], b[3]])(e / r) + g }
        } return a
    } var ia, Y = function () { }, ha = function () {
        a: { var b = g("<div>")[0].style; for (a in b) if (b = a.toLowerCase(), -1 !== b.indexOf("transition", b.length - 10)) break a; var a = !1 } if (!1 ===
            a) return !1; a = a.slice(0, a.length - 10); return 0 != a.length ? "-" + a + "-" : ""
    }(), cb = g(ma), ab = g(document); g.fn.sudoSlider = function (b) {
        var a = this; b = g.extend(Ca(qa()), Ca(b)); !1 !== ha && Bb([1, 8, 0]) || (b.usecss = !1); return this.each(function () {
            function e() {
                var c = 0; for (a in oa) d[c] = oa[a], c++; U = !0; db = !1; Pa = []; eb = []; Qa = []; Ea = []; Ra = []; H = Ka(M); c = H.length; var a = g("<div>"); c ? (Sa = H.is("ul")) || H.hasClass("slidesContainer") || mb || (a.append(H), M.append(H = a)) : (M.append(H = a), Sa = !1); mb = !0; c = Ka(H); B = []; y = c.length; c.each(function (c,
                    a) { var b = g(a); B[c] = b; b.css({ position: "relative" }); "none" == b.css("display") && b.css("display", "inline") }); H.addClass("slidesContainer"); c.addClass("slide"); c.each(function (c, a) { g(a).attr("data-slide", c + 1) }); if (d[30] && (a = d[30].length, a > y)) { for (c = 1; c <= a - y; c++) { var b = Sa ? "li" : "div"; b = g("<" + b + ">" + d[32] + "</" + b + ">"); H.append(b); B[y + (c - 1)] = b } c = Ka(H); y = a } c.each(function (c, a) { Pa[c] = !1; ja(g(a), !0, function () { Pa[c] = !0 }) }); q = (q = !1 === ba ? 0 : ba) || 0; X = !0; Ta = []; la = !1; M.css({ overflow: "hidden" }); "static" == M.css("position") &&
                        M.css({ position: "relative" }); c.css({ "float": "left", listStyle: "none" }); H.add(c).css({ display: "block", position: "relative", margin: "0" }); J(0, 0, !0); d[7] = G(d[7]); S = d[7]; d[7] += d[8] - 1; "string" == typeof d[9] && "random" == d[9].toLowerCase() && (d[9] = Math.random() * y | 0); d[9] = G(d[9]) - 1 || 0; d[0] = Oa(d[0]); d[18] = d[18].slice(0); for (c = 0; c < y; c++)d[18][c] || "" == d[18][c] || (d[18][c] = c + 1), d[30] && (d[30][c] = d[30][c] || !1); d[4] = d[4] && !d[15]; d[10] && A(!0); H[d[6] ? "height" : "width"](9E6)[d[6] ? "width" : "height"]("100%"); d[28] = d[28] && !d[10];
                d[10] && ta(cb, "resize focus", A, ""); if (d[17] || d[16]) { ua = g("<span " + d[35] + "></span>"); M[d[5] ? "after" : "before"](ua); if (d[17]) for (fb = g("<ol " + d[36] + "></ol>"), ua.prepend(fb), b = (a = "pages" == d[17]) ? S : 1, c = 0; c < y - (d[15] || a ? 1 : S) + 1; c += b)Ta[c] = g('<li data-target="' + (c + 1) + '"><a href="#"><span>' + d[18][c] + "</span></a></li>").appendTo(fb).click(function () { l(h(this) - 1, !0); return !1 }); d[16] && (nb = k(d[34], "next"), ob = k(d[33], "prev")); d[4] && v(q, 0) } c = [4, 1, 14]; for (a = 0; a < c.length; a++)d[c[a]] = x(d[c[a]]); d[2] && (c = Ba(d[2]), ">" ==
                    c.charAt(0) ? ta(M, "click", f, c.substr(1, c.length)) : ta(ab, "click", f, c)); ja(Aa(d[9], d[7]), !0, function () { if (!1 !== ba) Ua(ba, !1); else if (d[26]) { var c; (c = cb.hashchange) ? c(u) : (c = g.address) ? c.change(u) : ta(cb, "hashchange", u); u() } else Ua(d[9], !1); w(q) }); d[30][d[9]] && va(d[9]); if (!0 === d[31]) for (a = 0; a < y; a++)d[30][a] && d[9] != a && va(a); else m()
            } function f() { var c; if (c = h(this)) "stop" == c ? (d[12] = !1, z()) : "start" == c ? (r(), d[12] = !0) : "block" == c ? X = !1 : "unblock" == c ? X = !0 : l(c == G(c) ? c - 1 : c, !0); return !1 } function A(c) {
                function a() {
                    if ((M.is(":visible") &&
                        !U && !la || !0 === c) && 0 != y) { var a = M.width(); var b = d[6] ? a : a / S; a = G(b); var t = 1 / (b - a); if (pb !== b || !0 === c) { pb = b; b = 1; for (var e = 0; e < y; e++)b++ , b >= t ? (b = 0, B[e].width(a + 1)) : B[e].width(a); !1 !== Va && r(Va); Fa(); wa(); O(q); D(q, 0) } }
                } a(); p(a); setTimeout(a, 20)
            } function h(c) { c = g(c); return c.attr("data-target") || c.attr("rel") } function u() { a: { var c = location.hash.substr(1); for (var a = 0; a < d[18].length; a++)if (d[18][a] == c) { c = a; break a } c = c && !U ? q : d[9] } U ? Ua(c, !1) : l(c, !1) } function m() {
                if (!1 !== d[31]) {
                    var c = G(d[31]); if (d[30]) for (var a =
                        0; a < d[30].length; a++)if (d[30][a]) { clearTimeout(Wa); Wa = ya(function () { d[30][a] ? va(a) : m() }, c); break }
                }
            } function r(c) { c === ia && (c = B[q].attr("data-pause"), c = c !== ia ? G(c) : d[13]); gb && (c = T(c, 100)); z(); Xa = !0; Va = c; hb = ya(function () { Xa && !ea && (l("next", !1), Va = !1) }, c) } function z(c) { hb && clearTimeout(hb); c || (Xa = !1) } function x(c) { return G(c) || 0 == c ? G(c) : "fast" == c ? 200 : "normal" == c || "medium" == c ? 400 : 600 } function k(c, a) { return g(c).prependTo(ua).click(function () { l(a, !0); return !1 }) } function l(c, a, b) {
                if (X && !U) z(!0), la || Ha(c,
                    a, b); else if (d[37] && ea) I(fa(c)) !== ib && (Fa(), l(c, a, b)); else if (Ya = c, qb = a, rb = b, d[30]) for (a = c = fa(c); a < c + S; a++)d[30][a] && va(I(a))
            } function C(c, a, b) { function e() { c || 0 != t.css("opacity") || t.css({ visibility: "hidden" }) } c = c ? 1 : 0; var t = g(); d[16] && (t = b ? nb : ob); if (d[2]) { var f = g(d[2]); b = '="' + (b ? "next" : "prev") + '"]'; f = f.filter("[rel" + b + ", [data-target" + b); t = t.add(f) } f = { opacity: c }; c && t.css({ visibility: "visible" }); d[38] ? W(t, f, a, d[46], e) : t.animate(f, { queue: !1, duration: a, easing: d[11], callback: e }) } function v(c, a) {
                C(c, a,
                    !1); C(c < y - S, a, !0)
            } function w(c) { c = I(c) + 1; "pages" != d[17] || c != y - S + 1 || d[15] || (c = y); if (d[17]) for (var a = 0; a < Ta.length; ++a)E(Ta[a], c); d[2] && E(g(d[2]), c) } function E(c, a) { c && c.filter && (c.filter(".current").removeClass("current"), c.filter(function () { var c = h(this); if ("pages" == d[17]) for (var b = S - 1; 0 <= b; b--) { if (c == a - b) return !0 } else return c == a; return !1 }).addClass("current")) } function D(c, a) { sb = c = I(c); jb = +new Date + a; (d[27] || d[28]) && n(c) } function n(c) { M.ready(function () { Y(c); ja(B[c], !1, L(Y, [c])) }) } function V(c,
                a) { for (var b = 0, e = c; e < c + S; e++) { var t = B[I(e)]; t && (t = t["outer" + (a ? "Height" : "Width")](!0), b = a == d[6] ? b + t : T(t, b)) } return b } function Y(c) { if (c == sb && M.is(":visible") && !U && !la) { var a = jb - +new Date; a = T(a, 0); var b = {}; d[27] && (b.height = V(c, !0) || 1); d[28] && (b.width = V(c, !1) || 1); a: if (c = tb, La(c).length != La(b).length) var e = !1; else { for (e in c) if (c[e] != b[e]) { e = !1; break a } e = !0 } e || (tb = b, d[38] ? W(M, b, a, d[46]) : 0 == a ? M.stop().css(b) : M.animate(b, { queue: !1, duration: a, easing: d[11] })) } } function O(c) { var a = R(c, !1); c = R(c, !0); J(a, c) }
            function J(c, a, b) { ub = c; vb = a; (d[38] || b) && H.css({ transform: "translate(" + c + "px, " + a + "px)" }); if (!d[38] || b) H.css({ marginLeft: 0, marginTop: 0 }), H.css({ marginLeft: c, marginTop: a }) } function R(c, a) { a == ia && (a = d[6]); var b = B[I(c)]; return b && b.length ? -b.position()[a ? "top" : "left"] : 0 } function aa() { if (!1 !== Ya) { var c = Ya; Ya = !1; p(L(l, [c, qb, rb])) } } function Z() { if (Za) { Za = !1; for (var c = 0; c < y; c++)B[c].css({ position: "relative" }).show(); H.css({ display: "block", position: "relative" }); O(q) } } function da(c, a, b, d) {
                c = I(c); c = L(a ? na : qa,
                    [B[c], c + 1, d]); b ? c() : p(c)
            } function na(c, b) { d[25].call(c, b, a) } function qa(c, b, e) { d[24].call(c, b, a, e) } function fa(a) { return "next" == a ? ka(q + d[8], a) : "prev" == a ? ka(q - d[8], a) : "first" == a ? 0 : "last" == a ? y - 1 : ka(G(a), a) } function ka(a, b) { if (d[15]) return "next" == b || "prev" == b ? a : I(a); var c = y - S; return a > c ? q == c && "next" == b ? 0 : c : 0 > a ? 0 == q && "prev" == b ? c : 0 : a } function va(a, b) {
                function c() { var c = new Image; c.src = f; var b = g(c); ja(b, !0, function () { t.empty().append(c); ra(a, !0) }) } if (b) { var e = Qa[a]; e || (e = Qa[a] = []); e.push(b) } if (Ra[a]) b &&
                    ja(B[a], !0, L(p, [b])); else if (!Ea[a]) { Ea[a] = !0; var f = d[30][a]; if (f) { Wa && clearTimeout(Wa); var t = B[a]; d[30][a] = !1; oa.ajax[a] = !1; if (d[47]) { var h = !1; g.ajax({ url: f, success: function (b, d, e) { h = !0; Ia(function () { var d = e.getResponseHeader("Content-Type"); d && "i" != d.substr(0, 1) ? (t.html(b), ra(a, !1)) : c() }) }, complete: function () { h || c() } }) } else c() } else p(b) }
            } function Ia(a) { ea ? eb.push(a) : p(a) } function ra(c, b) {
                var e = B[c]; ea || (O(q), D(q, 0)); ja(e, !0, L(Ia, [function () {
                    O(q); D(q, 0); Ra[c] = !0; Ja(Qa[c]); m(); p(function () {
                        d[23].call(B[c],
                            c + 1, b, a)
                    }); U && (U = !1, p(sa))
                }]))
            } function sa() { db = !0; d[15] && xa(q, 0); D(q, 0); O(q); aa(); d[10] && A(); d[12] && r(); d[22].call(a); d[41] && Ca(); ja(Aa(q, y), !1, L(Ia, [function () { D(q, 0); O(q) }])) } function Ca() {
                var a = g("body"), b, e = !1, f = d[0]; d[0] = function (a) { if (e) { e = !1; var c = a.options; c.ease = b; c.cssease = b; return bb(a) } return f(a) }; var h, k, l, m, r, u = [], v = [], w = 0, n, A, x, B, E = !1, z = 0, C = 0, G = 0, H = 0, D = function (c) {
                    if (X) {
                        var f = c.type, t = "m" == f.substr(0, 1); if (t) { var pa = "mousedown"; var F = "mouseup"; var p = "" } else pa = "touchstart", F = "touchend",
                            p = "touchcancel", c = c.originalEvent; if (!E) { if (f !== pa) return; var N = c.target, ca = g(N); d[42] || (ca = ca.parents().add(N)); N = d[42] || M; "string" === typeof N && (N = Ba(N), ">" === N.charAt(0) && (N = g(N.substr(1, N.length), M))); if (!ca.filter(N).length) return; E = !0; Z(); I.on("touchmove touchend touchcancel", L, D); if (d[44]) I.on("mousemove mouseup", L, D) } if (f !== F && f !== p) {
                                t ? (F = c.pageX, p = c.pageY) : (p = c.touches[0], F = p.pageX, p = p.pageY); if (f === pa) {
                                    z = F; C = p; var K = F - z, P = p - C; a.addClass("sudoSlider-dragging"); n = q; A = 0; ea = !0; wa(); k = vb; h = ub;
                                    r = d[6] ? Q(P) : Q(K); m = l = +new Date; K = R(n); x = R(n + 1) - K; B = !1; !t && d[45] || c.preventDefault()
                                } else { f = F - z; pa = p - C; ca = d[6] ? pa : f; N = Q(ca); u[w] = N - r; var O = +new Date; v[w] = O - m; w = (w + 1) % 3; m = O; r = N; d[15] && y >= S + 1 && (N = 0, ca - A < x && (B = !1, A += x, N = 1, K = R(n + N), xa(n + N, 0), P = R(n + N)), 0 < ca - A && (N = -1, K = R(n), xa(n + N, 0), P = R(n), ca = R(n - 1) - P, A += ca, B = -ca), 0 != N && (n += N, d[6] ? k -= K - P : h -= K - P, x = R(n + 1) - R(n))); d[6] ? f = 0 : pa = 0; J(h + f, k + pa); K = F - z; K = Q(p - C) > Q(K); t = t ? !1 : d[45] ? d[6] ? !K : K : !1; t || c.preventDefault() } G = F - z; H = p - C
                            } else {
                                I.off("touchmove touchend touchcancel",
                                    L, D); d[44] && I.off("mousemove mouseup", L, D); c = G; t = H; a.removeClass("sudoSlider-dragging"); p = d[6] ? t : c; p -= A; B && (p -= B, n++); t = Q(p); F = +new Date; for (P = K = c = 0; 3 > P; P++)f = v[P], f + 100 < F && (c += f, K += u[P]); P = d[6] ? M.height() : M.width(); F = Q(K) / c; c = .2 <= F || t >= P / 2; if (0 < K && 0 > t || 0 > K && 0 < t || 10 >= t) c = !1; p = 0 > p ? "next" : "prev"; d[15] || (q + 1 == y ? "next" == p && (c = !1) : 0 == q && "prev" == p && (c = !1)); p = "next" == p ? n + 1 : n - 1; p = Da(p, y); t = c ? P - t : t; K = t / F * 1.3; P = T(P / t * d[1], d[1] / 4); K = K < P ? Na(K, d[1]) : Na(P, d[1]); t = F * K / (t + F * K); F = 1 - t; b = d[38] ? "cubic-bezier(" + F + "," +
                                        t + ",0.3,1)" : Cb([F || 0, t || 0, .3, 1]); X = !1; e = !0; c ? Ga(p, K, !0, !0, !0) : Ga(n, K, !0, !0, !0); E = !1
                        }
                    }
                }, p = d[42] || M; if ("string" === typeof p) if (p = Ba(p), ">" === p.charAt(0)) { var I = M; var L = p.substr(1, p.length) } else I = ab, L = p; else I = p; ta(I, "touchstart", D, L); d[44] && ta(I, "mousedown", D, L)
            } function Ja(a) { for (; a && a.length;)a.splice(0, 1)[0]() } function Ha(c, b, e) {
                var f = fa(c), t = "next" == c || "prev" == c; c = I(f); if (c != q) if (X = !1, ib = c, d[30]) {
                    for (var g = 0, h = c; h < c + S; h++) {
                        var k = I(h); Pa[k] && (d[30] ? d[30][k] ? 0 : !Ea[k] || Ra[k] : 1) || (g++ , va(k, function () {
                            g--;
                            0 == g && (d[40].call(a, f + 1), Ga(f, e, b, t))
                        }))
                    } 0 == g ? Ga(f, e, b, t) : d[39].call(a, f + 1)
                } else Ga(f, e, b, t)
            } function wa() { d[38] && H.css(ha + "transition-duration", "") } function za(a) { a = I(a); if (a != wb) { wb = a; wa(); for (var c = 0; c < y; c++) { var b = B[I(a + c)]; H.append(b) } O(q) } } function xa(a, b) { var c = T(G((y - b - S) / 2), 0); a = Da(a - c, y); za(a) } function Ma(a) { var b = Na(a, q); a = Q(a - q); xa(b, a) } function Ga(b, e, f, h, k) {
                Z(); d[29] && w(b); d[26] && f && (ma.location.hash = d[18][b]); d[4] && v(b, d[3]); for (var c = g(), t = g(), l = 0; l < S; l++)c = c.add(B[I(q + l)]), t = t.add(B[I(b +
                    l)]); l = b - q; if (d[15] && !h) { h = Q(l); var m = b; var n = -q + b + y; Q(n) < h && (m = b + y, l = n, h = Q(l)); n = -q + b - y; Q(n) < h && (m = b - y, l = n) } else m = b; d[15] && !k && Ma(m); k = R(m, !1); h = R(m, !0); var r = B[I(b)]; n = g.extend(!0, {}, oa); var u = d[1], x = r.attr("data-speed"); x != ia && (u = G(x)); e != ia && (u = G(e)); n.speed = u; var F = d[0]; (e = r.attr("data-effect")) && (F = Oa(e)); B[q] && (e = B[q].attr("data-effectout")) && (F = Oa(e)); ea = !0; xb = F; var z = !0; kb = function () {
                        z = ea = !1; Ua(b, f); if (screen.fontSmoothingEnabled && t.style) try { t.style.removeAttribute("filter") } catch (Eb) { } d[15] &&
                            xa(m, 0); da(b, !0); d[10] && A(); Ja(eb)
                    }; lb = { fromSlides: c, toSlides: t, slider: M, container: H, options: n, to: b + 1, from: q + 1, diff: l, target: { left: k, top: h }, stopCallbacks: [], callback: function () { z && (z = !1, Fa()) }, goToNext: function () { z && ja(g(".sudo-box", M), !0, L(O, [b])) } }; D(b, u); p(function () { da(b, !1, !0, u); F.call(a, lb) })
            } function Fa() { if (ea) { gb = !0; kb && kb(); Ja(lb.stopCallbacks); var a = xb.stop; a ? a() : (g(".sudo-box", M).remove(), H.stop()); D(q, 0); O(q); gb = !1 } } function ta(a, b, d, e) { a.on(b, e, d); yb.push(function () { a.off(b, e, d) }) } function Ua(a,
                b) { X = !b && !d[12]; q = a; wa(); D(q, 0); q = I(q); d[29] || w(q); O(q); X = !0; ib = !1; var c = d[48]; if (!Za && c && H.is("div")) { Za = !0; for (var e = 0; e < y; e++) { var f = B[e]; 2 === c && f.css({ position: "static" }); e >= q && e < q + d[7] || e + y >= q && e + y < q + d[7] || f.hide() } 2 === c && H.css({ display: "inline", position: "static" }) } d[12] && (b ? (z(), d[14] && r(d[14])) : U || r()); aa(); d[4] && U && v(q, 0); !U || d[30][q] || Ea[q] || (U = !1, p(sa)) } function Aa(a, b) { for (var c = g(), d = 0; d < b; d++)c = c.add(B[I(a + d)]); return c } function I(a) { return Da(a, y) } function zb() {
                    z(); Fa(); D(q, 0); Z(); la =
                        !0; ba = q; Ja(yb); wa(); ua && ua.remove(); za(0); O(q); d[43].call(a)
                } function Ab() { la && e() } function $a(a) { return function F() { var b = !la; if (U || db) zb(), a.apply(this, arguments), b && Ab(); else { var c = arguments; p(function () { F.apply(ia, c) }) } } } var U, db = !1, Sa, H, B, Pa, y, q, X, ib, Ta, fb, la, ba = !1, ua, nb, ob, hb, Xa, S, Wa, M = g(this), sb, jb = 0, ea = !1, xb, kb, lb, eb, Qa, Ea, Ra, Ya = !1, qb, rb, mb = !1, d = [], oa = g.extend(!0, {}, b), vb, ub, yb = [], Va = !1, gb = !1, pb = -1, tb = {}, Za = !1, wb = 0; a.destroy = zb; a.init = Ab; a.getOption = function (a) { return oa[a.toLowerCase()] };
            a.setOption = $a(function (a, b) { oa[a.toLowerCase()] = b }); a.setOptions = $a(function (a) { for (var b in a) oa[b.toLowerCase()] = a[b] }); a.runWhenNotAnimating = Ia; a.insertSlide = $a(function (a, b, e, f) { b = 0 > b ? y - Da(-b - 1, y + 1) : Da(b, y + 1); a = g(a || "<div>"); Sa ? a = g("<li>").prepend(a) : 1 != a.length && (a = g("<div>").prepend(a)); b && 0 != b ? B[b - 1].after(a) : H.prepend(a); f ? ba = f - 1 : (b <= ba || !b || 0 == b) && ba++; d[18].length < b && (d[18].length = b); d[18].splice(b, 0, e || G(b) + 1) }); a.removeSlide = $a(function (a) {
                a = T(0, G(a) - 1); B[Na(a, y - 1)].remove(); d[18].splice(a,
                    1); a < ba && ba--
            }); a.goToSlide = function (a, b) { var c = a == G(a) ? a - 1 : a; p(L(l, [c, !0, b])) }; a.block = function () { X = !1 }; a.unblock = function () { X = !0 }; a.startAuto = function () { d[12] = !0; r() }; a.stopAuto = function () { d[12] = !1; z() }; a.adjust = function F(a) { var b = T(jb - +new Date, 0); D(q, b); ea || O(q); a || p(L(F, [!0])) }; a.getValue = function (a) { return { currentslide: q + 1, totalslides: y, clickable: X, destroyed: la, autoanimation: Xa }[a.toLowerCase()] }; a.getSlide = function (a) { return B[I(G(a) - 1)] }; a.stopAnimation = Fa; e()
        })
    }; g.fn.sudoSlider.getDefaultOptions =
        qa; var Db = {
            box: {
                Random: ["", "GrowIn", "GrowInRounded", "GrowOut", "GrowOutRounded", "FlyIn", "FlyOut", function (b, a) { na(b, !1, !1, 0 < a && 5 > a, !0, 0, 5 == a || 6 == a, 3 == a || 4 == a || 6 == a, 2 == a || 4 == a) }], Rain: ["", "GrowIn", "GrowInRounded", "GrowOut", "GrowOutRounded", "FlyIn", "FlyOut", ["UpLeft", "DownLeft", "DownRight", "UpRight", function (b, a, e) { na(b, 0 == e || 3 == e, 1 == e || 3 == e, 1 <= a && 4 >= a, !1, 1, 5 == a || 6 == a, 6 == a || 3 == a || 4 == a, 2 == a || 4 == a) }]], Spiral: ["InWards", "OutWards", {
                    "": function (b, a) { na(b, a, !1, !1, !1, 2, !1, !1, !1) }, Grow: ["In", "Out", ["", "Rounded",
                        function (b, a, e, f) { na(b, a, !1, !0, !1, 2, !1, e, f) }]]
                }]
            }, fade: { "": function (b) { Ma(b, b.options.speed) }, OutIn: function (b) { var a = b.options.speed, e = G(.6 * a); e = a - e; b.stopCallbacks.push(function () { b.fromSlides.stop().css({ opacity: 1 }) }); W(b.fromSlides, { opacity: 1E-4 }, e, !1, L(Ma, [b, a]), b) } }, foldRandom: ["Horizontal", "Vertical", function (b, a) { Z(b, a, !1, !0) }], slide: bb, stack: ["Up", "Right", "Down", "Left", ["", "Reverse", function (b, a, e) { var f = 0 < b.diff; e && (f = !f); za(b, f, ++a) }]]
        }, da = {}; aa(da, {
            blinds: ["1", "2", function (b, a, e) {
                a++;
                Z(b, 2 == e || 4 == e, 1 == e || 4 == e, !1, !1, a)
            }], fold: function (b, a) { Z(b, 2 == a || 4 == a, 1 == a || 4 == a) }, push: ["Out", "In", za], reveal: function (b, a) {
                var e = 1 == a || 3 == a, f = b.options.speed, g = fa(b, !0), h = g.width(), p = g.height(), m = Aa(g, 0, 0, 0, 0).css({ opacity: 1 }).appendTo(b.slider), r = m.add(g); r.hide(); e ? (m.css({ width: h }), 1 == a && (g.css({ top: -p }), m.css({ bottom: 0, top: "auto" }))) : (m.css({ height: p }), 4 == a && (g.css({ left: -h }), m.css({ right: 0, left: "auto" }))); r.show(); e ? r.width(h) : r.height(p); W(g, { left: 0, top: 0 }, f, !1, Y, b); W(m, { width: h, height: p },
                    f, !1, b.callback, b)
            }, slice: { "": ["", "Reveal", ["", "Reverse", "Random", function (b, a, e, f) { Z(b, 1 == f || 3 == f, e, 2 == e, !1, 0, 1 == f || 4 == f ? 1 : 2, a) }]], Fade: function (b, a) { Z(b, 2 == a || 4 == a, 1 == a || 4 == a, !1, !0) } }, zip: function (b, a) { Z(b, 2 == a || 4 == a, 1 == a || 4 == a, !1, !1, 0, 3) }, unzip: function (b, a) { Z(b, 2 == a || 4 == a, 1 == a || 4 == a, !1, !1, 0, 3, !0) }
        }, "", !0, []); aa(da, Db, "", !1, []); da.random = ka(da); g.fn.sudoSlider.effects = da
});