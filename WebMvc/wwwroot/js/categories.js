﻿var slider = new Slider("#custom-slider",
    { tooltip: 'always', tooltip_position: 'bottom', range: true });

$(document).ready(function () {
    $('#pricebutton').on("click",
        function () 
        {
            var id = $('input[name=category]:checked').attr("id").split('-')[1];
            var minValue = slider.getValue()[0];
            var maxValue = slider.getValue()[1];
            var pageSize = $('#pageSize').find(":selected").val();
            var sort = $('#sort').find(":selected").val();

            if ($('input[name=manufacturer]:checked').attr("id") != null) {
                
                var manufacturerId = $('input[name=manufacturer]:checked').attr("id").split('-')[1];
                window.location.replace("/Categories?categoryId=" + id
                    +"&manufacturerId=" + manufacturerId + "&currentPriceMin=" + minValue + "&currentPriceMax=" + maxValue 
                    + "&sortOrder=" + sort + "&pageSize=" + pageSize) ;
            } 
            else {
                window.location.replace("/Categories?categoryId=" + id
                    + "&currentPriceMin=" + minValue + "&currentPriceMax=" + maxValue + "&sortOrder=" + sort + "&pageSize=" + pageSize) ;
            }

        });

    $('#categoryDiv').on("click",
        ".js-category-item",
        function () {
            var id = $(this).attr("id").replace('category-', '');
            var pageSize = $('#pageSize').find(":selected").val();
            var sort = $('#sort').find(":selected").val();
            
            window.location.replace("/Categories?categoryId=" + id + "&sortOrder=" + sort + "&pageSize=" + pageSize) ;
        });

    $('#manufacturerDiv').on("click",
        ".js-manufacturer-item",
        function () {
            var id = $('input[name=category]:checked').attr("id").split('-')[1];
            var manufacturerId = $(this).attr("id").replace('manufacturer-', '');
            var pageSize = $('#pageSize').find(":selected").val();
            var sort = $('#sort').find(":selected").val();
            window.location.replace("/Categories?categoryId=" + id
                +"&manufacturerId=" + manufacturerId + "&sortOrder=" + sort + "&pageSize=" + pageSize) ;
        });
});