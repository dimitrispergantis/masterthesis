﻿$(document).ready(function () {
    $(document).on('click',
        '.delete-link',
        function () {
            event.preventDefault();
            var tr = $(this).closest('tr');

            tr.addClass("bg-danger");

            tr.fadeOut(500,
                function () {
                    var table = tr.closest('table');
                    tr.remove();
                    updateIndexes(table);
                });
        });

    $("#btnAddProduct").on("click",
        function (event) {
            event.preventDefault();
            addProductsRecord('productsTable');
        });
});

function addProductsRecord(tableId) {
    var rowTemplate =
        '<tr>' +
        '<td class="col-sm-4">' +
        '<input type="text" data-val="true" data-val-required="Παρακαλώ συμπληρώστε το τίτλο του video " id="ProductVideos_{INDEX}__VideoTitle" name="ProductVideos[{INDEX}].VideoTitle" aria-describedby="ProductVideos_{INDEX}__VideoTitle-error" class="valid" aria-invalid="false">' +
        '</td>' +
        '<td class="col-sm-4">' +
        '<input type="text" data-val="true" data-val-required="Παρακαλώ συμπληρώστε το url του video" id="ProductVideos_{INDEX}__VideoId" name="ProductVideos[{INDEX}].VideoId">' +
        '</td>' +
        '<td class="col-sm-2">' +
        '<a class="delete-link"> <i class="fa fa-trash-o admin-icon" aria-hidden="true"></i></a>' +
        '</td>' +
        '</tr>';

    addRecord(tableId, rowTemplate);
}

function addRecord(tableId, rowTemplate) {
    var table = $("#" + tableId);
    var newIndex = table.find("tr").length - 1;

    rowTemplate = rowTemplate.replace(/{INDEX}/g, newIndex);
    var newRow = $(rowTemplate);
    newRow.hide();

    table.append(newRow);
    newRow.addClass("bg-success");
    newRow.fadeIn(500,
        function () {
            newRow.removeClass("bg-success");
        });
    fnValidateDynamicContent(newRow);
}

function fnValidateDynamicContent($element) {
    var $currForm = $element.closest("form");
    $currForm.removeData("validator");
    $currForm.removeData("unobtrusiveValidation");
    $.validator.unobtrusive.parse($currForm);
    $currForm.validate(); // This line is important and added for client side validation to trigger, without this it didn't fire client side errors.
}

// This is where the magic happens
function updateIndexes(table) {
    // get every tr element except the header
    table.find("tr:gt(0)").each(function (i, row) {
        // get every input and select elements
        $(row).find('input, select').each(function (j, input) {
            // check whether the id-attribute is of type _[index]__
            var id = input.id.match(/_\d+__/);

            // if it is an element necessary for the ModelBinder => update the name attribute
            if (id != null && id.length && id.length == 1) {
                var attr = $(input).attr("name");
                // replace the old index of the name attribute with the calculated index
                var newName = attr.replace(attr.match(/\d+/), i);
                $(input).attr("name", newName);
            }
        });
    });
}