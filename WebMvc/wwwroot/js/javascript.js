$(document).ready(function() {
	//************************************************************* DOCUMENT READY START


//************************************************************* MOBILE MENU
function toggle_mobile_menu(){
	$("#mobile-menu-items").stop(true,true).slideToggle(500);
	$("#mobile-menu-overlay").stop(true,true).fadeToggle(500);
	$("#mobile-menu").toggleClass("open");
}
$("#icon-menu").click(function(){
	toggle_mobile_menu();
});
$("#mobile-menu-overlay").click(function(){
	toggle_mobile_menu();
});



//************************************************************* SCROLL
    $(window).scroll(function () {
	if ($(this).scrollTop() > 0) {
		$('#scroll-top').fadeIn();
		$('body').addClass('scrolled');
	} else {
		$('#scroll-top').fadeOut();
		$('body').removeClass('scrolled');
	}
});

//***************************************************************** SUDOSLIDER 
var sudoSlider = $("#slider").sudoSlider({
	responsive:true,
	effect:'slide',
	prevNext:false,
	numeric:false,
	speed:800,
	auto:true,
	pause:5000,
	resumePause:6000,
	continuous:true,
	autowidth:true,
	autoheight:true,
	controlsAttr:'id="slider-controls"',
	prevHtml:'<a href="#" class="slider-arr slider-prev"></a>',
	nextHtml:'<a href="#" class="slider-arr slider-next"></a>'
});

function cat_slider_count(){
	var wh = $(window).width();
	var scount = 3;
	if(wh>768)
		var scount = 4;
	if(wh>992)
		var scount = 5;
	if(wh>1200)
		var scount = 6;
	return scount;
}

var scount = cat_slider_count();
var sudoSlider2 = $("#cat-slider").sudoSlider({
	responsive:true,
	effect:'slide',
	prevNext:true,
	numeric:false,
	speed:300,
	auto:false,
	pause:5000,
	resumePause:6000,
	continuous:true,
	autowidth:true,
	autoheight:true,
	controlsAttr:'id="cat-slider-controls"',
	prevHtml:'<a href="#" class="slider-arr slider-prev"></a>',
	nextHtml:'<a href="#" class="slider-arr slider-next"></a>',
	slideCount:scount
});
$(window).resize(function(){
	scount = cat_slider_count();
	sudoSlider2.setOption('slideCount', scount);
});


$(".scroll").click(function () {
    var id = $(this).attr('data-target');
    var bodyOffset = $('html').scrollTop();
    var targetOffset = $("#" + id).offset().top;
    var transitionTime = (bodyOffset - targetOffset) / 4;
    $('html, body').animate({ scrollTop: targetOffset }, transitionTime);
});



//************************************************************* DOCUMENT READY END
});

