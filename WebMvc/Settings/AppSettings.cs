﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace WebMvc.Settings
{
    public class AppSettings
    {
        public string GatewayUrl { get; set; }

        public string AdminAggregatorUrl { get; set; }
        
    }
}
