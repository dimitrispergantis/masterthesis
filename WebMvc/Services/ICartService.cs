﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebMvc.Models;

namespace WebMvc.Services
{
    public interface ICartService
    {
        Task<SessionCart> GetCart(string customerId);

        Task UpdateCart(Cart cart);

        Task ClearCart(Cart cart);

        Task InitiateCheckout(Cart cart);

        Task<Cart> SetQuantities(string customerId, Dictionary<string, int> quantities);

        Task AddItemToBasket(string customerId, int productId);
    }
}
