﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using WebMvc.Infrastructure;
using WebMvc.Models;
using WebMvc.Settings;

namespace WebMvc.Services
{
    public class CartService: ICartService
    {
        private readonly HttpClient _httpClient;
        private readonly string _remoteServiceBaseUrl;

        public CartService(HttpClient httpClient, IOptions<AppSettings> settings)
        {
            _httpClient = httpClient;
            _remoteServiceBaseUrl = $"{settings.Value.GatewayUrl}/";
        }

        public async Task<SessionCart> GetCart(string customerId)
        {
            var uri = API.Cart.GetCart(_remoteServiceBaseUrl, customerId);

            var responseString = await _httpClient.GetStringAsync(uri);

            return string.IsNullOrEmpty(responseString) ?
                new SessionCart() { CustomerId = customerId } :
                JsonConvert.DeserializeObject<SessionCart>(responseString);
        }

        public async Task UpdateCart(Cart cart)
        {
            var uri = API.Cart.UpdateCart(_remoteServiceBaseUrl);

            var json = JsonConvert.SerializeObject(cart);

            var cartContent = new StringContent(JsonConvert.SerializeObject(cart), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(uri, cartContent);

            response.EnsureSuccessStatusCode();

        }

        public async Task ClearCart(Cart cart)
        {
            var uri = API.Cart.CleanCart(_remoteServiceBaseUrl, cart.CustomerId);

            var response = await _httpClient.DeleteAsync(uri);

            response.EnsureSuccessStatusCode();

        }

        public async Task InitiateCheckout(Cart cart)
        {
            var uri = API.Cart.InitiateCheckout(_remoteServiceBaseUrl);

            var json = JsonConvert.SerializeObject(cart);

            var cartContent = new StringContent(JsonConvert.SerializeObject(cart), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(uri, cartContent);

            response.EnsureSuccessStatusCode();
        }

        public async Task<Cart> SetQuantities(string customerId, Dictionary<string, int> quantities)
        {
            var uri = API.Cart.UpdateCartItem(_remoteServiceBaseUrl);

            var cartUpdate = new
            {
                BasketId = customerId,
                Updates = quantities.Select(kvp => new
                {
                    CartItemId = kvp.Key,
                    NewQty = kvp.Value
                }).ToArray()
            };

            var cartContent = new StringContent(JsonConvert.SerializeObject(cartUpdate), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(uri, cartContent);

            response.EnsureSuccessStatusCode();

            var jsonResponse = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Cart>(jsonResponse);
        }

        public async Task AddItemToBasket(string customerId, int productId)
        {
            var uri = API.Cart.AddItemToCart(_remoteServiceBaseUrl);

            var newItem = new
            {
                ProductId = productId,
                CartItemId = customerId,
                Quantity = 1
            };

            var basketContent = new StringContent(JsonConvert.SerializeObject(newItem), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(uri, basketContent);
        }


    }
}
