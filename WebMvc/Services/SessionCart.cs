﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using WebMvc.Models;
using WebMvc.ViewModels;

namespace WebMvc.Services
{
    public class SessionCart: Cart
    {
        private static ISession _session;
        private static ICartService _service;
        private static string _customerId;

        public static async Task<Cart> GetCart(IServiceProvider services)
        {
            _service = services.GetRequiredService<ICartService>();
            _session = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;

            if ((_customerId = _session.GetString("CustomerId")) == null)
            {
                _customerId = Guid.NewGuid().ToString();
                _session.SetString("CustomerId", _customerId);
            }

            SessionCart cart = await _service.GetCart(_customerId);

            return cart;
        }

        public override async Task AddItem(int productId, int productVersionId, string productName, string photo, int quantity, float price)
        {
            await base.AddItem(productId, productVersionId, productName, photo, quantity, price);

            await _service.UpdateCart(this);

        }

        public override async Task UpdateItemQuantity(int productId, int productVersionId, int quantity)
        {
            await base.UpdateItemQuantity(productId, productVersionId, quantity);

            await _service.UpdateCart(this);
        }

        public override async Task RemoveItem(int productId, int productVersionId)
        {
            await base.RemoveItem(productId,  productVersionId);

            await _service.UpdateCart(this);
        }

        public override async Task Clear()
        {
            await base.Clear();

            await _service.UpdateCart(this);
        }

        public override async Task UpdateCheckout(Checkout checkout)
        {
            await base.UpdateCheckout(checkout);

            await _service.UpdateCart(this);
        }
    }
}
