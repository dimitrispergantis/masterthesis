﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebMvc.Infrastructure;
using WebMvc.Models;
using WebMvc.Settings;
using WebMvc.ViewModels;

namespace WebMvc.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly IOptions<AppSettings> _settings;
        private readonly HttpClient _httpClient;
        private readonly ILogger<CatalogService> _logger;

        private readonly string _remoteServiceBaseUrl;

        public CatalogService(HttpClient httpClient, ILogger<CatalogService> logger, IOptions<AppSettings> settings)
        {
            _httpClient = httpClient;
            _settings = settings;
            _logger = logger;

            _remoteServiceBaseUrl = $"{_settings.Value.GatewayUrl}/";
        }



        public async Task<BrandsViewModel> GetBrandsAsync(string sortOrder, string searchString, int page, int pageSize)
        {
            var uri = API.Catalog.GetAllBrands(_remoteServiceBaseUrl ,sortOrder, searchString, page, pageSize);

            var responseString = await _httpClient.GetStringAsync(uri);

            var brands =  JsonConvert.DeserializeObject<BrandsViewModel>(responseString);;

            return brands;
        }

        public async Task<BrandViewModel> GetBrandAsync(int id)
        {
            var uri = API.Catalog.GetBrandById(_remoteServiceBaseUrl, id);

            var responseString = await _httpClient.GetStringAsync(uri);

            var brand = JsonConvert.DeserializeObject<BrandViewModel>(responseString);

            return brand;
        }

        public async Task CreateBrandAsync(BrandViewModel viewModel)
        {
            var uri = API.Catalog.CreateBrand(_remoteServiceBaseUrl);

            var brandContent = new StringContent(JsonConvert.SerializeObject(viewModel), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(uri, brandContent);

            response.EnsureSuccessStatusCode();
        }

        public async Task UpdateBrandAsync(BrandViewModel viewModel)
        {
            var uri = API.Catalog.UpdateBrand(_remoteServiceBaseUrl, viewModel.Id);

            var brandContent = new StringContent(JsonConvert.SerializeObject(viewModel), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(uri, brandContent);

            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteBrandAsync(int id)
        {
            var uri = API.Catalog.DeleteBrand(_remoteServiceBaseUrl, id);

            var response = await _httpClient.DeleteAsync(uri);

            response.EnsureSuccessStatusCode();
        }

        public async Task<CategoriesViewModel> GetCategoriesAsync(string sortOrder, string searchString, int page, int pageSize)
        {
            var uri = API.Catalog.GetAllCategories(_remoteServiceBaseUrl ,sortOrder, searchString, page, pageSize);

            var responseString = await _httpClient.GetStringAsync(uri);

            var categories =  JsonConvert.DeserializeObject<CategoriesViewModel>(responseString);;

            return categories;
        }

        public async Task<CategoryViewModel> GetCategoryAsync(int id)
        {
            var uri = API.Catalog.GetCategoryById(_remoteServiceBaseUrl, id);

            var responseString = await _httpClient.GetStringAsync(uri);

            var viewModel = JsonConvert.DeserializeObject<CategoryViewModel>(responseString);

            return viewModel;
        }

        public async Task CreateCategoryAsync(CategoryViewModel viewModel)
        {
            var uri = API.Catalog.CreateCategory(_remoteServiceBaseUrl);

            var categoryContent = new StringContent(JsonConvert.SerializeObject(viewModel), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(uri, categoryContent);

            response.EnsureSuccessStatusCode();
        }

        public async Task UpdateCategoryAsync(CategoryViewModel viewModel)
        {
            var uri = API.Catalog.UpdateCategory(_remoteServiceBaseUrl, viewModel.Id);

            var categoryContent = new StringContent(JsonConvert.SerializeObject(viewModel), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(uri, categoryContent);

            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteCategoryAsync(int id)
        {
            var uri = API.Catalog.DeleteCategory(_remoteServiceBaseUrl, id);

            var response = await _httpClient.DeleteAsync(uri);

            response.EnsureSuccessStatusCode();
        }

        public async Task<ProductsViewModel> GetProductsAsync(string sortOrder, string searchString, int page, int pageSize)
        {
            var uri = API.Catalog.GetAllProducts(_remoteServiceBaseUrl, sortOrder, searchString, page, pageSize);

            var responseString = await _httpClient.GetStringAsync(uri);

            var products =  JsonConvert.DeserializeObject<ProductsViewModel>(responseString);;

            return products;
        }

        public async Task<ProductViewModel> GetAvailableDataForProductAdd()
        {
            var uri = API.Catalog.GetAvailableDataForProductAdd(_remoteServiceBaseUrl);

            var responseString = await _httpClient.GetStringAsync(uri);

            var product = JsonConvert.DeserializeObject<ProductViewModel>(responseString);

            return product;
        }

        public async Task<PaginatedProductsViewModel> GetProductData( int page, int pageSize, string sortOrder = null, string searchString = null,
            string categoryId = null, string brandId = null, string minPrice = null, string maxPrice = null)
        {
            var uri = API.Catalog.GetAllProductItems(_remoteServiceBaseUrl, sortOrder, searchString, page, pageSize,
                categoryId, brandId, minPrice, maxPrice);

            var responseString = await _httpClient.GetStringAsync(uri);

            var products = JsonConvert.DeserializeObject<PaginatedProductsViewModel>(responseString);

            return products;
        }

        public async Task<Product> GetProductData(int id)
        {
            var uri = API.Catalog.GetProductItem(_remoteServiceBaseUrl, id);

            var responseString = await _httpClient.GetStringAsync(uri);

            var product = JsonConvert.DeserializeObject<Product>(responseString);

            return product;
        }

        public async Task<ProductViewModel> GetProductDataById(int id)
        {
            var uri = API.Catalog.GetProductById(_remoteServiceBaseUrl, id);

            var responseString = await _httpClient.GetStringAsync(uri);

            var product = JsonConvert.DeserializeObject<ProductViewModel>(responseString);

            return product;
        }

        public async Task<ProductVersionsViewModel> GetProductVersions(int id)
        {
            var uri = API.Catalog.GetProductVersions(_remoteServiceBaseUrl, id);

            var responseString = await _httpClient.GetStringAsync(uri);

            var productVersions = JsonConvert.DeserializeObject<List<ProductVersionViewModel>>(responseString);

            return new ProductVersionsViewModel(){ Id = id, ProductVersionsList = productVersions};
        }

        public async Task CreateProductAsync(ProductViewModel viewModel)
        {
            var uri = API.Catalog.CreateProduct(_remoteServiceBaseUrl);

            var productContent = new StringContent(JsonConvert.SerializeObject(viewModel), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(uri, productContent);

            response.EnsureSuccessStatusCode();
        }

        public async Task UpdateProductAsync(ProductViewModel viewModel)
        {
            var uri = API.Catalog.UpdateProduct(_remoteServiceBaseUrl, viewModel.Id);

            var productContent = new StringContent(JsonConvert.SerializeObject(viewModel), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(uri, productContent);

            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteProductAsync(int id)
        {
            var uri = API.Catalog.DeleteProduct(_remoteServiceBaseUrl, id);

            var response = await _httpClient.DeleteAsync(uri);

            response.EnsureSuccessStatusCode();
        }

        public async Task ConfigureProductVersions(ProductVersionsViewModel viewModel)
        {
            var uri = API.Catalog.ConfigureProductVersions(_remoteServiceBaseUrl, viewModel.Id);

            var versionsContent = new StringContent(JsonConvert.SerializeObject(viewModel), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(uri, versionsContent);

            response.EnsureSuccessStatusCode();
        }

        public async Task<ProductPhotosViewModel> GetProductPhotos(int id)
        {
            var uri = API.Catalog.GetProductPhotos(_remoteServiceBaseUrl, id);

            var responseString = await _httpClient.GetStringAsync(uri);

            var productPictures = JsonConvert.DeserializeObject<List<ProductPictureViewModel>>(responseString);

            return new ProductPhotosViewModel(){Id = id, ProductPicturesList = productPictures};
        }

        public async Task ConfigureProductPhotos(ProductPhotosViewModel viewModel)
        {
            var productPicturesViewModel = new List<ProductPictureBase64ViewModel>();

            if (viewModel.FormFiles != null)
            {
                foreach (var formFile in viewModel.FormFiles)
                {
                    using (var stream = new MemoryStream())
                    {
                        await formFile.CopyToAsync(stream);

                        var bytes = stream.ToArray();
                        var base64String = "data:" + formFile.ContentType + ";base64," + Convert.ToBase64String(bytes);

                        productPicturesViewModel.Add(new ProductPictureBase64ViewModel(){Base64String = base64String, PictureTypeId = viewModel.PhotoType});
                    }
                }
            }

            if (viewModel.ProductPicturesList != null)
            {
                foreach (var picture in viewModel.ProductPicturesList)
                {
                    productPicturesViewModel.Add(new ProductPictureBase64ViewModel(){Id = picture.Id, PictureTypeId = picture.PictureTypeId});
                }
            }

            var configureProductPicturesViewModel = new ConfigureProductPicturesViewModel()
                {ProductPicturesViewModel = productPicturesViewModel};

            var uri = API.Catalog.ConfigureProductPhotos(_remoteServiceBaseUrl, viewModel.Id);

            var serializedString = JsonConvert.SerializeObject(configureProductPicturesViewModel);

            var picturesContent = new StringContent(serializedString, System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(uri, picturesContent);

            response.EnsureSuccessStatusCode();
        }


        public async Task<IEnumerable<ChildCategory>> GetAvailableCategoriesForAdd()
        {
            var uri = API.Catalog.GetAvailableChildrenCategories(_remoteServiceBaseUrl);

            var childrenString = await _httpClient.GetStringAsync(uri);

            var viewModel = JsonConvert.DeserializeObject<IEnumerable<ChildCategory>>(childrenString);

            return viewModel;
        }
    }
}
