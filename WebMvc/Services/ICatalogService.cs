﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebMvc.Models;
using WebMvc.ViewModels;

namespace WebMvc.Services
{
    public interface ICatalogService
    {
        Task<BrandsViewModel> GetBrandsAsync(string sortOrder, string searchString, int page, int pageSize);

        Task<BrandViewModel> GetBrandAsync(int id);

        Task CreateBrandAsync(BrandViewModel viewModel);

        Task UpdateBrandAsync(BrandViewModel viewModel);

        Task DeleteBrandAsync(int id);

        Task<CategoriesViewModel> GetCategoriesAsync(string sortOrder, string searchString, int page, int pageSize);

        Task<CategoryViewModel> GetCategoryAsync(int id);

        Task<IEnumerable<ChildCategory>> GetAvailableCategoriesForAdd();

        Task CreateCategoryAsync(CategoryViewModel viewModel);

        Task UpdateCategoryAsync(CategoryViewModel viewModel);

        Task DeleteCategoryAsync(int id);

        Task<ProductsViewModel> GetProductsAsync(string sortOrder, string searchString, int page, int pageSize);

        Task<ProductViewModel> GetAvailableDataForProductAdd();

        Task<ProductViewModel> GetProductDataById(int id);

        Task<ProductVersionsViewModel> GetProductVersions(int id);

        Task CreateProductAsync(ProductViewModel viewModel);

        Task UpdateProductAsync(ProductViewModel viewModel);

        Task DeleteProductAsync(int id);

        Task ConfigureProductVersions(ProductVersionsViewModel viewModel);

        Task<ProductPhotosViewModel> GetProductPhotos(int id);

        Task ConfigureProductPhotos(ProductPhotosViewModel viewModel);

        Task<PaginatedProductsViewModel> GetProductData( int page, int pageSize, string sortOrder = null, string searchString = null,
            string categoryId = null, string brandId = null, string minPrice = null, string maxPrice = null);

        Task<Product> GetProductData(int id);


    }
}
