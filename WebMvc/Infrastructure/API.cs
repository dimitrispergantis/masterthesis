﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMvc.Infrastructure
{
    public static class API
    {

        public static class Cart
        {
            public static string GetCart(string baseUri, string cartId) => $"{baseUri}cart-api/{cartId}";
            public static string UpdateCart(string baseUri) => $"{baseUri}cart-api/update_cart";
            public static string InitiateCheckout(string baseUri) => $"{baseUri}cart-api/checkout";
            public static string GetCheckoutInfo(string baseUri, string cartId) => $"{baseUri}cart-api/get-checkout-info/{cartId}";
            public static string CleanCart(string baseUri, string cartId) => $"{baseUri}cart-api/{cartId}";
            public static string AddItemToCart(string baseUri) => $"{baseUri}cart-api/items";
            public static string UpdateCartItem(string baseUri) => $"{baseUri}cart-api/items";
        }

        public static class Catalog
        {
            public static string GetAllProductItems(string baseUri, string sortOrder, string searchString, int page, int pageSize,
                string categoryId, string brandId, string minPrice, string maxPrice)
            {
                var filterQs = "";

                if (!string.IsNullOrEmpty(searchString))
                {
                    filterQs = $"?searchString={searchString}";

                }
                else
                {

                    filterQs = $"?page={page}&pageSize={pageSize}" +
                               $"&categoryId={((!string.IsNullOrWhiteSpace(categoryId))? categoryId : String.Empty)}" +
                               $"&brandId={((!string.IsNullOrWhiteSpace(brandId))? brandId: String.Empty)}" +
                               $"&minPrice={((!string.IsNullOrWhiteSpace(minPrice))? minPrice: String.Empty)}"+
                               $"&maxPrice={((!string.IsNullOrWhiteSpace(maxPrice))? maxPrice: String.Empty)}";
                }


                return $"{baseUri}catalog-api/products/full_data" + filterQs;
            }

            public static string GetProductItem(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/products/full_data/{id}";
            }

            public static string GetAllBrands(string baseUri, string sortOrder, string searchString, int page, int pageSize)
            {
                return $"{baseUri}catalog-api/brands?sortOrder={sortOrder}&searchString={searchString}&page={page}&pageSize={pageSize}";
            }

            public static string GetAllTypes(string baseUri)
            {
                return $"{baseUri}catalogTypes";
            }

            public static string CreateBrand(string baseUri)
            {
                return $"{baseUri}catalog-api/brands/create";
            }

            public static string UpdateBrand(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/brands/update/{id}";
            }

            public static string DeleteBrand(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/brands/delete/{id}";
            }

            public static string GetBrandById(string baseUri ,int id)
            {
                return $"{baseUri}catalog-api/brands/{id}";
            }

            public static string GetAllCategories(string baseUri, string sortOrder, string searchString, int page, int pageSize)
            {
                return $"{baseUri}catalog-api/categories?sortOrder={sortOrder}&searchString={searchString}&page={page}&pageSize={pageSize}";
            }

            public static string GetCategoryById(string baseUri, int id)
            {
                return $"{baseUri}aggregator/catalog-api/category/{id}";
            }


            public static string GetAvailableChildrenCategories(string baseUri)
            {
                return $"{baseUri}catalog-api/categories/available-children";
            }

            public static string CreateCategory(string baseUri)
            {
                return $"{baseUri}catalog-api/categories/create";
            }

            public static string UpdateCategory(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/categories/update/{id}";
            }

            public static string DeleteCategory(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/categories/delete/{id}";
            }

            public static string GetAllProducts(string baseUri, string sortOrder, string searchString, int page,
                int pageSize)
            {
                return $"{baseUri}catalog-api/products?sortOrder={sortOrder}&searchString={searchString}&page={page}&pageSize={pageSize}";
            }

            public static string GetAvailableDataForProductAdd(string baseUri)
            {
                return $"{baseUri}aggregator/catalog-api/product";
            }

            public static string GetProductById(string baseUri, int id)
            {
                return $"{baseUri}aggregator/catalog-api/product/{id}";
            }

            public static string CreateProduct(string baseUri)
            {
                return $"{baseUri}catalog-api/products";
            }

            public static string UpdateProduct(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/products/{id}";
            }

            public static string DeleteProduct(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/products/{id}";
            }

            public static string GetProductVersions(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/products/{id}/product-versions";
            }

            public static string ConfigureProductVersions(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/products/{id}/configure-product-versions";
            }

            public static string GetProductPhotos(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/pictures/product/{id}";
            }

            public static string ConfigureProductPhotos(string baseUri, int id)
            {
                return $"{baseUri}catalog-api/pictures/product/{id}";
            }
        }


    }
}
