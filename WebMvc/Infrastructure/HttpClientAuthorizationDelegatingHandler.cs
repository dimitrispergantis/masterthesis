﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace WebMvc.Infrastructure
{
    /// <summary>
    /// Delegating handler o οποίος ενσωματώνει το Bearer Token απο το IdentityApi στο request αυτομάτα πριν την πραγματική κλήση http.
    /// </summary>
    public class HttpClientAuthorizationDelegatingHandler: DelegatingHandler
    {
        private readonly IHttpContextAccessor _accessor;

        public HttpClientAuthorizationDelegatingHandler(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }


        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var authorizationHeader = _accessor.HttpContext.Request.Headers["Authorization"];

            if (!string.IsNullOrWhiteSpace(authorizationHeader))
            {
                request.Headers.Add("Authorization", new List<string>(){authorizationHeader});
            }

            var token = await GetToken();

            if (token != null)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            return await base.SendAsync(request, cancellationToken);
        }

        private async Task<string> GetToken()
        {
            const string accessToken = "access_token";

            return await _accessor.HttpContext.GetTokenAsync(accessToken);
        }
    }
}
