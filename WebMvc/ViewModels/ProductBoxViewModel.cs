﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebMvc.Models;

namespace WebMvc.ViewModels
{
    public class ProductBoxViewModel
    {
        public Product Product { get; set; }

        public IEnumerable<Picture> Media { get; set; }

        public IEnumerable<ProductVersion> Versions { get; set; }

        // ReturnUrl για το delete στο Wishlist
        public string ReturnUrl { get; set; }
    }
}
