﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebMvc.ViewModels
{
    public class ProductPhotosViewModel
    {
        public int Id { get; set; }

        public List<ProductPictureViewModel> ProductPicturesList { get; set; }

        public List<IFormFile> FormFiles { get; set; }

        public byte PhotoType { get; set; }

    }
}
