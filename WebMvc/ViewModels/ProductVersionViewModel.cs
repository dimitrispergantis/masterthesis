﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMvc.ViewModels
{
    public class ProductVersionViewModel
    {
        public int Id { get; set; }

        public string Mpn { get; set; }

        public string Size { get; set; }

        public string Color { get; set; }

        public string Weight { get; set; }

        public float InitialPrice { get; set; }

        public float Discount { get; set; }

        public int AvailableStock { get; set; }

        public int RestockThreshold { get; set; }

        public int MaxStockThreshold { get; set; }
    }
}
