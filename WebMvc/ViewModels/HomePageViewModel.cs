﻿using System.Collections.Generic;
using WebMvc.Models;

namespace WebMvc.ViewModels
{
    public class HomePageViewModel
    {
        public IEnumerable<Product> Products { get; set; }

        public IEnumerable<Category> Categories { get; set; }
    }
}
