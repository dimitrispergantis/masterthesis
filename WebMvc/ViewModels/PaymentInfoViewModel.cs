﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebMvc.Models;

namespace WebMvc.ViewModels
{
    public class PaymentInfoViewModel
    {
        public Cart Cart { get; set; }

        public int PaymentType { get; set; }
    }
}
