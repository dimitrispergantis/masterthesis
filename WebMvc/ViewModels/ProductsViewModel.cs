﻿using System.Collections.Generic;
using WebMvc.Models;

namespace WebMvc.ViewModels
{
    public class ProductsViewModel
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
        public List<ProductSummary> Data { get; set; }
    }
}