﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMvc.ViewModels
{
    public class ProductVersionsViewModel
    {
        public int Id { get; set; }

        public List<ProductVersionViewModel> ProductVersionsList { get; set; }

    }
}
