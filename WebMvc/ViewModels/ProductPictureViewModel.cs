﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMvc.ViewModels
{
    public class ProductPictureViewModel
    {
        public  int Id { get; set; }

        public string ThumbnailPictureUri { get; set; }

        public int PictureTypeId { get; set; }
    }
}
