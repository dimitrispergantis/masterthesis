﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebMvc.Models;

namespace WebMvc.ViewModels
{
    public class ProductPageViewModel
    {
        public Product Product { get; set; }

        public int ProductVersionId { get; set; }

        public byte Quantity { get; set; }
    }
}
