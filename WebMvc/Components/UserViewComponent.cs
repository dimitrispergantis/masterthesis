﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebMvc.Components
{
    public class UserViewComponent: ViewComponent
    {
        private readonly IHttpContextAccessor _accessor;

        public UserViewComponent( IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public async Task<IViewComponentResult> InvokeAsync(bool header = true)
        {
            var user = _accessor.HttpContext.User;

            if (user.Identities.Any(c => c.IsAuthenticated))
            {
                if (header)
                    return View("~/Views/Shared/Components/UserView/SignedIn.cshtml");
                else
                    return View("~/Views/Shared/Components/UserView/SignedInFooter.cshtml");
            }
            else
            {
                if (header)
                    return View("~/Views/Shared/Components/UserView/SignedOut.cshtml");
                else
                    return View("~/Views/Shared/Components/UserView/SignedOutFooter.cshtml");
            }

        }
    }

}
