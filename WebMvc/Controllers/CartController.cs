﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;
using WebMvc.Services;
using WebMvc.ViewModels;

namespace WebMvc.Controllers
{
    public class CartController : Controller
    {

        private readonly ICatalogService _catalogService;
        private readonly Cart _cart;

        public CartController( ICatalogService catalogService, Cart cart)
        {
            _catalogService = catalogService;
            _cart = cart;
        }

        public async Task<RedirectToActionResult> AddToCart(int productId, string returnUrl, int productVersionId, int quantity = 1)
        {
            // Βρίσκω το προϊον απο το productId και productVersionId
            var product = await _catalogService.GetProductData(productId);

            // Βρισκώ την παρούσα έκδοση
            var currentProductVersion = product.Versions.SingleOrDefault(v => v.Id == productVersionId && v.HasStock);

            // Εχω stock οπότε έχει νοήμα να προχωρήσω
            if (currentProductVersion != null)
            {
                await _cart.AddItem(productId, productVersionId, product.Name,
                        product.Pictures.SingleOrDefault(p => p.PictureTypeId == Picture.IsMainPhoto)?.ThumbnailPictureUri,
                        quantity, currentProductVersion.GetPrice );
            }

            return RedirectToAction("Index", new {returnUrl});
        }

        public async Task<RedirectToActionResult> RemoveFromCart(int productId, int productVersionId, string returnUrl)
        {
            var cartLine = _cart.LineCollection.SingleOrDefault(l => l.ProductId == productId && l.ProductVersionId == productVersionId);

            if (cartLine != null)
            {
                await _cart.RemoveItem(cartLine.ProductId, cartLine.ProductVersionId);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        public async Task<RedirectToActionResult> UpdateCart(IFormCollection formCollection, string returnUrl)
        {
            // Για κάθε αντικείμενο στο καλάθι ενημερώνω τον αριθμο των προϊλοντων.
            foreach (var line in _cart.LineCollection)
            {
                int newQuantity = int.Parse(formCollection[String.Concat(line.ProductId,line.ProductVersionId)]);
                await _cart.UpdateItemQuantity(line.ProductId, line.ProductVersionId, newQuantity);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToActionResult ClearCart(string returnUrl)
        {
            _cart.Clear();

            return RedirectToAction("Index", new {returnUrl});
        }


        public IActionResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = _cart,
                ReturnUrl = returnUrl
            });
        }
    }
}