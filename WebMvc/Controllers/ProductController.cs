﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebMvc.Services;
using WebMvc.ViewModels;

namespace WebMvc.Controllers
{
    public class ProductController : Controller
    {
        private readonly ICatalogService _service;

        public ProductController(ICatalogService service)
        {
            _service = service;
        }

        public async Task<IActionResult> Index(int id, int productVersionId)
        {
            var product = await _service.GetProductData(id);

            return View(new ProductPageViewModel {ProductVersionId = productVersionId, Product = product, Quantity = 1});
        }
    }
}