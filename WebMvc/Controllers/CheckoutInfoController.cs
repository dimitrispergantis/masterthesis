﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebMvc.Infrastructure;
using WebMvc.Models;
using WebMvc.ViewModels;

namespace WebMvc.Controllers
{
    public class CheckoutInfoController : Controller
    {
        private readonly Cart _cart;

        public CheckoutInfoController(Cart cart)
        {
            _cart = cart;
        }

        public IActionResult Index()
        {
            return View(_cart.Checkout);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> AddCheckoutInfo(Checkout checkout)
        {
            // Εαν ο ελέγχος δεν περάσει απο τον server επέστρεψε την φόρμα με τα δεδομένα της μεταβλητής στο Session
            if (!ModelState.IsValid)
            {
                return View("Index", checkout);

            }

            await _cart.UpdateCheckout(checkout);

            return RedirectToAction("Index", "PaymentInfo");
        }




    }
}