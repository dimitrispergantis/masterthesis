﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;
using WebMvc.Services;
using WebMvc.ViewModels;

namespace WebMvc.Controllers
{
    public class OrderController : Controller
    {
        private readonly Cart _cart;
        private readonly ICartService _cartService;

        public OrderController(Cart cart, ICartService cartService)
        {
            _cart = cart;
            _cartService = cartService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> AddOrder(byte paymentType)
        {

            if (!_cart.LineCollection.Any())
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
                return RedirectToAction("Error", "Home");
            }

            _cart.RequestId = Guid.NewGuid();
            _cart.PaymentType = paymentType;

            await _cartService.InitiateCheckout(_cart);

            if (paymentType == 2)
            {
                return RedirectToAction("Paypal");
            }

            return RedirectToAction("Index");
        }

        public IActionResult Paypal()
        {
            return View("Paypal");
        }

    }
}