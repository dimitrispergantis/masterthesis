﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;
using WebMvc.Services;
using WebMvc.ViewModels;

namespace WebMvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICatalogService _service;

        public HomeController(ICatalogService service)
        {
            _service = service;
        }

        public async Task<IActionResult> Index()
        {
            var paginatedProducts = await _service.GetProductData(1, 100);
            var paginatedCategories = await _service.GetCategoriesAsync(null, null, 1, 100);

            HomePageViewModel viewModel = new HomePageViewModel()
            {
                Products = paginatedProducts.Data,
                Categories = paginatedCategories.Data,
            };

            return View(viewModel);
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult PageNotFound()
        {
            return View();
        }

        public IActionResult NotFoundView()
        {
            Response.StatusCode = 404;
            return View("PageNotFound");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}