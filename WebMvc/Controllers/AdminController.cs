﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;
using WebMvc.Services;
using WebMvc.ViewModels;

namespace WebMvc.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private readonly ICatalogService _service;

        public AdminController(ICatalogService service)
        {
            _service = service;
        }

        public IActionResult AddBrand()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBrandAsync(BrandViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("AddBrand", viewModel);
            }
            try
            {
                await _service.CreateBrandAsync(viewModel);
                TempData["message"] = $"O κατασκευαστής εισήχθηκε επιτυχώς.";

            }
            catch(Exception e)
            {
                TempData["error"] = $"H προσθήκη brand απέτυχε.";
            }

            try
            {

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return RedirectToAction("Brands");
        }

        public async Task<IActionResult> Brands(string sortOrder, string searchString, int page = 1, int pageSize = 30)
        {
           // Κρατάω την σειρα ταξινόμησης ωστε οταν αλλάζω σελίδα να παραμένει η συγκεκριμένη ταξινόμηση
            // μέχρι ο χρήστης να πατήσει κάποιο κουμπι αλλάγης ταξινόμήσης
            ViewData["CurrentSort"] = sortOrder;

            // Κάθε φορά που μπαίνω στον controller κρατάω σαν είσοδο την αντιθετη sorting παράμετρο
            // ωστε στο επόμενο κλικ να εισάγω την καινούργια παράμετρο ταξινόμησης
            ViewData["CurrentSort"] = String.IsNullOrEmpty(sortOrder) ? "Title_desc" : "";

            // Κρατάω το παρόν query αναζήτησης σε περίπτωση που έχω παραπάνω μιας σελίδας
            ViewData["CurrentFilter"] = searchString;

            // Εαν δεν έχω sort order τοτέ είμαι στην default περίπτωση.
            if (string.IsNullOrEmpty(sortOrder))
            {
                sortOrder = "Title";
                ViewData["CurrentSort"] = sortOrder;
            }

            // Διαφορετικα κοιτάω το επίθεμα desc και πράττω αναλόγως θέτοντας την μεταβλητή descenting.
            if (sortOrder.EndsWith("_desc"))
            {
                ViewData["CurrentSort"] = sortOrder;
            }

            var brands = await _service.GetBrandsAsync(sortOrder, searchString, page, pageSize);

            return View(brands);
        }

        public async Task<IActionResult> EditBrand(int id)
        {
            var brandViewModel = await _service.GetBrandAsync(id);

            return View("EditBrand", brandViewModel);
        }

        public async Task<IActionResult> UpdateBrandAsync(BrandViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("EditBrand", viewModel);
            }
            try
            {
                 await _service.UpdateBrandAsync(viewModel);
                 TempData["message"] = $"O κατασκεύαστης ενημερώθηκε επιτυχώς.";
            }
            catch(Exception e)
            {
                TempData["error"] = $"H ενημέρωση του κατασκεύαστη απέτυχε.";
            }

            return RedirectToAction("Brands");
        }

        public async Task<IActionResult> DeleteBrandAsync(int id)
        {
            try
            {
                await _service.DeleteBrandAsync(id);
                TempData["message"] = $"O κατασκεύαστης διαγράφηκε επιτυχώς.";
            }
            catch (Exception e)
            {
                TempData["error"] = $"H διαγραφή του κατασκεύαστη απέτυχε.";
            }

            return RedirectToAction("Brands");
        }

        public async Task<ActionResult> Categories(string sortOrder, string searchString, int page = 1, int pageSize = 30 )
        {

            // Κρατάω την σειρα ταξινόμησης ωστε οταν αλλάζω σελίδα να παραμένει η συγκεκριμένη ταξινόμηση
            // μέχρι ο χρήστης να πατήσει κάποιο κουμπι αλλάγης ταξινόμήσης
            ViewData["CurrentSort"] = sortOrder;

            // Κάθε φορά που μπαίνω στον controller κρατάω σαν είσοδο την αντιθετη sorting παράμετρο
            // ωστε στο επόμενο κλικ να εισάγω την καινούργια παράμετρο ταξινόμησης
            ViewData["NameSortParam"] = String.IsNullOrEmpty(sortOrder) ? "Title_desc" : "";

            // Κρατάω το παρόν query αναζήτησης σε περίπτωση που έχω παραπάνω μιας σελίδας
            ViewData["CurrentFilter"] = searchString;

            // Εαν δεν έχω sort order τοτέ είμαι στην default περίπτωση.
            if (string.IsNullOrEmpty(sortOrder))
            {
                sortOrder = "Title";
                ViewData["CurrentSort"] = sortOrder;
            }

            // Διαφορετικα κοιτάω το επίθεμα desc και πράττω αναλόγως θέτοντας την μεταβλητή descenting.
            if (sortOrder.EndsWith("_desc"))
            {
                ViewData["CurrentSort"] = sortOrder;
            }

            var categories = await _service.GetCategoriesAsync(sortOrder, searchString, page, pageSize);

            return View(categories);
        }

        public async Task<IActionResult> AddCategory()
        {
            ViewBag.AvailableChildren = await _service.GetAvailableCategoriesForAdd();

            return View(new CategoryViewModel());
        }

        public async Task<IActionResult> EditCategory(int id)
        {
            var category = await _service.GetCategoryAsync(id);

            // Κάνω populate το dropdown list με τις διαθέσιμες κατηγορίες παιδιά, εξαίρω τον ευατό της στην περίπτωση που είναι η ίδια κατα το edit.
            ViewBag.AvailableChildren = category.AvailableChildren;

            return View(category);
        }

        public async Task<IActionResult> DeleteCategory(int id)
        {
            try
            {
                await _service.DeleteCategoryAsync(id);
                TempData["message"] = $"H κατηγορία διαγραφηκε επιτυχώς.";
            }
            catch (Exception e)
            {
                TempData["error"] = $"H διαγραφή της κατηγορίας απέτυχε.";
            }

            return RedirectToAction("Categories");
        }

        public async Task<IActionResult> UpdateCategoryAsync(CategoryViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("EditCategory", viewModel);
            }
            try
            {
                await _service.UpdateCategoryAsync(viewModel);
                TempData["message"] = $"H κατηγορία ενημερώθηκε επιτυχώς.";
            }
            catch(Exception e)
            {
                TempData["error"] = $"H ενημέρωση της κατηγορίας απέτυχε.";
            }

            return RedirectToAction("Categories");

        }

        public async Task<IActionResult> AddCategoryAsync(CategoryViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("AddCategory", viewModel);
            }
            try
            {
                await _service.CreateCategoryAsync(viewModel);
                TempData["message"] = $"H κατηγορία προστέθηκε επιτυχώς.";
            }
            catch(Exception e)
            {
                TempData["error"] = $"H προσθήκη της κατηγορίας απέτυχε.";
            }

            return RedirectToAction("Categories");
        }


        public async Task<IActionResult> Products(string sortOrder, string searchString, int page = 1, int pageSize = 30)
        {

            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParam"] = String.IsNullOrEmpty(sortOrder) ? "Title_desc" : "";
            ViewData["CurrentFilter"] = searchString;

            if (string.IsNullOrEmpty(sortOrder))
            {
                sortOrder = "Title";
                ViewData["CurrentSort"] = sortOrder;
            }

            if (sortOrder.EndsWith("_desc"))
            {
                ViewData["CurrentSort"] = sortOrder;
            }

            var products = await _service.GetProductsAsync(sortOrder, searchString, page, pageSize);

            return View(products);
        }

        public async Task<IActionResult> AddProduct()
        {
            var data = await _service.GetAvailableDataForProductAdd();

            return View(data);
        }

        public async Task<IActionResult> AddProductAsync(ProductViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("AddProduct", viewModel);
            }
            try
            {
                await _service.CreateProductAsync(viewModel);
                TempData["message"] = $"To προϊον προστέθηκε επιτυχώς.";
            }
            catch(Exception e)
            {
                TempData["error"] = $"H προσθήκη του προίοντος απέτυχε.";
            }

            return RedirectToAction("Products");
        }

        public async Task<IActionResult> EditProduct(int id)
        {
            var viewModel = await _service.GetProductDataById(id);;

            return View(viewModel);
        }

        public async Task<IActionResult> UpdateProductAsync(ProductViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("EditProduct", viewModel);
            }
            try
            {
                await _service.UpdateProductAsync(viewModel);
                TempData["message"] = $"To προϊον ενημερώθηκε επιτυχώς.";
            }
            catch(Exception e)
            {
                TempData["error"] = $"H ενημέρωση του προίοντος απέτυχε.";
            }

            return RedirectToAction("Products");
        }

        public async Task<IActionResult> ProductVersions(int id)
        {
            return View(await _service.GetProductVersions(id));
        }

        public async Task<IActionResult> DeleteProduct(int id)
        {
            try
            {
                await _service.DeleteProductAsync(id);
                TempData["message"] = $"To προϊον διαγραφηκε επιτυχώς.";
            }
            catch (Exception e)
            {
                TempData["error"] = $"To προϊον της κατηγορίας απέτυχε.";
            }

            return RedirectToAction("Products");
        }

        public async Task<IActionResult> SaveProductVersions(ProductVersionsViewModel viewModel)
        {
            try
            {
                await _service.ConfigureProductVersions(viewModel);
                TempData["message"] = $"Οι εκδόσεις του προϊοντος ενημερώθηκαν σωστά.";
            }
            catch (Exception e)
            {
                TempData["error"] = $"Οι εκδόσεις του προϊοντος απέτυχαν να αποθηκευτούν.";
            }

            return RedirectToAction("Products");
        }

        public async Task<IActionResult> ProductPhotos(int id)
        {
            return View(await _service.GetProductPhotos(id));
        }

        public async Task<IActionResult> SaveProductPhotos(ProductPhotosViewModel viewModel)
        {
            try
            {
                await _service.ConfigureProductPhotos(viewModel);
                TempData["message"] = $"Οι φωτογραφίες του προϊοντος ενημερώθηκαν σωστά.";
            }
            catch (Exception e)
            {
                TempData["error"] = $"Οι φωτογραφίες του προϊοντος απέτυχαν να αποθηκευτούν.";
            }

            return RedirectToAction("Products");
        }

        [Authorize]
        public IActionResult Orders()
        {
            return View();
        }

        public IActionResult EditOrder()
        {
            throw new NotImplementedException();
        }
    }
}