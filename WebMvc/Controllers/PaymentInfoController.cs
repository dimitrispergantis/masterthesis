﻿using System;
using Microsoft.AspNetCore.Mvc;
using WebMvc.Models;
using WebMvc.ViewModels;

namespace WebMvc.Controllers
{
    public class PaymentInfoController : Controller
    {
        private readonly Cart _cart;

        public PaymentInfoController(Cart cart)
        {
            _cart = cart;
        }

        public IActionResult Index()
        {
            return View(new PaymentInfoViewModel
            {
                Cart = _cart
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddPaymentInformation(PaymentInfoViewModel viewModel)
        {
            // Ελέγχουμε την εγγυρότητα των δεδομένων. Ο χρήστης είναι υποχρεώμένος να συμπληρώσει τρόπο πληρώμης
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("AddOrder","Order", new {paymentType = viewModel.PaymentType});

        }
    }
}