﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ordering.Domain.Aggregates.OrderAggregate;
using Ordering.Domain.SeedWork;

namespace Ordering.Domain.Interfaces
{
    public interface IOrderRepository: IRepository<Order>
    {
        Task<Order> AddOrderAsync(Order order);

        void UpdateOrder(Order order);

        Task<Order> GetOrderAsync(int orderId);
    }
}
