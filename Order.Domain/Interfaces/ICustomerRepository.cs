﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ordering.Domain.Aggregates.CustomerAggregate;
using Ordering.Domain.SeedWork;

namespace Ordering.Domain.Interfaces
{
    public interface ICustomerRepository: IRepository<Customer>
    {
        Task<Customer> AddCustomerAsync(Customer customer);
        Customer UpdateCustomer(Customer customer);
        Task<Customer> FindCustomerAsync(string customerIdentityGuid);
        Task<Customer> FindCustomerByIdAsync(string id);
    }

}
