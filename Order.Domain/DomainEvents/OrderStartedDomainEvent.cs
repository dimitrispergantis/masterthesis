﻿using System;
using MediatR;
using Ordering.Domain.Aggregates.OrderAggregate;

namespace Ordering.Domain.DomainEvents
{
    /// <summary>
    /// Event used when an order is created
    /// </summary>
    public class OrderStartedDomainEvent : INotification
    {
        public string CustomerId { get; }

        public string FirstName { get; }

        public string LastName { get; }

        public string Email { get;  }

        public string PhoneNumber { get; }

        public string CompanyName { get;  }

        public Order Order { get; }

        public OrderStartedDomainEvent(Order order, string customerId, string firstName, string lastName, string email, string phoneNumber, string companyName)
        {
            Order = order;
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            PhoneNumber = phoneNumber;
            CompanyName = companyName;
        }
    }
}
