﻿using MediatR;
using Ordering.Domain.Aggregates.OrderAggregate;

namespace Ordering.Domain.DomainEvents
{
    public class OrderCancelledDomainEvent : INotification
    {
        public Order Order { get; }

        public OrderCancelledDomainEvent(Order order)
        {
            Order = order;
        }
    }
}
