﻿using MediatR;
using Ordering.Domain.Aggregates.OrderAggregate;

namespace Ordering.Domain.DomainEvents
{
    public class OrderShippedDomainEvent : INotification
    {
        public Order Order { get; }

        public OrderShippedDomainEvent(Order order)
        {
            Order = order;           
        }
    }
}
