﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Ordering.Domain.DomainEvents
{
    public class CustomerRegisteredToOrderDomainEvent: INotification
    {
        public int CustomerId { get; }

        public int OrderId { get; }

        public CustomerRegisteredToOrderDomainEvent(int orderId, int customerId)
        {
            OrderId = orderId;
            CustomerId = customerId;
        }
    }
}
