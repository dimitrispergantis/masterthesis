﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ordering.Domain.Aggregates.OrderAggregate;
using Ordering.Domain.DomainEvents;
using Ordering.Domain.SeedWork;

namespace Ordering.Domain.Aggregates.CustomerAggregate
{
    public class Customer : Entity, IAggregateRoot
    {
        public string IdentityGuid { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string Email { get;  private set; }

        public string PhoneNumber { get; private set; }

        public string CompanyName { get; private set; }

        public Customer()
        {

        }

        public Customer(string identity, string firstName, string lastName, string email, string phoneNumber, string companyName)
        {
            IdentityGuid = !string.IsNullOrWhiteSpace(identity) ? identity : throw new ArgumentNullException(nameof(identity));
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
            CompanyName = companyName;
            Email = email;

        }

        public void BindCustomerToOrder(int orderId)
        {
            AddDomainEvent(new CustomerRegisteredToOrderDomainEvent(orderId,Id));
        }

    }
}
