﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ordering.Domain.Aggregates.CustomerAggregate;
using Ordering.Domain.Exceptions;
using Ordering.Domain.SeedWork;

namespace Ordering.Domain.Aggregates.OrderAggregate
{
    public class PaymentMethod
        : Enumeration
    {
        public static PaymentMethod PayOnDeliver = new PaymentMethod(1, nameof(PayOnDeliver).ToLowerInvariant());
        public static PaymentMethod Paypal = new PaymentMethod(2, nameof(Paypal).ToLowerInvariant());

        public PaymentMethod(int id, string name)
            : base(id, name)
        {
        }

        public static IEnumerable<PaymentMethod> List() =>
            new[] { PayOnDeliver, Paypal };

        public static PaymentMethod FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
            {
                throw new OrderDomainException($"Possible values for PaymentMethod: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        public static PaymentMethod From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new OrderDomainException($"Possible values for PaymentMethod: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }
    }
}
