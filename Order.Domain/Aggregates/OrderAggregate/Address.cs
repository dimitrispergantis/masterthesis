﻿using System;
using System.Collections.Generic;
using Ordering.Domain.SeedWork;

namespace Ordering.Domain.Aggregates.OrderAggregate
{
    public class Address : ValueObject
    {

        public string AddressName { get; private set; }

        public string AddressAdditionalInfo { get; private set; }

        public string AddressCity { get; private set; }

        public string AddressState { get; private set; }

        public string AddressCountry { get; private set; }

        public string ZipCode { get; private set; }

        public Address(string addressName, string addressAdditionalInfo, string addressCity, string addressState, string addressCountry, string zipCode)
        {
            AddressName = addressName;
            AddressAdditionalInfo = addressAdditionalInfo;
            AddressCity = addressCity;
            AddressState = addressState;
            AddressCountry = addressCountry;
            ZipCode = zipCode;
        }


        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return AddressName;
            yield return AddressAdditionalInfo;
            yield return AddressCity;
            yield return AddressState;
            yield return AddressCountry;
            yield return ZipCode;
        }
    }
}
