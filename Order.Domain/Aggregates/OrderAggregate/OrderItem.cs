﻿using Ordering.Domain.Exceptions;
using Ordering.Domain.SeedWork;

namespace Ordering.Domain.Aggregates.OrderAggregate
{
    public class OrderItem : Entity
    {

        public int ProductId { get; private set; }

        public int ProductVersionId { get; private set; }

        public string ProductName { get; private set; }

        public int Quantity { get; private set; }

        public float Price { get; private set; }

        protected OrderItem()
        {

        }

        public OrderItem(int productId, int productVersionId, string productName, float price, int quantity = 1)
        {
            ProductId = productId;
            ProductVersionId = productVersionId;
            ProductName = productName;
            Price = price;
            Quantity = quantity;
        }


        public void AddUnits(int units)
        {
            if (units < 0)
            {
                throw new OrderDomainException("Invalid units");
            }

            Quantity += units;
        }
    }
}
