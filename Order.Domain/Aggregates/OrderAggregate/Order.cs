﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ordering.Domain.DomainEvents;
using Ordering.Domain.Exceptions;
using Ordering.Domain.SeedWork;

namespace Ordering.Domain.Aggregates.OrderAggregate
{
    public class Order : Entity, IAggregateRoot
    {
        private DateTime _orderDate;

        public Address Address { get; private set; }

        public int? CustomerId { get; private set; }

        private int _orderStatusId;

        public OrderStatus OrderStatus { get; private set; }

        private readonly List<OrderItem> _orderItems;
        public IReadOnlyCollection<OrderItem> OrderItems => _orderItems;

        private int _paymentMethodId;

        public PaymentMethod PaymentMethod { get; private set; }

        protected Order() {
            _orderItems = new List<OrderItem>();
        }

        public Order(string customerIdHash, string firstName, string lastName, string email, string phoneNumber, string companyName,
            Address address, int paymentMethodId, int? customerId = null) : this()
        {
            _orderDate = DateTime.UtcNow;
            Address = address;
            SetCustomerId(customerId);
            SetPaymentMethodId(paymentMethodId);
            SetSubmittedStatus();

            AddOrderStartedDomainEvent(customerIdHash, firstName, lastName, email,  phoneNumber, companyName);
        }

        public void AddOrderItem(int productId, int productVersionId, string productName, float price, int quantity = 1)
        {
            var orderItem = new OrderItem(productId, productVersionId, productName, price, quantity);

            _orderItems.Add(orderItem);

        }

        public void SetPaymentMethodId(int id)
        {
            _paymentMethodId = PaymentMethod.From(id).Id;
        }

        public void SetCustomerId(int? id)
        {
            CustomerId = id;
        }

        private void SetSubmittedStatus()
        {
            _orderStatusId = OrderStatus.Submitted.Id;
        }

        public void SetAwaitingValidationStatus()
        {
            if (_orderStatusId == OrderStatus.Submitted.Id)
            {
                 AddDomainEvent(new OrderStatusChangedToAwaitingValidationDomainEvent(Id, _orderItems));
                _orderStatusId = OrderStatus.AwaitingValidation.Id;
            }
        }

        public void SetStockConfirmedStatus()
        {
            if (_orderStatusId == OrderStatus.AwaitingValidation.Id)
            {
                AddDomainEvent(new OrderStatusChangedToStockConfirmedDomainEvent(Id));

                _orderStatusId = OrderStatus.StockConfirmed.Id;

            }
        }

        public void SetPaidStatus()
        {
            if (_orderStatusId == OrderStatus.StockConfirmed.Id)
            {
                AddDomainEvent(new OrderStatusChangedToPaidDomainEvent(Id, OrderItems));

                _orderStatusId = OrderStatus.Paid.Id;

            }
        }

        public void SetShippedStatus()
        {
            if (_orderStatusId != OrderStatus.Paid.Id)
            {
                StatusChangeException(OrderStatus.Shipped);
            }

            _orderStatusId = OrderStatus.Shipped.Id;

            AddDomainEvent(new OrderShippedDomainEvent(this));
        }

        public void SetCancelledStatus()
        {
            if (_orderStatusId == OrderStatus.Paid.Id ||
                _orderStatusId == OrderStatus.Shipped.Id)
            {
                StatusChangeException(OrderStatus.Cancelled);
            }

            _orderStatusId = OrderStatus.Cancelled.Id;

            AddDomainEvent(new OrderCancelledDomainEvent(this));
        }

        public void SetCancelledStatusWhenStockIsRejected(IEnumerable<int> orderStockRejectedItems)
        {
            if (_orderStatusId == OrderStatus.AwaitingValidation.Id)
            {
                _orderStatusId = OrderStatus.Cancelled.Id;

                AddDomainEvent(new OrderCancelledDomainEvent(this));

            }
        }

        private void AddOrderStartedDomainEvent(string customerId, string firstName, string lastName, string email, string phoneNumber, string companyName)
        {
            var orderStartedDomainEvent = new OrderStartedDomainEvent(this, customerId, firstName, lastName, email,  phoneNumber, companyName);

            this.AddDomainEvent(orderStartedDomainEvent);
        }

        private void StatusChangeException(OrderStatus orderStatusToChange)
        {
            throw new OrderDomainException($"Is not possible to change the order status from {OrderStatus.Name} to {orderStatusToChange.Name}.");
        }

        public float GetTotal()
        {
            return _orderItems.Sum(o => o.Quantity * o.Price);
        }

    }
}

