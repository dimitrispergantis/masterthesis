﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Ordering.Domain.Aggregates.OrderAggregate;
using Ordering.Domain.Interfaces;
using Ordering.Domain.SeedWork;
using Ordering.Infrastructure.Repositories.Base;
using Ordering.Infrastructure.Specifications;

namespace Ordering.Infrastructure.Repositories
{
    public class OrderRepository: Repository<Order>, IOrderRepository
    {
        public OrderRepository(OrderContext context) : base(context)
        {
        }

        public async Task<Order> AddOrderAsync(Order order)
        {
            await AddAsync(order);

            return order;
        }

        public void UpdateOrder(Order order)
        {
            Update(order);
        }

        public async Task<Order> GetOrderAsync(int orderId)
        {
            var order = await _context.Orders.FindAsync(orderId);
            if (order != null)
            {
                await _context.Entry(order)
                    .Collection(i => i.OrderItems).LoadAsync();
                await _context.Entry(order)
                    .Reference(i => i.OrderStatus).LoadAsync();
                await _context.Entry(order)
                    .Reference(i => i.Address).LoadAsync();
                await _context.Entry(order)
                    .Reference(i => i.PaymentMethod).LoadAsync();
            }

            return order;
        }
    }
}
