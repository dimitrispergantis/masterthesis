﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Ordering.Domain.Aggregates.CustomerAggregate;
using Ordering.Domain.Interfaces;
using Ordering.Infrastructure.Repositories.Base;

namespace Ordering.Infrastructure.Repositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(OrderContext context) : base(context)
        {
        }

        public async Task<Customer> AddCustomerAsync(Customer customer)
        {
            await AddAsync(customer);

            return customer;
        }

        public Customer UpdateCustomer(Customer customer)
        {
            Update(customer);

            return customer;
        }

        public async Task<Customer> FindCustomerAsync(string customerIdentityGuid)
        {
            return await _context.Customers.SingleOrDefaultAsync(b => b.IdentityGuid == customerIdentityGuid);
        }

        public async Task<Customer> FindCustomerByIdAsync(string id)
        {
            return await _context.Customers.FindAsync(int.Parse(id));
        }
    }
}
