﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Ordering.Domain.Aggregates.CustomerAggregate;
using Ordering.Domain.Aggregates.OrderAggregate;

namespace Ordering.Infrastructure.EntityConfigurations
{
    class OrderEntityTypeConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Orders");

            builder.HasKey(o => o.Id);

            builder.Ignore(b => b.DomainEvents);

            builder.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("orderseq");

            builder.OwnsOne(o => o.Address);

            builder.Property<DateTime>("OrderDate").IsRequired();
            builder.Property<int?>("CustomerId").IsRequired(false);
            builder.Property<int>("OrderStatusId").IsRequired();
            builder.Property<int>("PaymentMethodId").IsRequired();

           builder.Metadata.FindNavigation(nameof(Order.OrderItems)).SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.Metadata.FindNavigation(nameof(PaymentMethod)).SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.Metadata.FindNavigation(nameof(OrderStatus)).SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.HasOne(o => o.PaymentMethod)
                .WithMany()
                .HasForeignKey("PaymentMethodId");

            builder.HasOne<Customer>()
                .WithMany()
                .IsRequired(false)
                .HasForeignKey("CustomerId");

            builder.HasOne(o => o.OrderStatus)
                .WithMany()
                .HasForeignKey("OrderStatusId");
        }
    }
}
