﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ordering.Domain.Aggregates.OrderAggregate;

namespace Ordering.Infrastructure.EntityConfigurations
{
    public class PaymentMethodEntityTypeConfiguration: IEntityTypeConfiguration<PaymentMethod>
    {
        public void Configure(EntityTypeBuilder<PaymentMethod> builder)
        {
            builder.ToTable("PaymentMethod");

            builder.HasKey(o => o.Id);

            builder.Property(o => o.Id).HasDefaultValue(1).ValueGeneratedNever().IsRequired();

            builder.Property(o => o.Name).HasMaxLength(200).IsRequired();
        }
    }
}
