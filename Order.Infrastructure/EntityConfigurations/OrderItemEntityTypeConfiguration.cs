﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ordering.Domain.Aggregates.OrderAggregate;


namespace Ordering.Infrastructure.EntityConfigurations
{
    class OrderItemEntityTypeConfiguration
        : IEntityTypeConfiguration<OrderItem>
    {
        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.ToTable("OrderItems");

            builder.HasKey(o => o.Id);

            builder.Ignore(b => b.DomainEvents);

            builder.Property(o => o.Id).ForSqlServerUseSequenceHiLo("orderitemseq");

            builder.Property<int>("OrderId").IsRequired();

            builder.Property<int>("ProductId").IsRequired();

            builder.Property<int>("ProductVersionId").IsRequired();

            builder.Property<float>("Price").IsRequired();

            builder.Property<int>("Quantity").IsRequired();

        }
    }
}
