﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ordering.Domain.Aggregates.CustomerAggregate;

namespace Ordering.Infrastructure.EntityConfigurations
{
    public class CustomerEntityTypeConfiguration: IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customers");

            builder.HasKey(o => o.Id);

            builder.Ignore(b => b.DomainEvents);

            builder.Property(o => o.Id).ForSqlServerUseSequenceHiLo("customerseq");

            builder.Property(b => b.IdentityGuid)
                .HasMaxLength(200)
                .IsRequired();

            builder.HasIndex("IdentityGuid")
                .IsUnique(true);

            builder.Property<string>("FirstName").IsRequired();
            builder.Property<string>("LastName").IsRequired();
            builder.Property<string>("Email").IsRequired();
            builder.Property<string>("PhoneNumber").IsRequired();
            builder.Property<string>("CompanyName").IsRequired();

        }
    }
}
