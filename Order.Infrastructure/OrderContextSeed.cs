﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Ordering.Domain.Aggregates.OrderAggregate;
using Ordering.Domain.SeedWork;
using Polly;

namespace Ordering.Infrastructure
{
    public class OrderContextSeed
    {
        public  async Task SeedAsync(OrderContext context, ILogger<OrderContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(OrderContextSeed));

            await policy.Execute(async () =>
            {
                using (context)
                {
                    context.Database.Migrate();

                    if (!context.OrderStatus.Any())
                    {
                        context.OrderStatus.AddRange(GetPredefinedOrderStatuses());

                        await context.SaveChangesAsync();
                    }

                    if (!context.PaymentsMethods.Any())
                    {
                        context.PaymentsMethods.AddRange(GetPredefinedPaymentsMethodsExtensions());
                    }

                    await context.SaveChangesAsync();
                }
            });
        }

        private IEnumerable<OrderStatus> GetPredefinedOrderStatuses()
        {
            return Enumeration.GetAll<OrderStatus>();
        }

        private IEnumerable<PaymentMethod> GetPredefinedPaymentsMethodsExtensions()
        {
            return Enumeration.GetAll<PaymentMethod>();
        }

        private Policy CreatePolicy( ILogger<OrderContextSeed> logger, string prefix, int retries =3)
        {
            return Policy.Handle<SqlException>().
                WaitAndRetry(
                    retryCount: retries,
                    sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
                    onRetry: (exception, timeSpan, retry, ctx) =>
                    {
                        logger.LogWarning(exception, "[{prefix}] Exception {ExceptionType} with message {Message} detected on attempt {retry} of {retries}", prefix, exception.GetType().Name, exception.Message, retry, retries);
                    }
                );
        }
    }
}
