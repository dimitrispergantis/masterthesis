﻿using EventLogEntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Ordering.Infrastructure.IntegrationEvents
{
    public class IntegrationEventLogContextDesignTimeFactory : IDesignTimeDbContextFactory<IntegrationEventLogContext>
    {
        public IntegrationEventLogContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<IntegrationEventLogContext>();

            optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=OrderDatabase;Trusted_Connection=True;MultipleActiveResultSets=true",
                x => x.MigrationsAssembly("Ordering.Infrastructure"));

            return new IntegrationEventLogContext(optionsBuilder.Options);
        }
    }
}