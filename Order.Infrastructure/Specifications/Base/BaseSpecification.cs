﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Ordering.Domain.SeedWork;

namespace Ordering.Infrastructure.Specifications.Base
{
    public abstract class BaseSpecification<T> : ISpecification<T> where T: Entity, IAggregateRoot
    {
        public Expression<Func<T, bool>> Criteria { get; }
        public List<Expression<Func<T, bool>>> MultipleCriteria { get; } = new List<Expression<Func<T, bool>>>();
        public List<Expression<Func<T, object>>> Includes { get; } = new List<Expression<Func<T, object>>>();
        public Expression<Func<T, object>> OrderBy { get; private set; }
        public Expression<Func<T, object>> OrderByDescending { get; private set; }
        public int Take { get; private set; }
        public int Skip { get; private set; }
        public bool IsPagingEnabled { get; private set; } = false;

        protected BaseSpecification(Expression<Func<T, bool>> criteria)
        {
            Criteria = criteria;
        }

        protected void AddCriterion(Expression<Func<T, bool>> criterion)
        {
            MultipleCriteria.Add(criterion);
        }

        protected void AddInclude(Expression<Func<T, object>> includeExpression)
        {
            Includes.Add(includeExpression);
        }

        protected void ApplyPaging(int skip, int take)
        {
            Skip = skip;
            Take = take;
            IsPagingEnabled = true;
        }
        protected void ApplyOrderBy(Expression<Func<T, object>> orderByExpression)
        {
            OrderBy = orderByExpression;
        }
        protected void ApplyOrderByDescending(Expression<Func<T, object>> orderByDescendingExpression)
        {
            OrderByDescending = orderByDescendingExpression;
        }
    }
}
