﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Ordering.Domain.SeedWork;

namespace Ordering.Infrastructure.Specifications.Base
{
    public static class SpecificationEvaluator<T> where T : Entity, IAggregateRoot
    {
        public static IQueryable<T> GetQuery(IQueryable<T> inputQuery, ISpecification<T> specification)
        {
            var query = inputQuery;

            if (specification.Criteria != null)
            {
                query = query.Where(specification.Criteria);
            }
            else if(specification.MultipleCriteria != null)
            {
                foreach (var criterion in specification.MultipleCriteria)
                {
                    query = query.Where(criterion);
                }
            }

            query = specification.Includes.Aggregate(query, (current, include) => current.Include(include));

            if (specification.OrderBy != null)
            {
                query = query.OrderBy(specification.OrderBy);
            }
            else if (specification.OrderByDescending != null)
            {
                query = query.OrderByDescending(specification.OrderByDescending);
            }

            if (specification.IsPagingEnabled)
            {
                query = query.Skip(specification.Skip).Take(specification.Take);
            }

            return query;
        }
    }
}
