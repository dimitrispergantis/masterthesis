﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Ordering.Domain.Aggregates.OrderAggregate;
using Ordering.Domain.SeedWork;
using Ordering.Infrastructure.Specifications.Base;

namespace Ordering.Infrastructure.Specifications
{
    public class OrderQuerySpecification: BaseSpecification<Order>
    {
        public OrderQuerySpecification() : base(null)
        {
        }

        public OrderQuerySpecification(int id): base(o => o.Id == id)
        {
            AddInclude(o => o.OrderItems);
            AddInclude(o => o.Address);
            AddInclude(o => o.OrderStatus);
            //AddInclude(o => o.PaymentMethod);
        }
    }
}
