﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cart.API.Model;
using RabbitMQEventBus.Events;

namespace Cart.API.IntegrationEvents.Events
{
    public class CustomerCheckoutAcceptedIntegrationEvent : IntegrationEvent
    {
        public CustomerCart CustomerCart { get;}

        public CustomerCheckoutAcceptedIntegrationEvent(CustomerCart customerCart)
        {
            CustomerCart = customerCart;
        }

    }
}
