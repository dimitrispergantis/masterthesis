﻿using RabbitMQEventBus.Events;

namespace Cart.API.IntegrationEvents.Events
{
    public class OrderStartedIntegrationEvent: IntegrationEvent
    {
        public string CustomerId { get; set; }

        public OrderStartedIntegrationEvent(string customerId)
            => CustomerId = customerId;
    }
}
