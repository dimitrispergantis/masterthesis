﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Cart.API.IntegrationEvents.Events;
using Cart.API.Model;
using Cart.API.Repositories;
using Microsoft.Extensions.Logging;
using RabbitMQEventBus.Interfaces;
using Serilog.Context;

namespace Cart.API.IntegrationEvents.EventHandling
{
    public class ProductVersionPriceChangedIntegrationEventHandler : IIntegrationEventHandler<ProductVersionPriceChangedIntegrationEvent>
    {
        private readonly ILogger<ProductVersionPriceChangedIntegrationEventHandler> _logger;
        private readonly ICartRepository _repository;

        public ProductVersionPriceChangedIntegrationEventHandler(
            ILogger<ProductVersionPriceChangedIntegrationEventHandler> logger,
            ICartRepository repository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task Handle(ProductVersionPriceChangedIntegrationEvent @event)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{@event.Id}-{Program.AppName}"))
            {
                _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, Program.AppName, @event);

                var userIds = _repository.GetUsers();

                foreach (var id in userIds)
                {
                    var basket = await _repository.GetCartAsync(id);

                    await UpdatePriceInCart(@event.ProductId, @event.ProductVersionId, @event.NewPrice, basket);
                }
            }
        }

        private async Task UpdatePriceInCart(int productId, int productVersionId, float newPrice, CustomerCart cart)
        {
            var itemToUpdate = cart?.LineCollection?.SingleOrDefault(x => x.ProductId == productId && x.ProductVersionId == productVersionId);

            if (itemToUpdate != null)
            {
                _logger.LogInformation("----- ProductPriceChangedIntegrationEventHandler - Updating item in cart for user: {CustomerId} ({@Item})", cart.CustomerId, itemToUpdate);

                 itemToUpdate.Price =  newPrice;

                await _repository.UpdateCartAsync(cart);
            }
        }
    }
}

