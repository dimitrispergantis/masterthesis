﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Cart.API.IntegrationEvents.Events;
using Cart.API.Model;
using Cart.API.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RabbitMQEventBus.Interfaces;

namespace Cart.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly ICartRepository _repository;
        private readonly IEventBus _eventBus;
        private readonly ILogger<CartController> _logger;

        public CartController(ICartRepository repository, ILogger<CartController> logger, IEventBus eventBus)
        {
            _repository = repository;
            _logger = logger;
            _eventBus = eventBus;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerCart>> GetBasketByIdAsync(string id)
        {
            var basket = await _repository.GetCartAsync(id);

            return Ok(basket ?? new CustomerCart(id));
        }

        [HttpPost]
        [Route("update_cart")]
        public async Task<ActionResult<CustomerCart>> UpdateBasketAsync([FromBody]CustomerCart value)
        {
            return Ok(await _repository.UpdateCartAsync(value));
        }

        [Route("checkout")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Accepted)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public ActionResult CheckoutAsync([FromBody]CustomerCart cart, [FromHeader(Name = "x-requestid")] string requestId)
        {
            if (cart == null)
            {
                return BadRequest();
            }

            cart.RequestId = (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty) ?
                guid : cart.RequestId;

            var eventMessage = new CustomerCheckoutAcceptedIntegrationEvent(cart);

            try
            {
                _logger.LogInformation("----- Publishing integration event: {IntegrationEventId} from {AppName} - ({@IntegrationEvent})", eventMessage.Id, Program.AppName, eventMessage);

                _eventBus.Publish(eventMessage);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ERROR Publishing integration event: {IntegrationEventId} from {AppName}", eventMessage.Id, Program.AppName);

                throw;
            }

            return Accepted();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        public async Task DeleteBasketByIdAsync(string id)
        {
            await _repository.DeleteCartAsync(id);
        }
    }
}