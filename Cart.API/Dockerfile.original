#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src

# Caching Docker Compose
COPY MasterThesis.sln .
COPY docker-compose.dcproj /src/

COPY ["OcelotApiGateway/OcelotApiGateway.csproj", "OcelotApiGateway/"]
COPY ["Web.Admin.HttpAggregator/Web.Admin.HttpAggregator.csproj", "Web.Admin.HttpAggregator/"]
COPY ["WebHost.Customization/WebHost.Customization.csproj","WebHost.Customization/"]
COPY ["Catalog.API/Catalog.API.csproj", "Catalog.API/"]
COPY ["Catalog.Domain/Catalog.Domain.csproj", "Catalog.Domain/"]
COPY ["Catalog.Infrastacture/Catalog.Infrastucture.csproj", "Catalog.Infrastacture/"]
COPY ["Catalog.UnitTests/Catalog.UnitTests.csproj","Catalog.UnitTests/"]
COPY ["WebMvc/WebMvc.csproj", "WebMvc/"]
COPY ["Identity.API/Identity.API.csproj", "Identity.API/"]
COPY ["Cart.API/Cart.API.csproj", "Cart.API/"]

RUN dotnet restore MasterThesis.sln
# End Caching Docker Compose


COPY . .
WORKDIR "/src/Cart.API"
RUN dotnet build "Cart.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Cart.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Cart.API.dll"]