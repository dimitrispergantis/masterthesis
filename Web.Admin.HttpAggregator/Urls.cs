﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Admin.HttpAggregator
{
    public class Urls
    {
        public class CategoryOperations
        {
            public static string GetCategoryById(int id) => $"/api/catalog/categories/{id}";

            public static string GetAvailableChildCateogiesById(int id) =>
                $"/api/catalog/categories/available-children/parent/{id}";
        }

        public class ProductOperations
        {
            public static string GetAvailableProductCategories => $"/api/catalog/categories/available-children";

            public static string GetAvailableProductBrands => $"/api/catalog/brands/getall";

            public static string GetProductById(int id) => $"/api/catalog/products/{id}";
        }

        public string Catalog { get; set; }
    }
}
