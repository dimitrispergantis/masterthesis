﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;
using Web.Admin.HttpAggregator.Model;

namespace Web.Admin.HttpAggregator.ClientModels
{
    public class CategoryByIdData
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsFrontPageCategory { get; set; }

        public List<int> ChildrenIds { get; set; }

        public IEnumerable<ChildCategory> AvailableChildren { get; set; }
    }
}
