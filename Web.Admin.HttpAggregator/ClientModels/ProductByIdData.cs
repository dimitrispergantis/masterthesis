﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Admin.HttpAggregator.Model;

namespace Web.Admin.HttpAggregator.ClientModels
{
    public class ProductByIdData
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }

        public int BrandId { get; set; }

        public bool IsBest { get; set; }

        public IEnumerable<ChildCategory> Categories { get; set; }

        public IEnumerable<Brand> Brands { get; set; }
    }
}
