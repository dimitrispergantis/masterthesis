﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace Web.Admin.HttpAggregator.Mapping
{
    public static class MappingExtensionMethods
    {
        /// <summary>
        /// Extension method η οποία κάνει map δύο object σε ενα αντικείμενο TResult υπο την προυπόθεση οτι
        /// υπάρχει το κατάλληλο mapping στο MappingProfile.
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="mapper"></param>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <returns></returns>
        public static TResult MergeInto<TResult>(this IMapper mapper, object item1, object item2)
        {
            return mapper.Map(item2, mapper.Map<TResult>(item1));
        }


        /// <summary>
        /// Extension method η οποία κάνει map περισσότερα των δύο object σε ενα αντικείμενο TResult υπο την προυπόθεση οτι
        /// υπάρχούν τα κατάλληλα mappings στο MappingProfile.
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="mapper"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static TResult MergeInto<TResult>(this IMapper mapper, params object[] objects)
        {
            var res = mapper.Map<TResult>(objects.First());
            return objects.Skip(1).Aggregate(res, (r, obj) => mapper.Map(obj, r));
        }
    }
}
