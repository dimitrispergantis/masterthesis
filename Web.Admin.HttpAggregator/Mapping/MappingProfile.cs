﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Web.Admin.HttpAggregator.ClientModels;
using Web.Admin.HttpAggregator.Model;

namespace Web.Admin.HttpAggregator.Mapping
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Category, CategoryByIdData>().ForMember(d => d.ChildrenIds, 
                opt => opt.MapFrom(c => c.Children.Select(cc => cc.Id).ToList()));
        }
    }
}
