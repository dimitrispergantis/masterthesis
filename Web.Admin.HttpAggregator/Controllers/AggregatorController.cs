﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Admin.HttpAggregator.ClientModels;
using Web.Admin.HttpAggregator.Services;

namespace Web.Admin.HttpAggregator.Controllers
{
    [ApiController]
    [Route("aggregator")]
    public class AggregatorController : Controller
    {
        private readonly IAdminAggregateService _service;

        public AggregatorController(IAdminAggregateService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("category/{id:int}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(CategoryByIdData), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCategoryDataById([FromRoute] int id)
        {
            return Ok(await _service.GetCategoryByIdData(id));
        }

        [HttpGet]
        [Route("product/{id:int}")]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]

        public async Task<IActionResult> GetProductDataById([FromRoute] int id)
        {
            return Ok(await _service.GetProductByIdData(id));
        }

        [HttpGet]
        [Route("product")]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProductAvailableParts()
        {
            return Ok(await _service.GetAvailableCategoriesAndBrandsForProductAdd());
        }

    }
}