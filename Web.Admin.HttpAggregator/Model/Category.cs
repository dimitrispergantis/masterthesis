﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Admin.HttpAggregator.Model
{
    public class Category
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsFrontPageCategory { get; set; }

        public IEnumerable<ChildCategory> Children { get; set; }
    }
}
