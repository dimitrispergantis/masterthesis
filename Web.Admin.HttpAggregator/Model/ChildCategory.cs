﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Admin.HttpAggregator.Model
{
    public class ChildCategory
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}
