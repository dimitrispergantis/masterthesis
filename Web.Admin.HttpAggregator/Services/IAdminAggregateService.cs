﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Admin.HttpAggregator.ClientModels;

namespace Web.Admin.HttpAggregator.Services
{
    public interface IAdminAggregateService
    {
        Task<CategoryByIdData> GetCategoryByIdData(int id);

        Task<ProductByIdData> GetProductByIdData(int id);

        Task<ProductByIdData> GetAvailableCategoriesAndBrandsForProductAdd();
    }
}
