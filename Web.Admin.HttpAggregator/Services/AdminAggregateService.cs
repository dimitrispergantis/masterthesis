﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Web.Admin.HttpAggregator.ClientModels;
using Web.Admin.HttpAggregator.Mapping;
using Web.Admin.HttpAggregator.Model;

namespace Web.Admin.HttpAggregator.Services
{
    public class AdminAggregateService: IAdminAggregateService
    {
        private readonly HttpClient _apiClient;
        private readonly Urls _urls;
        private readonly IMapper _mapper;

        public AdminAggregateService(HttpClient apiClient, IOptions<Urls> urls, IMapper mapper)
        {
            _apiClient = apiClient;
            _mapper = mapper;
            _urls = urls.Value;
        }

        public async Task<CategoryByIdData> GetCategoryByIdData(int id)
        {
            var categoryStringContent = await _apiClient.GetStringAsync(_urls.Catalog + Urls.CategoryOperations.GetCategoryById(id));
            var category = JsonConvert.DeserializeObject<Category>(categoryStringContent);

            var availableChildrenCategoriesStringContent = await _apiClient.GetStringAsync(_urls.Catalog + Urls.CategoryOperations.GetAvailableChildCateogiesById(id));
            var availableChildrenCategories = JsonConvert.DeserializeObject<IEnumerable<ChildCategory>>(availableChildrenCategoriesStringContent);

            var clientView = _mapper.Map<CategoryByIdData>(category);
            clientView.AvailableChildren = availableChildrenCategories;

            return clientView;
        }

        public async Task<ProductByIdData> GetProductByIdData(int id)
        {
            var availableCategoriesContent = await _apiClient.GetStringAsync(_urls.Catalog + Urls.ProductOperations.GetAvailableProductCategories);
            var availableCategories =
                JsonConvert.DeserializeObject<IEnumerable<ChildCategory>>(availableCategoriesContent);

            var availableBrandsContent = await _apiClient.GetStringAsync(_urls.Catalog + Urls.ProductOperations.GetAvailableProductBrands);
            var availableBrands = JsonConvert.DeserializeObject<IEnumerable<Brand>>(availableBrandsContent);

            var productContent = await _apiClient.GetStringAsync(_urls.Catalog + Urls.ProductOperations.GetProductById(id));
            var product = JsonConvert.DeserializeObject<ProductByIdData>(productContent);
            
            product.Categories = availableCategories;
            product.Brands = availableBrands;

            return product;
        }

        public async Task<ProductByIdData> GetAvailableCategoriesAndBrandsForProductAdd()
        {
            var availableCategoriesContent = await _apiClient.GetStringAsync(_urls.Catalog + Urls.ProductOperations.GetAvailableProductCategories);
            var availableCategories =
                JsonConvert.DeserializeObject<IEnumerable<ChildCategory>>(availableCategoriesContent);

            var availableBrandsContent = await _apiClient.GetStringAsync(_urls.Catalog + Urls.ProductOperations.GetAvailableProductBrands);
            var availableBrands = JsonConvert.DeserializeObject<IEnumerable<Brand>>(availableBrandsContent);

            return new ProductByIdData(){Categories = availableCategories, Brands = availableBrands};
        }
    }
}
