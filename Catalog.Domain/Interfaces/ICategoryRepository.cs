﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.CategoryAggregate;

namespace Catalog.Domain.Interfaces
{
    public interface ICategoryRepository
    {
        Task<Category> GetCategoryAsync(int id, bool enableTracking = true);

        Task<IReadOnlyList<Category>> GetCategoriesAsync(string sortOrder, string searchString, int page,
            int pageSize);

        Task<IReadOnlyList<Category>> GetCategoriesAsync();

        Task<IEnumerable<Category>> GetAvailableChildrenCategories(int? id);

        Task<IReadOnlyList<Category>> GetCategoriesAsync(IEnumerable<int> ids, bool enableTracking = true);

        Task<int> CountCategoriesAsync();

        Task<int> CreateCategoryAsync(Category category);

        Task<int> UpdateCategoryAsync(Category category);

        Task DeleteCategoryAsync(Category category);

    }
}
