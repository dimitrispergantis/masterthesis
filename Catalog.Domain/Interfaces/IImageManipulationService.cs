﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Text;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;

namespace Catalog.Domain.Interfaces
{
    public interface IImageManipulationService
    {
        byte[] GetImageBytes(string base64String);

        string GetImageMimeType(string base64String);

        string GetImagePublicUri(string filename);

        string GetThumbnailPublicUri(string filename);
 
        void ImportPictureSpecificsInAddList(string filename, Image image, IImageFormat format, int width, int height, int thumbnailWidth, int thumbnailHeight );

        void ImportPictureSpecificsInRemoveList(string filename);

        void SaveImagesToFileSystem();

        void DeleteImagesFromFileSystem();
    }
}
