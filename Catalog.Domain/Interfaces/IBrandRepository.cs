﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.BrandAggregate;

namespace Catalog.Domain.Interfaces
{
    public interface IBrandRepository
    {
        Task<Brand> GetBrandAsync(int id, bool enableTracking = true);

        Task<IReadOnlyList<Brand>> GetBrandsAsync(string sortOrder, string searchString, int page, int pageSize);

        Task<IReadOnlyList<Brand>> GetBrandsAsync(IEnumerable<int> ids, bool enableTracking = true);

        Task<IReadOnlyList<Brand>> GetBrandsAsync();

        Task<int> CountBrandsAsync();

        Task<int> CreateBrandAsync(Brand brand);

        Task<int> UpdateBrandAsync(Brand brand);

        Task DeleteBrandAsync(Brand brand);



    }
}
