﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.ProductAggregate;

namespace Catalog.Domain.Interfaces
{
    public interface IProductRepository
    {
        Task<IReadOnlyList<Product>> GetProductsAsync(string sortOrder, string searchString, int page, int pageSize,
            int? categoryId = null, int? brandId = null, int? minPrice = null, int? maxPrice = null);

        Task<Product> GetProductAsync(int id, bool enableTracking = true);

        Task<IReadOnlyList<ProductVersion>> GetProductVersions(int id);

        Task<int> CountProducts();

        Task<int> CreateProductAsync(string name, string description, bool isBest, int categoryId, int brandId);

        Task UpdateProductAsync(Product product);

        Task DeleteProductAsync(Product product);
    }
}
