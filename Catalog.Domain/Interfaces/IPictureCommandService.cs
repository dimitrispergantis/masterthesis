﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.PictureAggregate;

namespace Catalog.Domain.Interfaces
{
    public interface IPictureCommandService
    {
        Task ConfigureProductPictures(int productId, List<Picture> pictureList);

        IReadOnlyList<Picture> PicturesToDeleteFromFileSystem();

        IReadOnlyList<Picture> PicturesToAddToFilesystem();

    }
}
