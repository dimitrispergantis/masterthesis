﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.PictureAggregate;

namespace Catalog.Domain.Interfaces
{
    public interface IPictureRepository
    {
        Task<Picture> GetPictureAsync(int id, bool enableTracking = true);

        Task<IReadOnlyList<Picture>> GetProductPicturesAsync(int id, bool enableTracking = true);

        Task DeletePicturesAsync(IEnumerable<Picture> pictures);

        Task UpdatePicturesAsync(IEnumerable<Picture> pictures);

        Task AddPicturesAsync(IEnumerable<Picture> pictures);
    }
}
