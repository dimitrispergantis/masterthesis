﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.ProductAggregate;

namespace Catalog.Domain.Interfaces
{
    public interface IProductCommandService
    {
        Task ConfigureProductVersions(int productId, List<ProductVersion> productVersions);
    }
}
