﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Catalog.Domain.Interfaces
{
    public interface IBrandCommandService
    {
        Task<int> CreateNewBrandAsync(string title, float discount);

        Task<int> UpdateExistingBrandAsync(int id, string title, float discount);

        Task DeleteBrandAsync(int id);

    }
}
