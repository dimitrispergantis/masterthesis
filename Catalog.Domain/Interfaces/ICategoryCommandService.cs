﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Catalog.Domain.Interfaces
{
    public interface ICategoryCommandService
    {
        Task<int> CreateNewCategoryAsync(string title, string description, bool isInFrontPage, IEnumerable<int> childrenIds);

        Task<int> UpdateExistingCategoryAsync(int id, string title, string description, bool isInFrontPage, IEnumerable<int> childrenIds);

        Task DeleteCategoryAsync(int id);
    }
}
