﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.Exceptions;
using Catalog.Domain.Interfaces;

namespace Catalog.Domain.Services
{
    public class PictureCommandService: IPictureCommandService
    {
        private readonly IPictureRepository _repository;

        private List<Picture> _picturesToDelete = new List<Picture>();

        private List<Picture> _picturesToAdd = new List<Picture>();

        public PictureCommandService(IPictureRepository repository)
        {
            _repository = repository;
        }

        public async Task ConfigureProductPictures(int productId, List<Picture> pictureList)
        {
            // Βρισκώ τις φωτογραφίες προς εισαγωγή 
            _picturesToAdd = pictureList.Where(p => p.Id == 0).ToList();

            // Βρίσκω τις φωτογραφιές που ειναι αποθηκευμένες στην βάση για το συγκεκριμένο προϊον
            var pictures = await _repository.GetProductPicturesAsync(productId);

            // Βρίσκω ποιές φωτογραφίες βρίσκονταν στην βάση κατα την διάρκεια του post. Οι φωτογραφίες με id = 0 είναι καινούργιες
            var postedInDatabasePictures = pictureList.Where(p => p.Id != 0);

            // Βρισκω τις φωτογραφίες που προκείται να διαγραφούν
            _picturesToDelete = FindPicturesForDeletion(pictures, postedInDatabasePictures);

            // Δημιουργώ την καινούργια κατάσταση φωτογραφίων, αφαιρώντας τις προ διαγραφή φωτογραφίες
            var newPicturesStateList = postedInDatabasePictures.Concat(_picturesToAdd).Except(_picturesToDelete);

            // Ελεγχώ αν η καινουργια κατάσταση φωτογραφιών ειναι εγκυρη. Θα πρέπει να υπάρχει μονο μια κεντρική φωτογραφία στο σύνολο.
            // Αν δεν ισχύει αυτος ο περιορισμός, εχω exception και δεν προχωράω
            IsProductPicturesStateValid(newPicturesStateList);

            // Διαγράφω τις φωτογραφίες
            await _repository.DeletePicturesAsync(_picturesToDelete);

            // Ενημερώνω τις καταστάσεις των υπάρχουσων φωτογραφιών
            await _repository.UpdatePicturesAsync(postedInDatabasePictures);

            // Προσθέτω τις καινούργιες φωτογραφίες στην βάση
            await _repository.AddPicturesAsync(_picturesToAdd);


        }

        public IReadOnlyList<Picture> PicturesToDeleteFromFileSystem() => _picturesToDelete.AsReadOnly();

        public IReadOnlyList<Picture> PicturesToAddToFilesystem() => _picturesToAdd.AsReadOnly();

        private List<Picture> FindPicturesForDeletion(IReadOnlyList<Picture> databaseList, IEnumerable<Picture> postedListed)
        {
            var picturesToDelete = new List<Picture>();

            if (databaseList.Any())
            {
                picturesToDelete = databaseList.Except(postedListed).ToList();
            }

            return picturesToDelete;
        }

        private void IsProductPicturesStateValid(IEnumerable<Picture> pictures)
        {
            if(pictures.Count(p => p.GetPictureType().Equals(PictureType.MainPicture)) != 1)
                throw new CatalogDomainException("Θα πρέπει να υπάρχει μονο μια κεντρική φωτογραφία για το συγκεκριμένο προϊον");
          
        }

        
    }
}
