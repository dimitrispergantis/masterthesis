﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.ProductAggregate;
using Catalog.Domain.Exceptions;
using Catalog.Domain.Interfaces;

namespace Catalog.Domain.Services
{
    public class ProductCommandService: IProductCommandService
    {
        private readonly IProductRepository _repository;

        public ProductCommandService(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task ConfigureProductVersions(int productId, List<ProductVersion> productVersions)
        {
            // Βρίσκω το προϊον
           var product = await _repository.GetProductAsync(productId);

           // Βρίσκώ το συμπλήρωμα της παρούσας λίστας και την λίστα που έγινε post εκτός των καινούργιων εκδόσεων
           // και αφαιρώ τις extra εκδόσεις
           ReleaseUnusedVersions(product, productVersions);

           // Ενημερώνω όσες εκδόσεις διατηρούνταί
           UpdateTheExistingVersions(product, productVersions);

           // Προσθέτω τις καινούργιές versions
           AddNewProductVersions(product, productVersions);
           
           // Αποθηκεύω ασυγχρόνα στην βάση, ενημερώνοντας τις εκδόσεις προϊοντος
           await _repository.UpdateProductAsync(product);
        }

        private void AddNewProductVersions(Product product, List<ProductVersion> productVersions)
        {
            // Απο ολές τις posted versions προσθέτω όσες έχουν id = 0. Είναι καινουργιες εκδόσεις
            var newVersions = productVersions.Where(v => v.Id == 0);

            foreach (var newVersion in newVersions)
            {
               product.AddProductVersion(newVersion);
            }

        }

        private void UpdateTheExistingVersions(Product product, List<ProductVersion> productVersions)
        {
            // Απο ολές τις posted versions εξαιρώ εκείνες με id = 0. Είναι καινουργιες εκδόσεις
            var databaseVersions = productVersions.Where(v => v.Id != 0);

            // Βρισκώ τις κοινές εκδόσεις ανάμεσα στην λίστα εκδόσεων του προίοντος και τις εκδόσεις της βάσης απο την posted λιστα 
            var unionListIds = databaseVersions.Intersect(product.ProductVersions).Select(v => v.Id);

            // και κανώ update τις αντίστοιχες εγγραφές
            foreach (var id in unionListIds)
            {
                var update = productVersions.SingleOrDefault(v => v.Id == id);

                product.UpdateProductVersion(update);

            }
        }

        private void ReleaseUnusedVersions(Product product, List<ProductVersion> productVersions)
        {
            var complementList = new List<ProductVersion>();

            // Απο ολές τις posted versions εξαιρώ εκείνες με id = 0. Είναι καινουργιες εκδόσεις
            var databaseVersions = productVersions.Where(v => v.Id != 0);

            // Εαν υπάρχουν υπάρχουσες εκδόσεις στο post, τότε βρίσκω την διαφορά για να αφαιρέσω ανάλογα
            if (databaseVersions.Any())
            {
                // Βρίσκώ το συμπλήρωμα των εκδόσεων που έχω στην βάση και οσών εκδόσεων της βάσης επέστρεψαν απο το post
                complementList = product.ProductVersions.Except(databaseVersions).ToList();
            }
            
            // Ελέγχω αν η posted αρχική λίστα είναι κενή. Σε αυτη την περίπτωση διαγράφω τα versions 
            if (!productVersions.Any())
            {
                product.ClearProductVersions();
            }
            // Διαφορετικά έχω καποια διαφοροποίηση στις εκδόσεις και αφαιρώ τις extra
            else if(complementList.Any())
            {
                foreach (var unusedVersionId in complementList.Select(v => v.Id).ToList())
                {
                    product.RemoveProductVersion(unusedVersionId);
                }
            }
        }
    }
}
