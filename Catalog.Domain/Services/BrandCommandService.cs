﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.BrandAggregate;
using Catalog.Domain.DomainEvents;
using Catalog.Domain.Exceptions;
using Catalog.Domain.Interfaces;

namespace Catalog.Domain.Services
{
    public class BrandCommandService: IBrandCommandService
    {
        private readonly IBrandRepository _repository;

        public BrandCommandService(IBrandRepository repository)
        {
            _repository = repository;
        }

        public async Task<int> CreateNewBrandAsync(string title, float discount)
        {
            // Ελέγχω αν υπάρχει duplicate title
            if ((await _repository.GetBrandsAsync()).Any(b => b.Title.Contains(title)))
                throw new CatalogDomainException("Δεν ειναι δυνατή η εισαγωγή του brand με duplicate title");

            var brand = new Brand(title, discount);

            int id = await _repository.CreateBrandAsync(brand);

            return id;
        }

        public async Task<int> UpdateExistingBrandAsync(int id, string title, float discount)
        {
            var brand = await _repository.GetBrandAsync(id);

            // Ελέγχω αν ο καινούργιος τιτλός ειναι διαφορετικός του αρχικού
            if (!brand.Title.Equals(title))
            {
                // Ελέγχω αν υπάρχει duplicate title
                if ((await _repository.GetBrandsAsync()).Any(b => b.Title.Contains(title)))
                    throw new CatalogDomainException("Δεν ειναι δυνατή η εισαγωγή του brand με duplicate title");

                brand.SetTitle(title);
            }

            // Εαν αλλάξει η εκπτώση του brand θα πρέπει να εγείρουμε ενα domain event αλλαγής εκπτώσης
            if (brand.Discount != discount)
            {
                brand.SetDiscount(discount);

                var domainEvent = new BrandDiscountChangedDomainEvent(brand.Id, discount);

                brand.AddDomainEvent(domainEvent);
            }

            // Aποθηκεύουμε τις αλλαγές
            await _repository.UpdateBrandAsync(brand);

            return id;

        }

        public async Task DeleteBrandAsync(int id)
        {
            var brand = await _repository.GetBrandAsync(id);

            await _repository.DeleteBrandAsync(brand);
        }
    }
}



