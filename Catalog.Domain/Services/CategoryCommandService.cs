﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Catalog.Domain.Exceptions;
using Catalog.Domain.Interfaces;

namespace Catalog.Domain.Services
{
    public class CategoryCommandService: ICategoryCommandService
    {
        private readonly ICategoryRepository _repository;

        public CategoryCommandService(ICategoryRepository repository)
        {
            _repository = repository;
        }

        public async Task<int> CreateNewCategoryAsync(string title, string description, bool isInFrontPage, IEnumerable<int> childrenIds)
        {
            // Βρισκω τις κατηγοριες προς εισαγωγή
            var children = await _repository.GetCategoriesAsync(childrenIds, true);

            // Ελέγχω τα string literals
            await AreCategoryStringsValidForCommands(null, title, description);

            // Στην συνέχεια θα πρέπει να ελέγξω αν τα παιδια δεν ειναι κεντρικές κατηγορίες
            AreChildenCategoriesValidForCommands(children);
            
            var category = new Category(title, description, isInFrontPage, null, null);

            // Ετσι αλλάζω των δεντρο κατηγορίών
            category = UpdateTheCategoriesTree(category, children);

            int id = await _repository.CreateCategoryAsync(category);

            return id;

        }

        public async Task<int> UpdateExistingCategoryAsync(int id, string title, string description, bool isInFrontPage, IEnumerable<int> childrenIds)
        {
            // Βρισκώ την προς ενημέρωση κατηγορία.
            var category = await _repository.GetCategoryAsync(id);

            // Βρισκω τις κατηγοριες προς εισαγωγή
            var children = await _repository.GetCategoriesAsync(childrenIds, true);

            // Ελέγχω τα string literals
            await AreCategoryStringsValidForCommands(id ,title, description);

            // Στην συνέχεια θα πρέπει να ελέγξω αν τα παιδια δεν ειναι κεντρικές κατηγορίες, ωστε να προχωρήσω στην ενημέρωση του δέντρου κατηγοριών
            AreChildenCategoriesValidForCommands(children);

            // Ετσι αλλάζω των δεντρο κατηγορίών
            category = UpdateTheCategoriesTree(category, children);

            // Ενημερώνω ασυγχρόνα
            await _repository.UpdateCategoryAsync(category);

            return id;
        }

        public async Task DeleteCategoryAsync(int id)
        {
            // Βρίσκω την προς διαγραφή κατηγορία 
            var category = await _repository.GetCategoryAsync(id);

            // Αν η κατηγορια ειναι πατέρας τοτέ ελευθερώνω τις κατηγορίες παιδία
            if (category.IsParentCategory())
            {
                category.ClearChildrenCategories();
            }
            
            // Διαγράφω ασυγχρόνα
            await _repository.DeleteCategoryAsync(category);
        }

        private async Task AreCategoryStringsValidForCommands(int? id, string title, string description)
        {
            // Δημιουργω την λίστα με όλες τις κατηγορίες
            var categoryList = await _repository.GetCategoriesAsync();

            // Σε περίπτωση που έχω id, αναφέρομαι σε update
            if (id.HasValue)
            {
                var otherCategories = categoryList.Where(c => c.Id != id.Value);

                if(otherCategories.Any(c => c.Title.Equals(title)))
                    throw new CatalogDomainException("Δεν ειναι δυνατή η ενημέρωση κατηγορίας με υπάρχον τιτλο.");
            }
            else
            {
                // Διαφορετικά αναφέρομαι σε add και ελέγχω αν υπαρχεί ο παρον τιτλός κατηγορίας σε ολά τα αντικείμενα
                if((await _repository.GetCategoriesAsync()).Any(c => c.Title.Equals(title)))
                    throw new CatalogDomainException("Δεν ειναι δυνατή η δημιουργία κατηγορίας με υπάρχον τιτλο.");
            }

            // Επίσης δεν θα πρέπει η περιγραφή να ειναι κενή.
            if(string.IsNullOrWhiteSpace(description))
                throw new CatalogDomainException("Η περιγραφή κατηγορίας θα πρέπει να ειναι εγκύρη. Δεν επιτρέπεται κενή περιγραφή.");
        }

        private void AreChildenCategoriesValidForCommands(IReadOnlyList<Category> childrenList)
        {
            bool areParents = false;

            if (childrenList.Any())
            {
                foreach (var child in childrenList)
                {
                    areParents |= child.IsParentCategory();
                }

                if(areParents)
                    throw new CatalogDomainException("Οι προς εισαγωγή/ενημέρωση κατηγορίες παιδια δεν ειναι εγκυρες. " +
                                                     "Θα πρέπει να είναι ελεύθερες κατηγορίες.");
            }
            
        }
        
        private Category UpdateTheCategoriesTree(Category newParent, IReadOnlyList<Category> childrenList)
        {
            // Βρισκώ ποιοί κομβοι αλλάζουν την κατάσταση του δέντρου κατηγοριών,
            // χρησιμοποιώ την symmetric except with της Hash Set για να βρώ τα αντικείμενα που αλλάζουν
            var complementList = childrenList.ToHashSet();
            complementList.SymmetricExceptWith(newParent.ChildrenCategories);
            
            // Εαν η λιστα συμπλήρωμα ειναι κένη, τοτέ θα πρέπει να αφαιρέσω τα παίδια απο τον πατέρα σε περιπτώση που έχει
            if (!childrenList.Any())
            {
                if(newParent.IsParentCategory())
                    newParent.ClearChildrenCategories();
            }

            // Για καθε μια valid κατηγορία - παιδί, βρίσκω την παρούσα κεντρική κατηγορία
            // και στην συνέχεια αφαιρώ απο τον κόμβο την κατηγορία παιδί.
            foreach (var child in complementList)
            {
                // Βρισκώ τον πατέρα και αφαιρώ την κατηγορία
                var oldParent = child.ParentCategory;

                // Ελέγχώ αν υπάρχει ο πατέρας καθώς υπάρχει και περίπτωση η κατηγορία παίδι να ειναι ελεύθερη
                // και επείτα αφαιρώ την κατηγορία παίδι
                if (oldParent != null)
                {
                    oldParent.RemoveChildCategory(child);
                }

                // Και την προσθέτω στον καινούργιο της πατέρα, σε περίπτωση που δεν έχουμε update
                // απο την ίδια κατήγορια 
                if (newParent != oldParent)
                {
                    newParent.AddChildCategory(child);
                }
                
            }

            return newParent;
            
        }

        
    }
}
