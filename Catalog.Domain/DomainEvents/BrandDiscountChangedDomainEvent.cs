﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Catalog.Domain.DomainEvents
{
    public class BrandDiscountChangedDomainEvent : INotification
    {
        public int Id { get; set; }
        public float Discount { get; set; }

        public BrandDiscountChangedDomainEvent(int id, float discount)
        {
            Id = id;
            Discount = discount;
        }
    }
}
