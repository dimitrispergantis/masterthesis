﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Catalog.Domain.DomainEvents
{
    public class ProductVersionPriceChangedDomainEvent: INotification
    {
        public int ProductId { get; private set; }

        public int ProductVersionId { get; private set; }

        public float Price { get; private set; }

        public ProductVersionPriceChangedDomainEvent(int productId, int productVersionId, float price)
        {
            ProductId = productId;
            ProductVersionId = productVersionId;
            Price = price;
        }
    }
}
