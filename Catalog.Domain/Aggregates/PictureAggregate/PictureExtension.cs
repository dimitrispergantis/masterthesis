﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Catalog.Domain.Exceptions;
using Catalog.Domain.SeedWork;

namespace Catalog.Domain.Aggregates.PictureAggregate
{
    public class PictureExtension: Enumeration
    {
        public static PictureExtension Jpeg = new PictureExtension(1, nameof(Jpeg).ToLowerInvariant());
        public static PictureExtension Png = new PictureExtension(2, nameof(Png).ToLowerInvariant());

        public PictureExtension(int id, string name) : base(id, name)
        {
        }

        public static IEnumerable<PictureExtension> List() =>
            new PictureExtension[]{Jpeg, Png};

        public static PictureExtension FromName(string name)
        {
            var pictureExtension = List().SingleOrDefault(p => String.Equals(p.Name, name, StringComparison.InvariantCulture));

            if(pictureExtension == null)
            {
                throw new CatalogDomainException($"Πιθανές τιμές για την απαρίθμηση PictureExtension: {String.Join(",", List().Select(s => s.Name))}");
            }

            return pictureExtension;
        }

        public static PictureExtension From(int id)
        {
            var pictureExtension = List().SingleOrDefault(s => s.Id == id);

            if (pictureExtension == null)
            {
                throw new CatalogDomainException($"Πιθανές τιμές για την απαρίθμηση PictureExtension: {String.Join(",", List().Select(s => s.Name))}");
            }

            return pictureExtension;
        }

        public static PictureExtension FromMimeType(string mimeType) => PictureExtension.FromName(mimeType.Split("/")[1]);
        
    }
}