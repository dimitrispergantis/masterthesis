﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Catalog.Domain.Exceptions;
using Catalog.Domain.SeedWork;

namespace Catalog.Domain.Aggregates.PictureAggregate
{
    public class PictureType: Enumeration
    {
        public static PictureType MainPicture = new PictureType(1, nameof(MainPicture).ToLowerInvariant());
        public static PictureType GalleryPicture = new PictureType(2, nameof(GalleryPicture).ToLowerInvariant());
        public static PictureType DescriptionPicture = new PictureType(3, nameof(DescriptionPicture).ToLowerInvariant());

        public PictureType(int id, string name) : base(id, name)
        {
        }

        public static IEnumerable<PictureType> List() =>
            new PictureType[]{MainPicture, GalleryPicture, DescriptionPicture};

        public static PictureType FromName(string name)
        {
            var pictureType = List().SingleOrDefault(p => String.Equals(p.Name, name, StringComparison.InvariantCulture));

            if(pictureType == null)
            {
                throw new CatalogDomainException($"Πιθανές τιμές για την απαρίθμηση PictureType: {String.Join(",", List().Select(s => s.Name))}");
            }

            return pictureType;
        }

        public static PictureType From(int id)
        {
            var pictureType = List().SingleOrDefault(s => s.Id == id);

            if (pictureType == null)
            {
                throw new CatalogDomainException($"Πιθανές τιμές για την απαρίθμηση PictureType: {String.Join(",", List().Select(s => s.Name))}");
            }

            return pictureType;
        }

    }
}
