﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Catalog.Domain.Exceptions;
using Catalog.Domain.SeedWork;

namespace Catalog.Domain.Aggregates.PictureAggregate
{
    public class Picture: Entity, IAggregateRoot
    {
        public int ProductId { get; private set; }

        public string PictureName { get; private set; }

        private int _pictureExtensionId;

        public PictureExtension PictureExtension { get; private set; }

        public int Width { get; private set; }

        public int Height { get; private set; }

        public int ThumbnailWidth { get; private set; }

        public int ThumbnailHeight { get; private set; }

        private int _pictureTypeId;

        public PictureType PictureType { get; private set; }

        private static readonly int dimensionTransformationBound = 1000;

        private static readonly int thumbnailDimensionTransformationBound = 500;

        private static readonly int validImageDimension = 400;

        protected Picture()
        {

        }

        public Picture(int productId, string name, string guid, int width, int height, int pictureTypeId, string mimeType)
        {
            ProductId = productId;
            SetPictureName(name, guid, mimeType);
            SetPictureType(pictureTypeId);
            SetWidth(width);
            SetHeight(height);
        }

        public PictureType GetPictureType() => PictureType.From(_pictureTypeId);
        
        public void SetPictureName(string name, string guid, string mimeType)
        {
            string formattedName = "";

            // Ελέγχω αν έχει δωθεί κάποιο ονομα φωτογραφίας
            if(String.IsNullOrWhiteSpace(name))
                throw new CatalogDomainException("Θα πρέπει να δώσετε ενα εγκύρο ονομά φωτογραφίας.");

            // Ορίζω το διαθέσιμο extension
            _pictureExtensionId = PictureExtension.FromMimeType(mimeType).Id;

            // Βρίσκω την σωστή αναπαράσταση του ονόματος και προσθέτω το guid στο array
            var stringArray = name.ToLowerInvariant().Split(" ").Append(guid);
            
            // Δημιουργώ το full name
            string fullName = String.Join("_", stringArray);

            // Ενημερώνω το ονομά φωτογραφίας του αντικειμένου, προσθέτοντας το extension του
            PictureName = String.Join(".", new string [] {fullName, PictureExtension.From(_pictureExtensionId).Name});

        }

        public void SetPictureType(int typeId)
        {
            _pictureTypeId = PictureType.From(typeId).Id;
        }

        private void SetWidth(int initialWidth)
        {
            if(initialWidth < validImageDimension)
                throw new CatalogDomainException("To πλάτος της φωτογραφίας θα πρέπει να είναι μεγαλύτερο απο 400px " +
                                                 "ώστε να ειναι αντιπροσωπευτικο του προϊοντος.");

            
            Width = (initialWidth > dimensionTransformationBound) ? dimensionTransformationBound : initialWidth;
            ThumbnailWidth = (initialWidth > thumbnailDimensionTransformationBound) ? thumbnailDimensionTransformationBound : initialWidth;
        }

        private void SetHeight(int initialHeight)
        {
            if(initialHeight < validImageDimension)
                throw new CatalogDomainException("To υψός της φωτογραφίας θα πρέπει να είναι μεγαλύτερο απο 400px " +
                                                 "ώστε να ειναι αντιπροσωπευτικο του προϊοντος.");
            
            Height = (initialHeight > dimensionTransformationBound) ? dimensionTransformationBound : initialHeight;
            ThumbnailHeight = (initialHeight > thumbnailDimensionTransformationBound) ? thumbnailDimensionTransformationBound : initialHeight;
        }

    }
}
