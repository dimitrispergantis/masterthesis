﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Catalog.Domain.Exceptions;
using Catalog.Domain.SeedWork;


namespace Catalog.Domain.Aggregates.BrandAggregate
{
    public class Brand: Entity, IAggregateRoot
    {
        public string Title { get; private set; }

        public float Discount { get; private set; }

        public Brand(string title, float discount)
        {
            SetTitle(title);
            SetDiscount(discount);
        }

        /// <summary>
        /// Συνάρτηση η οποία θέτει τον τίτλο του κατασκευαστή με το σωστό format.
        /// Θεωρούμε ως universal rule οτι οι τίτλοι θα πρέπει να είναι
        /// </summary>
        /// <param name="title"></param>
        public void SetTitle(string title)
        {
            if(title == null)
                throw new ArgumentNullException(nameof(title));

            Title = GenericTools.FormatTitle(title);
        }

        /// <summary>
        /// Συνάρτηση η οποία θέτει την εκπτώση ανα brand.
        /// </summary>
        /// <param name="discount"></param>
        public void SetDiscount(float discount)
        {
            if(discount > 1 || discount < 0)
                throw new CatalogDomainException("H εκπτώση πρέπει να κυμαίνεται απο 0% έως 100%.");

            Discount = discount;
        }

        
    }
}
