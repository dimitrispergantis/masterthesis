﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Catalog.Domain.DomainEvents;
using Catalog.Domain.Exceptions;
using Catalog.Domain.SeedWork;

namespace Catalog.Domain.Aggregates.ProductAggregate
{
    public class Product : Entity, IAggregateRoot
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        public int CategoryId { get; private set; }

        public int BrandId { get; private set; }

        public bool IsBest { get; private set; }

        public bool IsDraft { get; private set; }

        private readonly List<ProductVersion> _productVersions;

        public IReadOnlyList<ProductVersion> ProductVersions => _productVersions;

        protected Product()
        {
            _productVersions = new List<ProductVersion>();
            IsBest = false;
            IsDraft = false;
        }

        public Product(string name, string description, int categoryId, int brandId, bool isBest)
        {
            _productVersions = new List<ProductVersion>();
            SetName(name);
            SetDescription(description);
            SetBrand(brandId);
            SetCategory(categoryId);
            SetIsBestProduct(isBest);
            SetIsDraft();
        }

        public void SetName(string name)
        {
            Name = GenericTools.FormatTitle(name);
        }

        public void SetDescription(string description)
        {
            Description = description;
        }

        public void SetDiscount(float discount)
        {
            if (discount < 0 || discount > 1)
                throw new CatalogDomainException("H εκπτώση θα πρέπει να βρίσκεται ανάμεσα στο 0 και 1.");

            foreach (var version in _productVersions)
            {
                version.SetProductPrice(version.ProductPrice.InitialPrice, discount);
            }
        }

        public void SetCategory(int categoryId)
        {
            CategoryId = categoryId;
        }

        public void SetBrand(int brandId)
        {
            BrandId = brandId;
        }

        public void SetIsBestProduct(bool isBest)
        {
            IsBest = isBest;
        }

        /// <summary>
        /// Συνάρτηση ορισμού draft κατάστασης προϊοντος. Αρχικά ελέγχω αν η λίστα των εκδόσεων είναι null. Αφορά την περίπτωση που το προϊον δημιουργήθηκε
        /// Σε περίπτωση που δεν είναι null, τοτέ ελέγχω τον αριθμό των εκδόσεων. Αφορά την περίπτωση που εχούν αφαιρεθεί ολές οι εκδόσεις του προϊντος.
        /// </summary>
        public void SetIsDraft()
        {
            bool isDraft = (_productVersions == null || _productVersions?.Count == 0);

            IsDraft = isDraft;
        }

        /// <summary>
        /// Συνάρτηση προσθήκης εκδόσης προϊοντος στο Aggregate.
        /// Προσθέτει την έκδοση υπο την προυπόθεση οτι δεν υπάρχει διπλότυπο specification
        /// </summary>
        public void AddProductVersion(ProductVersion newVersion)
        {
            bool isDuplicateSpecification = _productVersions.Any(v => v.ProductSpecification.Equals(newVersion.ProductSpecification));

            if(isDuplicateSpecification)
                throw new CatalogDomainException($"Δεν είναι δυνατή η προσθήκή έκδοσης με διπλοτυπo specification: {newVersion.ProductSpecification.ToString()}.");

            _productVersions.Add(newVersion); SetIsDraft();
        }

        /// <summary>
        /// Συνάρτηση ενημέρωσης εκδόσης προϊοντος.
        /// Ενημερώνει την εκδόση του προϊοντος χρησιμοποιώντας τις public συναρτήσεις του ProductVersion (Encapsulation).
        /// </summary>
        public void UpdateProductVersion(ProductVersion updatedVersion)
        {
            var existingVersion = _productVersions.SingleOrDefault(v => v.Id == updatedVersion.Id);

            if(existingVersion == null)
                throw new CatalogDomainException("Η προς ενημέρωση εκδόση δεν είναι διαθέσιμη.");

            existingVersion.SetProductSpecification(updatedVersion.ProductSpecification.Mpn, updatedVersion.ProductSpecification.Size,
                updatedVersion.ProductSpecification.Color, updatedVersion.ProductSpecification.Weight);

            // Εαν γινει αλλαγή της συνολικής τιμής, τοτέ εγείρω ενα PriceChangedDomainEvent
            if (!updatedVersion.ProductPrice.GetPrice().Equals(existingVersion.ProductPrice.GetPrice()))
            {
                AddDomainEvent(new ProductVersionPriceChangedDomainEvent(Id, existingVersion.Id, updatedVersion.ProductPrice.GetPrice()));
            }

            existingVersion.SetProductPrice(updatedVersion.ProductPrice.InitialPrice, updatedVersion.ProductPrice.Discount);
            existingVersion.SetProductStock(updatedVersion.ProductStock.AvailableStock, updatedVersion.ProductStock.RestockThreshold, updatedVersion.ProductStock.MaxStockThreshold);
        }

        /// <summary>
        /// Συνάρτηση αφαίρεσης εκδόσης προϊοντος.
        /// Θα πρέπει να ελέγχουμε την κατάσταση του προϊοντος και να ενημερώνουμε ανάλογα το isDraft πεδίο.
        /// </summary>
        /// <param name="id"></param>
        public void RemoveProductVersion(int id)
        {
            var existingVersion = CheckVersionAvailability(id);

            _productVersions.Remove(existingVersion); SetIsDraft();
        }

        /// <summary>
        /// Συνάρτηση καθαρισμού εκδόσεων
        /// Αφαιρώ ολές τις εκδόσεις και επιστρέφω στην draft κατάσταση το προϊον
        /// </summary>
        public void ClearProductVersions()
        {
            _productVersions.Clear(); SetIsDraft();
        }

        /// <summary>
        /// Συνάρτηση προσθήκης stock σε έκδοση προϊντος. Το προϊον θα πρέπει να ελέγχει την διαδικασία stock
        /// των διαφορετικών versions.
        /// </summary>
        public void AddProductVersionStock(int id, int quantity)
        {
           var productVersion = CheckVersionAvailability(id);

           productVersion.AddStock(quantity);

        }

        /// <summary>
        /// Συνάρτηση αφαίρεσης stock σε έκδοση προϊντος. Το προϊον θα πρέπει να ελέγχει την διαδικασία stock
        /// των διαφορετικών versions.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="quantity"></param>
        public void RemoveProductVersionStock(int id, int quantity)
        {
            var productVersion = CheckVersionAvailability(id);

            productVersion.RemoveStock(quantity);
        }

        private ProductVersion CheckVersionAvailability(int id)
        {
            var existingVersion = _productVersions.SingleOrDefault(v => v.Id == id);

            if(existingVersion == null)
                throw new CatalogDomainException("Η προς ενημέρωση εκδόση δεν είναι διαθέσιμη.");

            return existingVersion;
        }


    }
}
