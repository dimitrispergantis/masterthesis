﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Catalog.Domain.SeedWork;

namespace Catalog.Domain.Aggregates.ProductAggregate
{
    public class ProductPrice : ValueObject
    {
        public float InitialPrice { get; private set; }

        public float Discount { get; private set; }

        public ProductPrice(float initialPrice, float discount)
        {
            InitialPrice = initialPrice;
            Discount = discount;
        }

        public override string ToString()
        {
            return GetPrice().ToString("C", new CultureInfo("fr-FR"));
        }

        public bool HasDiscount()
        {
            return Discount > 0;
        }

        public float GetPrice() => (float) Math.Round(InitialPrice - (Discount * InitialPrice), 2);

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return InitialPrice;

            yield return Discount;
        }
    }
}
