﻿using System;
using System.Collections.Generic;
using System.Text;
using Catalog.Domain.Exceptions;
using Catalog.Domain.SeedWork;

namespace Catalog.Domain.Aggregates.ProductAggregate
{
    public class ProductVersion : Entity
    {
        public ProductSpecification ProductSpecification { get; private set; }

        public ProductPrice ProductPrice { get; private set; }

        public ProductStock ProductStock { get; private set; }

        protected ProductVersion()
        {

        }

        public ProductVersion(string mpn, string size, string color, string weight, float initialPrice, float discount,
           int availableStock, int restockThreshold, int maxStockThreshold)
        {
            SetProductSpecification(mpn, size, color, weight);
            SetProductPrice(initialPrice, discount);
            SetProductStock(availableStock, restockThreshold, maxStockThreshold);
        }

        /// <summary>
        /// Συνάρτηση η οποία θέτει το Product Price, καθώς σε περίπτωση που χρειαστεί αρχικοποίηση είτε ενημέρωση,
        /// η ενέργειά αυτη γίνεται με αντικάτασταση της τιμής (Value Object Interchangeability)
        /// </summary>
        /// <param name="initialPrice"></param>
        /// <param name="discount"></param>
        public void SetProductPrice(float initialPrice, float discount)
        {
            ProductPrice = new ProductPrice(initialPrice, discount);
        }

        /// <summary>
        /// Συνάρτηση η οποία θέτει το Product Specification, καθώς σε περίπτωση που χρειαστεί αρχικοποίηση είτε ενημέρωση,
        /// η ενέργειά αυτη γίνεται με αντικάτασταση της τιμής (Value Object Interchangeability)
        /// </summary>
        /// <param name="mpn"></param>
        /// <param name="size"></param>
        /// <param name="color"></param>
        /// <param name="weight"></param>
        public void SetProductSpecification(string mpn, string size, string color, string weight)
        {
             ProductSpecification = new ProductSpecification(mpn, size, color, weight);
        }

        
        /// <summary>
        /// Συνάρτηση η οποία θέτει το Product Stock, καθώς σε περίπτωση που χρειαστεί αρχικοποίηση είτε ενημέρωση,
        /// η ενέργειά αυτη γίνεται με αντικάτασταση της τιμής (Value Object Interchangeability) 
        /// </summary>
        /// <param name="availableStock"></param>
        /// <param name="restockThreshold"></param>
        /// <param name="maxStockThreshold"></param>
        public void SetProductStock(int availableStock, int restockThreshold, int maxStockThreshold)
        {
            ProductStock = new ProductStock(availableStock, restockThreshold, maxStockThreshold);
        }

        
         /// <summary>
        /// Decrements the quantity of a particular item in inventory and ensures the restockThreshold hasn't
        /// been breached. If so, a RestockRequest is generated in CheckThreshold. 
        /// 
        /// If there is sufficient stock of an item, then the integer returned at the end of this call should be the same as quantityDesired. 
        /// In the event that there is not sufficient stock available, the method will remove whatever stock is available and return that quantity to the client.
        /// In this case, it is the responsibility of the client to determine if the amount that is returned is the same as quantityDesired.
        /// It is invalid to pass in a negative number. 
        /// </summary>
        /// <param name="quantityDesired"></param>
        /// <returns>int: Returns the number actually removed from stock. </returns>
        /// 
        public int RemoveStock(int quantityDesired)
        {
            if (ProductStock.AvailableStock == 0)
            {
                throw new CatalogDomainException($"Empty stock, product item is sold out");
            }

            if (quantityDesired <= 0)
            {
                throw new CatalogDomainException($"Item units desired should be greater than zero");
            }

            int removed = Math.Min(quantityDesired, ProductStock.AvailableStock);
            
            int newStock = ProductStock.AvailableStock - removed;

            SetProductStock(newStock, ProductStock.RestockThreshold, ProductStock.MaxStockThreshold);

            return removed;
        }

        /// <summary>
        /// Increments the quantity of a particular item in inventory.
        /// <param name="quantity"></param>
        /// <returns>int: Returns the quantity that has been added to stock</returns>
        /// </summary>
        public int AddStock(int quantity)
        {
            int original = ProductStock.AvailableStock;
            int newProductStock = 0;

            // The quantity that the client is trying to add to stock is greater than what can be physically accommodated in the Warehouse
            if ((ProductStock.AvailableStock + quantity) > ProductStock.MaxStockThreshold)
            {
                // For now, this method only adds new units up maximum stock threshold. In an expanded version of this application, we
                //could include tracking for the remaining units and store information about overstock elsewhere. 
                newProductStock = ProductStock.AvailableStock + (ProductStock.MaxStockThreshold - ProductStock.AvailableStock);
                
                SetProductStock(newProductStock, ProductStock.RestockThreshold, ProductStock.MaxStockThreshold);
                
            }
            else
            {
                newProductStock = ProductStock.AvailableStock + quantity;
                
                SetProductStock(newProductStock, ProductStock.RestockThreshold, ProductStock.MaxStockThreshold);
            }

            return ProductStock.AvailableStock - original;
        }

    }
}
