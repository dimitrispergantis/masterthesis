﻿using System;
using System.Collections.Generic;
using System.Text;
using Catalog.Domain.SeedWork;

namespace Catalog.Domain.Aggregates.ProductAggregate
{
    public class ProductSpecification: ValueObject
    {
        public string Mpn { get; private set; }

        public string Size { get; private set; }

        public string Color { get; private set; }

        public string Weight { get; private set; }

        public ProductSpecification(string mpn, string size, string color, string weight)
        {
            Mpn = mpn;
            Size = size;
            Color = color;
            Weight = weight;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Mpn;

            yield return Size;

            yield return Color;

            yield return Weight;
        }

        public override string ToString()
        {
            var specification = String.Join(" ", new string[] {Mpn, Size, Color, Weight});

            return specification;
        }
    }
}
