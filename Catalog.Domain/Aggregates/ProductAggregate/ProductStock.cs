﻿using System;
using System.Collections.Generic;
using System.Text;
using Catalog.Domain.Exceptions;
using Catalog.Domain.SeedWork;

namespace Catalog.Domain.Aggregates.ProductAggregate
{
    public class ProductStock : ValueObject
    {
        public int AvailableStock { get; private set; }

        public int RestockThreshold { get; private set; }

        public int MaxStockThreshold { get; private set; }

        public bool OnReorder { get; private set; }

        public ProductStock(int availableStock, int restockThreshold, int maxStockThreshold)
        {
           SetStock(availableStock, restockThreshold, maxStockThreshold);
        }

        private void SetStock(int availableStock, int restockThreshold, int maxStockThreshold)
        {
            OnReorder = (availableStock <= RestockThreshold) ? true : false;
            AvailableStock = availableStock;
            RestockThreshold = restockThreshold;
            MaxStockThreshold = maxStockThreshold;
            
        }

        public bool IsOnStock()
        {
            return AvailableStock > 0;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return AvailableStock;

            yield return RestockThreshold;

            yield return MaxStockThreshold;

            yield return OnReorder;
        }
    }
}
