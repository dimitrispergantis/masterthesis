﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Catalog.Domain.Exceptions;
using Catalog.Domain.SeedWork;

namespace Catalog.Domain.Aggregates.CategoryAggregate
{
    public class Category : Entity, IAggregateRoot
    {
        public string Title { get; private set; }

        public string Description { get; private set; }

        public bool IsFrontPageCategory { get; private set; }

        public Category ParentCategory { get; private set; }

        private List<Category> _childrenCategories;

        public IReadOnlyList<Category> ChildrenCategories => _childrenCategories.AsReadOnly();

        protected Category()
        {
            _childrenCategories = new List<Category>();
            IsFrontPageCategory = false;
        }

        public Category(string title, string description, bool isFrontPageCategory, Category parent = null, List<Category> children = null)
        { 
            SetTitle(title);
            SetDescription(description); 
            SetIsFrontPageCategory(isFrontPageCategory);
            SetParentCategory(parent);
            SetChildrenCategories(children);
        }

        public string GetBreadcrumbCategoryTitle()
        {
            return (ParentCategory != null) ? ParentCategory.Title + " > " + Title : Title;
        }

        public void SetTitle(string title)
        {
            if(string.IsNullOrWhiteSpace(title))
                throw new ArgumentNullException(nameof(title));

            Title = GenericTools.FormatTitle(title);
        }

        public void SetDescription(string description)
        {
            if(string.IsNullOrWhiteSpace(description))
                throw new ArgumentNullException(nameof(description));

            Description = description;
        }

        public void SetIsFrontPageCategory(bool isFrontPage)
        {
            IsFrontPageCategory = isFrontPage;
        }

        public void SetParentCategory(Category parent)
        {
            // Ελέγχω αν η πατέρας δεν υπάρχει ως παιδί της παρούσας κατηγορίας.
            if(_childrenCategories?.Any(c => c.Id == parent.Id) ?? false)
                throw new CatalogDomainException($"Δεν επιτρέπεται κυκλική αναφορά στο δέντρο κατηγοριών.");

            ParentCategory = parent;
        }

        public void AddChildCategory(Category child)
        {
            if(child == null)
                throw new CatalogDomainException("H κατηγορία παιδί δεν μπορεί να ειναι null αντικείμενο.");

            // Η προς εισαγωγή κατηγορία παιδί, δεν μπορεί να είναι πατέρας της υπάρχουσας.
            if(child.Equals(ParentCategory))
                throw new CatalogDomainException($"H κατηγορία παιδι προς εισαγωγή {child.Title} είναι πατέρας της υπάρχουσας {this.Title}.");

            // Δεν μπορούν να υπάρχουν διπλοτυπές κατηγορίες - παιδία
            if(_childrenCategories?.Contains(child) ?? false)
                throw new CatalogDomainException($"H κατηγορία {child.Title} βρίσκεται ήδη στην λίστα παιδιών της {this.Title}.");

            _childrenCategories.Add(child);
        }

        private void SetChildrenCategories(List<Category> children)
        {
            _childrenCategories = new List<Category>();

            foreach (var child in ((children == null) ? new List<Category>() : children))
            {
                AddChildCategory(child);
            }
        }

        public void RemoveParentCategory()
        {
            ParentCategory = null;
        }

        public void RemoveChildCategory(Category child)
        {
            _childrenCategories.Remove(child);
        }

        public void ClearChildrenCategories()
        {
            _childrenCategories.Clear();
        }

        public bool IsParentCategory()
        {
            return _childrenCategories.Count > 0;
        }

        public bool IsChildCategory()
        {
            return ParentCategory != null;
        }



    }
}
