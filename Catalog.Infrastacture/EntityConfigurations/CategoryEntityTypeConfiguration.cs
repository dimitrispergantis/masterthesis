﻿using System;
using System.Collections.Generic;
using System.Text;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Catalog.Infrastructure.EntityConfigurations
{
    public class CategoryEntityTypeConfiguration: IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Category");

            builder.HasKey(c => c.Id);

            builder.Ignore(c => c.DomainEvents);

            builder.Property(c => c.Id).ForSqlServerUseSequenceHiLo("category_hilo");

            builder.Property(c => c.Title).IsRequired();

            builder.Property(c => c.Description).IsRequired();

            builder.Property(c => c.IsFrontPageCategory).HasDefaultValue(false);

            builder.HasOne(c => c.ParentCategory).WithMany(c => c.ChildrenCategories);

            var navigation = builder.Metadata.FindNavigation(nameof(Category.ChildrenCategories));
            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);

        }
    }
}
