﻿using System;
using System.Collections.Generic;
using System.Text;
using Catalog.Domain.Aggregates.PictureAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Catalog.Infrastructure.EntityConfigurations
{
    public class PictureTypeEntityTypeConfiguration: IEntityTypeConfiguration<PictureType>
    {
        public void Configure(EntityTypeBuilder<PictureType> builder)
        {
            builder.ToTable("PictureType");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).HasDefaultValue(1).ValueGeneratedNever().IsRequired();

            builder.Property(p => p.Name).HasMaxLength(200).IsRequired();
        }
    }
}
