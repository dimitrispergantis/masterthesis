﻿using System;
using System.Collections.Generic;
using System.Text;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.Aggregates.ProductAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Catalog.Infrastructure.EntityConfigurations
{
    public class PictureEntityTypeConfiguration: IEntityTypeConfiguration<Picture>
    {
        public void Configure(EntityTypeBuilder<Picture> builder)
        {
            builder.ToTable("Picture");

            builder.Ignore(p => p.DomainEvents);

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).ForSqlServerUseSequenceHiLo("picture_hilo");

            builder.Property(p => p.PictureName).IsRequired();

            builder.Property(p => p.Height).IsRequired();

            builder.Property(p => p.Width).IsRequired();

            builder.HasOne<Product>().WithMany().HasForeignKey("ProductId");

            builder.Metadata.FindNavigation(nameof(PictureExtension)).SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.Metadata.FindNavigation(nameof(PictureType)).SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
