﻿using System;
using System.Collections.Generic;
using System.Text;
using Catalog.Domain.Aggregates.ProductAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Catalog.Infrastructure.EntityConfigurations
{
    public class ProductVersionEntityTypeConfiguration: IEntityTypeConfiguration<ProductVersion>
    {
        public void Configure(EntityTypeBuilder<ProductVersion> builder)
        {
            builder.ToTable("ProductVersion");

            builder.HasKey(v => v.Id);

            builder.Property(v => v.Id).ForSqlServerUseSequenceHiLo("productversion_hilo");

            builder.Ignore(v => v.DomainEvents);

            builder.OwnsOne(v => v.ProductSpecification);

            builder.OwnsOne(v => v.ProductPrice);
            
            builder.OwnsOne(v => v.ProductStock);

        }
    }
}
