﻿using System;
using System.Collections.Generic;
using System.Text;
using Catalog.Domain.Aggregates.BrandAggregate;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Catalog.Domain.Aggregates.ProductAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Catalog.Infrastructure.EntityConfigurations
{
    public class ProductEntityTypeConfiguration: IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Product");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).ForSqlServerUseSequenceHiLo("product_hilo");

            builder.Ignore(p => p.DomainEvents);

            builder.Property(p => p.Name).IsRequired();

            builder.Property(p => p.Description).IsRequired();

            builder.Property(p => p.IsBest).IsRequired();

            builder.Property(p => p.IsDraft).IsRequired();

            builder.Property<int>("BrandId").IsRequired();

            builder.Property<int>("CategoryId").IsRequired();

            builder.HasOne<Brand>().WithMany().HasForeignKey("BrandId").IsRequired().OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<Category>().WithMany().HasForeignKey("CategoryId").IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            var productVersionsNavigation = builder.Metadata.FindNavigation(nameof(Product.ProductVersions));
            productVersionsNavigation.SetPropertyAccessMode(PropertyAccessMode.Field);

        }
    }
}
