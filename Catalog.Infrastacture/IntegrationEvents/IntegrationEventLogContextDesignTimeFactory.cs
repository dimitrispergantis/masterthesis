﻿using EventLogEntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Catalog.Infrastructure.IntegrationEvents
{
    public class IntegrationEventLogContextDesignTimeFactory : IDesignTimeDbContextFactory<IntegrationEventLogContext>
    {
        public IntegrationEventLogContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<IntegrationEventLogContext>();

            optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=CatalogDatabase;Trusted_Connection=True;MultipleActiveResultSets=true",
                x => x.MigrationsAssembly("Catalog.Infrastructure"));

            return new IntegrationEventLogContext(optionsBuilder.Options);
        }
    }
}