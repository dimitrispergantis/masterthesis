﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Text.RegularExpressions;
using Catalog.Domain.Exceptions;
using Catalog.Domain.Interfaces;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;

namespace Catalog.Infrastructure.Services
{
    public class ImageManipulationService: IImageManipulationService
    {
        private readonly Regex _dataUriPattern;

        private readonly List<ImageSpecifics> _addSpecificsList;
        private readonly List<ImageSpecifics> _removeSpecificsList;

        private readonly string _webRootPath;
        private readonly string _publicBaseUrl;

        private readonly IFileSystem _fileSystem;

        public ImageManipulationService(IFileSystem fileSystem, string webRootPath, string publicBaseUrl)
        {
            _webRootPath = webRootPath;
            _publicBaseUrl = publicBaseUrl;
            _fileSystem = fileSystem;
            _addSpecificsList = new List<ImageSpecifics>();
            _removeSpecificsList = new List<ImageSpecifics>();
            _dataUriPattern = new Regex(@"^data\:(?<type>image\/(png|tiff|jpg|jpeg|gif));base64,(?<data>[A-Z0-9\+\/\=]+)$",
                RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase);
        }

        public byte[] GetImageBytes(string base64String)
        {
            var match = _dataUriPattern.Match(base64String);
            var base64Data = match.Groups["data"].Value;
            
            return Convert.FromBase64String(base64Data);
        }

        public string GetImageMimeType(string base64String)
        {
            var match = _dataUriPattern.Match(base64String);
            
            return match.Groups["type"].Value;
        }

        public void ImportPictureSpecificsInAddList(string filename, Image image, IImageFormat format,  int width, int height, int thumbnailWidth, int thumbnailHeight )
        {
            if(image == null)
                throw new CatalogDomainException("Δεν γίνεται να προστεθεί φωτογραφιά στο file system χωρίς δεδομένα");

            _addSpecificsList.Add(new ImageSpecifics(filename, image, format, width, height, thumbnailWidth, thumbnailHeight ));
        }

        public void ImportPictureSpecificsInRemoveList(string filename)
        {
            _removeSpecificsList.Add(new ImageSpecifics(filename, null, null, 0, 0, 0, 0));
        }

        public string GetImagePublicUri(string filename) => 
            (_fileSystem.File.Exists(GetImageFileSystemUri(filename))) ? 
                Path.Combine(_publicBaseUrl, filename) : string.Empty;

        public string GetThumbnailPublicUri(string filename) =>
            (_fileSystem.File.Exists(GetThumbnailImageFileSystemUri(filename))) ?
                Path.Combine(_publicBaseUrl, "thumbnail_" + filename) : string.Empty;

        public void SaveImagesToFileSystem()
        {
            foreach (var imageSpecifics in _addSpecificsList)
            {
                var fullImageUri = GetImageFileSystemUri(imageSpecifics.Filename);
                var thumbImageUri = GetThumbnailImageFileSystemUri(imageSpecifics.Filename);

                var format = imageSpecifics.Format;

                var mainImage = imageSpecifics.Image;
                var thumbImage = imageSpecifics.Image;

                using (var output = _fileSystem.FileStream.Create(fullImageUri, FileMode.Create))
                {
                    mainImage.Mutate(i => i.Resize(new ResizeOptions
                    {
                        Size = new Size(imageSpecifics.Width, imageSpecifics.Height),
                        Mode = ResizeMode.Max
                    }));

                    mainImage.Save(output, format);
                }
                
                using (var output = _fileSystem.FileStream.Create(thumbImageUri, FileMode.Create))
                {
                    thumbImage.Mutate(i =>  i.Resize(new ResizeOptions
                    {
                        Size = new Size(imageSpecifics.ThumbnailWidth, imageSpecifics.ThumbnailHeight),
                        Mode = ResizeMode.Max
                    }));

                    thumbImage.Save(output, format);
                }
            }
        }

        public void DeleteImagesFromFileSystem()
        {
            foreach (var imageSpecifics in _removeSpecificsList)
            {
                var fullImageUri = GetImageFileSystemUri(imageSpecifics.Filename);
                var thumbImageUri = GetThumbnailImageFileSystemUri(imageSpecifics.Filename);

               if(_fileSystem.File.Exists(fullImageUri))
                   _fileSystem.File.Delete(fullImageUri);

               if(_fileSystem.File.Exists(thumbImageUri))
                   _fileSystem.File.Delete(thumbImageUri);
            }
        }

        private string GetImageFileSystemUri(string pictureName) => Path.Combine(_webRootPath, pictureName);

        private string GetThumbnailImageFileSystemUri(string pictureName) => Path.Combine(_webRootPath, "thumbnail_" + pictureName);

    }

    internal class ImageSpecifics
    {
        public string Filename { get; set; }

        public Image Image { get; set; }

        public IImageFormat Format { get; set; }

        public int Width { get; private set; }

        public int Height { get; private set; }

        public int ThumbnailWidth { get; private set; }

        public int ThumbnailHeight { get; private set; }

        public ImageSpecifics(string filename, Image image, IImageFormat format, int width, int height, int thumbnailWidth, int thumbnailHeight)
        {
            Filename = filename ?? throw new ArgumentNullException(nameof(filename));
            Image = image;
            Format = format;
            Width = width;
            Height = height;
            ThumbnailWidth = thumbnailWidth;
            ThumbnailHeight = thumbnailHeight;
        }
    }
}
