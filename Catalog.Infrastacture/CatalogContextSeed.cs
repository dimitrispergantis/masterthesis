﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Polly;

namespace Catalog.Infrastructure
{
    public class CatalogContextSeed
    {

        public  async Task SeedAsync(CatalogContext context, ILogger<CatalogContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(CatalogContextSeed));

            await policy.Execute(async () =>
            {
                using (context)
                {
                    context.Database.Migrate();

                    if (!context.PictureTypes.Any())
                    {
                        context.PictureTypes.AddRange(GetPredefinedPictureTypes());

                        await context.SaveChangesAsync();
                    }

                    if (!context.PictureExtensions.Any())
                    {
                        context.PictureExtensions.AddRange(GetPredefinedPictureExtensions());
                    }

                    await context.SaveChangesAsync();
                }
            });
        }

        private IEnumerable<PictureType> GetPredefinedPictureTypes()
        {
            return Enumeration.GetAll<PictureType>();
        }

        private IEnumerable<PictureExtension> GetPredefinedPictureExtensions()
        {
            return Enumeration.GetAll<PictureExtension>();
        }

        private Policy CreatePolicy( ILogger<CatalogContextSeed> logger, string prefix, int retries =3)
        {
            return Policy.Handle<SqlException>().
                WaitAndRetry(
                    retryCount: retries,
                    sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
                    onRetry: (exception, timeSpan, retry, ctx) =>
                    {
                        logger.LogWarning(exception, "[{prefix}] Exception {ExceptionType} with message {Message} detected on attempt {retry} of {retries}", prefix, exception.GetType().Name, exception.Message, retry, retries);
                    }
                );
        }
    }
}
