﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.ProductAggregate;
using Catalog.Domain.Interfaces;
using Catalog.Infrastructure.Repositories.Base;
using Catalog.Infrastructure.Specifications;

namespace Catalog.Infrastructure.Repositories
{
    public class ProductRepository: Repository<Product>, IProductRepository
    {
        public ProductRepository(CatalogContext context) : base(context)
        {
        }

        public async Task<IReadOnlyList<Product>> GetProductsAsync(string sortOrder, string searchString, int page, int pageSize,
            int? categoryId = null, int? brandId = null, int? minPrice = null, int? maxPrice = null)
        {

            var products = (String.IsNullOrWhiteSpace(searchString))
                ? await  GetAsync(new ProductQuerySpecification(sortOrder, page, pageSize, categoryId, brandId), false)
                : await  GetAsync(new ProductQuerySpecification(searchString), false);

            if(minPrice.HasValue && maxPrice.HasValue)
            {
                products = products.Where(p =>
                    p.ProductVersions.Any(v => v.ProductPrice.GetPrice() <= maxPrice && v.ProductPrice.GetPrice() >= minPrice))
                    .ToList();

                return products.ToList();

            }

            return products;
        }

        public async Task<Product> GetProductAsync(int id, bool enableTracking = true)
        {
            return await GetByIdAsync(new ProductQuerySpecification(id), enableTracking);
        }

        public async Task<IReadOnlyList<ProductVersion>> GetProductVersions(int id)
        {
            var productVersions = (await GetProductAsync(id, false)).ProductVersions;

            return productVersions;
        }

        public async Task<int> CountProducts()
        {
            return await CountAsync();
        }

        public async Task<int> CreateProductAsync(string name, string description, bool isBest, int categoryId, int brandId)
        {
            var product = new Product(name, description, categoryId, brandId, isBest);

            await AddAsync(product);
            await UnitOfWork.SaveEntitiesAsync();

            return product.Id;
        }

        public async Task UpdateProductAsync(Product product)
        {
            Update(product);
            await UnitOfWork.SaveEntitiesAsync();
        }

        public async Task DeleteProductAsync(Product product)
        {
            Delete(product);
            await UnitOfWork.SaveEntitiesAsync();
        }
    }
}
