﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.Interfaces;
using Catalog.Infrastructure.Repositories.Base;
using Catalog.Infrastructure.Specifications;

namespace Catalog.Infrastructure.Repositories
{
    public class PictureRepository: Repository<Picture>, IPictureRepository
    {
        public PictureRepository(CatalogContext context) : base(context)
        {
        }

        public async Task<Picture> GetPictureAsync(int id, bool enableTracking = true)
        {
            return await GetByIdAsync(new PictureQuerySpecification(id), enableTracking);
        }

        public async Task<IReadOnlyList<Picture>> GetProductPicturesAsync(int id, bool enableTracking = true)
        {
            return await GetAsync(new ProductPicturesSpecification(id), enableTracking);
        }

        public async Task DeletePicturesAsync(IEnumerable<Picture> pictures)
        {
            foreach (var picture in pictures)
            {
                Delete(picture);
            }

            await UnitOfWork.SaveChangesAsync();
        }

        public async Task UpdatePicturesAsync(IEnumerable<Picture> pictures)
        {
            foreach (var picture in pictures)
            {
                Update(picture);
            }

            await UnitOfWork.SaveChangesAsync();
        }

        public async Task AddPicturesAsync(IEnumerable<Picture> pictures)
        {
            foreach (var picture in pictures)
            {
                await AddAsync(picture);
            }

            await UnitOfWork.SaveChangesAsync();
        }
    }
}
