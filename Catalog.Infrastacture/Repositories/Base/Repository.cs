﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.BrandAggregate;
using Catalog.Domain.SeedWork;
using Catalog.Infrastructure.Specifications.Base;
using Microsoft.EntityFrameworkCore;

namespace Catalog.Infrastructure.Repositories.Base
{
    public class Repository<T>: IRepository<T> where T: Entity, IAggregateRoot 
    {
        protected readonly CatalogContext _context;

        public Repository(CatalogContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork
        {
            get { return _context; }
        }

        public async Task<int> FindByIdAsync(int id)
        {
            var entity = await _context.Set<T>().FindAsync(id);

            return (entity != null)? entity.Id : 0;
        }

        public async Task<List<T>> GetAsync(ISpecification<T> spec, bool enableTracking = true)
        {
            return await ApplySpecification(spec, enableTracking).ToListAsync();
        }

        public async Task<T> GetByIdAsync(ISpecification<T> spec, bool enableTracking = true)
        {
            return await ApplySpecification(spec, enableTracking).SingleOrDefaultAsync();
        }

        public async Task AddAsync(T entity)
        { 
            await _context.Set<T>().AddAsync(entity);
        }

        public void Update(T entity)
        {  
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public async Task<int> CountAsync()
        {
            return await _context.Set<T>().CountAsync();
        }

        private IQueryable<T> ApplySpecification(ISpecification<T> spec, bool enableTracking = true)
        {
            var query = SpecificationEvaluator<T>.GetQuery(_context.Set<T>().AsQueryable(), spec);

            return (enableTracking) ? query : query.AsNoTracking();
        }
    }
}
