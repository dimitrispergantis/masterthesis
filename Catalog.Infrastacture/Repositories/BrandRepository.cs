﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.BrandAggregate;
using Catalog.Domain.DomainEvents;
using Catalog.Domain.Interfaces;
using Catalog.Domain.SeedWork;
using Catalog.Infrastructure.Repositories.Base;
using Catalog.Infrastructure.Specifications;

namespace Catalog.Infrastructure.Repositories
{
    public class BrandRepository : Repository<Brand>, IBrandRepository
    {
        public BrandRepository(CatalogContext context) : base(context)
        {
        }

        public async Task<Brand> GetBrandAsync(int id, bool enableTracking = true)
        {
            return await GetByIdAsync(new BrandQuerySpecification(id), enableTracking);
        }

        public async Task<IReadOnlyList<Brand>> GetBrandsAsync(string sortOrder, string searchString, int page, int pageSize)
        {
            return (String.IsNullOrWhiteSpace(searchString))
                ? await  GetAsync(new BrandQuerySpecification(sortOrder, page, pageSize), false)
                : await  GetAsync(new BrandQuerySpecification(searchString), false);
        }

        public async Task<IReadOnlyList<Brand>> GetBrandsAsync(IEnumerable<int> ids, bool enableTracking = true)
        {
            if (ids != null)
            {
                return await GetAsync(new BrandQuerySpecification(ids), enableTracking);
            }

            return new List<Brand>();
        }

        public async Task<IReadOnlyList<Brand>> GetBrandsAsync()
        {
            return await GetAsync(new BrandQuerySpecification(), false);
        }

        public async Task<int> CountBrandsAsync()
        {
            return await CountAsync();
        }

        public async Task<int> CreateBrandAsync(Brand brand)
        {
            await AddAsync(brand);
            await UnitOfWork.SaveEntitiesAsync();

            return brand.Id;
        }

        public async Task<int> UpdateBrandAsync(Brand brand)
        {
            Update(brand);
            await UnitOfWork.SaveEntitiesAsync();

            return brand.Id;

        }

        public async Task DeleteBrandAsync(Brand brand)
        {
            Delete(brand);
            await UnitOfWork.SaveEntitiesAsync();
        }
    }
}
