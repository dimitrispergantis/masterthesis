﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Catalog.Domain.Interfaces;
using Catalog.Infrastructure.Repositories.Base;
using Catalog.Infrastructure.Specifications;

namespace Catalog.Infrastructure.Repositories
{
    public class CategoryRepository: Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(CatalogContext context) : base(context)
        {
        }

        public async Task<IReadOnlyList<Category>> GetCategoriesAsync(string sortOrder, string searchString, int page,
            int pageSize)
        {
            return (String.IsNullOrWhiteSpace(searchString))
                ? await  GetAsync(new CategoryQuerySpecification(sortOrder, page, pageSize), false)
                : await  GetAsync(new CategoryQuerySpecification(searchString), false);
        }

        public async Task<IReadOnlyList<Category>> GetCategoriesAsync()
        {
            return await GetAsync(new CategoryQuerySpecification());
        }

        public async Task<IEnumerable<Category>> GetAvailableChildrenCategories(int? id)
        {
            var availableChildrenWithParent = (id.HasValue) ? await GetAsync(new AvailableChildrenWithParentSpecification(id.Value))
                : await GetAsync(new AvailableChildrenWithParentSpecification());

            var availableRootChildren = (id.HasValue) ?
                (await GetAsync(new AvailableRootChildrenSpecification(id.Value))).Where(c => !c.IsParentCategory()) :
                (await GetAsync(new AvailableRootChildrenSpecification())).Where(c => !c.IsParentCategory());

            var availableChildrenUnion = availableRootChildren.Concat(availableChildrenWithParent);

            return availableChildrenUnion;
        }

        public async Task<IReadOnlyList<Category>> GetCategoriesAsync(IEnumerable<int> ids, bool enableTracking = true)
        {
            if (ids != null)
            {
                return await GetAsync(new CategoryQuerySpecification(ids), enableTracking);
            }

            return new List<Category>();
            
        }

        public Task<int> CountCategoriesAsync()
        {
            return CountAsync();
        }

        public async Task<Category> GetCategoryAsync(int id, bool enableTracking = true)
        {
            return await GetByIdAsync(new CategoryQuerySpecification(id), enableTracking);
        }

        public async Task<int> CreateCategoryAsync(Category category)
        {
            await AddAsync(category);

            await UnitOfWork.SaveEntitiesAsync();

            return category.Id;
        }

        public async Task<int> UpdateCategoryAsync(Category category)
        {
            Update(category);
            await UnitOfWork.SaveEntitiesAsync();

            return category.Id;
        }

        public async Task DeleteCategoryAsync(Category category)
        {
            Delete(category);
            await UnitOfWork.SaveEntitiesAsync();
        }
    }
}
