﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Catalog.Infrastructure.Migrations
{
    public partial class PictureAggregateTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "picture_hilo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "PictureExtension",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false, defaultValue: 1),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PictureExtension", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PictureType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false, defaultValue: 1),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PictureType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Picture",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    PictureName = table.Column<string>(nullable: false),
                    PictureExtensionId = table.Column<int>(nullable: true),
                    PictureUri = table.Column<string>(maxLength: 255, nullable: false),
                    PictureUriThumbnail = table.Column<string>(maxLength: 255, nullable: false),
                    Width = table.Column<int>(nullable: false),
                    Height = table.Column<int>(nullable: false),
                    PictureTypeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Picture", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Picture_PictureExtension_PictureExtensionId",
                        column: x => x.PictureExtensionId,
                        principalTable: "PictureExtension",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Picture_PictureType_PictureTypeId",
                        column: x => x.PictureTypeId,
                        principalTable: "PictureType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Picture_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Picture_PictureExtensionId",
                table: "Picture",
                column: "PictureExtensionId");

            migrationBuilder.CreateIndex(
                name: "IX_Picture_PictureTypeId",
                table: "Picture",
                column: "PictureTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Picture_ProductId",
                table: "Picture",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Picture");

            migrationBuilder.DropTable(
                name: "PictureExtension");

            migrationBuilder.DropTable(
                name: "PictureType");

            migrationBuilder.DropSequence(
                name: "picture_hilo");
        }
    }
}
