﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Catalog.Domain.Aggregates.BrandAggregate;
using Catalog.Domain.SeedWork;
using Catalog.Infrastructure.Specifications.Base;
using Microsoft.EntityFrameworkCore;

namespace Catalog.Infrastructure.Specifications
{
    public class BrandQuerySpecification : BaseSpecification<Brand>
    {
        public BrandQuerySpecification() : base(null)
        {
            
        }

        public BrandQuerySpecification(int id) : base(b => b.Id == id)
        {
            
        }

        public BrandQuerySpecification(string sortOrder, int page, int pageSize) 
            : base(null)
        {
            if(String.IsNullOrWhiteSpace(sortOrder) || sortOrder.Equals("Title"))
                ApplyOrderBy(b => b.Title);
            else if(sortOrder.Equals("Title_desc"))
                ApplyOrderByDescending(b => b.Title);

            ApplyPaging((page - 1) * pageSize, pageSize);
        }

        public BrandQuerySpecification(string searchString) 
            : base(b => EF.Functions.Like(b.Title, "%" + searchString + "%"))
        {

        }

        public BrandQuerySpecification(IEnumerable<int> ids): base(c => ids.Contains(c.Id))
        {
            
        }



    }
}
