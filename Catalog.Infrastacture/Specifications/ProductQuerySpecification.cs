﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Catalog.Domain.Aggregates.ProductAggregate;
using Catalog.Infrastructure.Specifications.Base;
using Microsoft.EntityFrameworkCore;

namespace Catalog.Infrastructure.Specifications
{
    public class ProductQuerySpecification : BaseSpecification<Product>
    {
        public ProductQuerySpecification() : base(null)
        {

        }

        public ProductQuerySpecification(string sortOrder, int page, int pageSize)
            : base(null)
        {
            AddInclude(p => p.ProductVersions);

            if(String.IsNullOrWhiteSpace(sortOrder) || sortOrder.Equals("Title"))
                ApplyOrderBy(b => b.Name);
            else if(sortOrder.Equals("Title_desc"))
                ApplyOrderByDescending(b => b.Name);

            ApplyPaging((page - 1) * pageSize, pageSize);
        }

        public ProductQuerySpecification(string searchString)
            : base(b => EF.Functions.Like(b.Name, "%" + searchString + "%"))
        {
            AddInclude(p => p.ProductVersions);
        }

        public ProductQuerySpecification(int id) : base(p => p.Id == id)
        {
            AddInclude(p => p.ProductVersions);
        }

        public ProductQuerySpecification(string sortOrder, int page, int pageSize, int? categoryId = null, int? brandId = null)
            : base(null)

        {
            if(categoryId.HasValue)
                AddCriterion(p => p.CategoryId == categoryId);
            if(brandId.HasValue)
                AddCriterion(p => p.BrandId == brandId);

            AddInclude(p => p.ProductVersions);
        }

    }
}
