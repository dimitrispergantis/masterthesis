﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Infrastructure.Specifications.Base;

namespace Catalog.Infrastructure.Specifications
{
    public class ProductPicturesSpecification: BaseSpecification<Picture>
    {
        public ProductPicturesSpecification(int id) : base(p => p.ProductId == id)
        {
            AddInclude(p => p.PictureExtension);
            AddInclude(p => p.PictureType);
        }
    }
}
