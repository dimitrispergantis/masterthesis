﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Catalog.Infrastructure.Specifications.Base;

namespace Catalog.Infrastructure.Specifications
{
    public class AvailableRootChildrenSpecification: BaseSpecification<Category>
    {
        public AvailableRootChildrenSpecification(int id) : base(c => c.Id!= id && c.ParentCategory == null)
        {
        }

        public AvailableRootChildrenSpecification() : base(c => c.ParentCategory == null)
        {
            
        }
    }
}
