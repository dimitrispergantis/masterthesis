﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Catalog.Infrastructure.Specifications.Base;
using Microsoft.EntityFrameworkCore;

namespace Catalog.Infrastructure.Specifications
{
    public class CategoryQuerySpecification: BaseSpecification<Category>
    {
        public CategoryQuerySpecification() : base(null)
        {
        }

        public CategoryQuerySpecification(int id): base(c => c.Id == id)
        {
            AddInclude(c => c.ParentCategory);
            AddInclude(c => c.ChildrenCategories);
        }

        public CategoryQuerySpecification(string sortOrder, int page, int pageSize) 
            : base(null)
        {
            AddInclude(c => c.ParentCategory);
            AddInclude(c => c.ChildrenCategories);

            if(String.IsNullOrWhiteSpace(sortOrder) || sortOrder.Equals("Title"))
                ApplyOrderBy(b => b.Title);
            else if(sortOrder.Equals("Title_desc"))
                ApplyOrderByDescending(b => b.Title);

            ApplyPaging((page - 1) * pageSize, pageSize);
        }

        public CategoryQuerySpecification(string searchString)
            : base(b => EF.Functions.Like(b.Title, "%" + searchString + "%"))
        {
            AddInclude(c => c.ParentCategory);
            AddInclude(c => c.ChildrenCategories);
        }

        public CategoryQuerySpecification(IEnumerable<int> ids): base(c => ids.Contains(c.Id))
        {
            AddInclude(c => c.ParentCategory);
            AddInclude(c => c.ChildrenCategories);
        }
    }
}
