﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.SeedWork;
using Catalog.Infrastructure.Specifications.Base;

namespace Catalog.Infrastructure.Specifications
{
    public class PictureQuerySpecification: BaseSpecification<Picture>
    {
        public PictureQuerySpecification() : base(null)
        {
        }

        public PictureQuerySpecification(int id): base(p => p.Id == id)
        {
            AddInclude(p => p.PictureExtension);
            AddInclude(p => p.PictureType);
        }

    }
}
