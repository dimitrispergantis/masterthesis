﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Catalog.Infrastructure.Specifications.Base;
using Microsoft.EntityFrameworkCore;

namespace Catalog.Infrastructure.Specifications
{

    public class AvailableChildrenWithParentSpecification: BaseSpecification<Category>
    {
        public AvailableChildrenWithParentSpecification(int id) : base(c => c.Id != id && c.ParentCategory != null)
        {

        }

        public AvailableChildrenWithParentSpecification(): base(c=> c.ParentCategory != null)
        {
            
        }

        
    }
}
