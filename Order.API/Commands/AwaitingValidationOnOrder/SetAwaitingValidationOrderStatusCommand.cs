﻿using System.Runtime.Serialization;
using MediatR;

namespace Ordering.API.Commands.AwaitingValidationOnOrder
{
    public class SetAwaitingValidationOrderStatusCommand : IRequest<bool>
    {

        [DataMember]
        public int OrderNumber { get; private set; }

        public SetAwaitingValidationOrderStatusCommand(int orderNumber)
        {
            OrderNumber = orderNumber;
        }
    }
}