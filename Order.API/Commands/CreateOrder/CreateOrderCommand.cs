﻿using MediatR;
using Ordering.API.IntegrationEvents.Models;

namespace Ordering.API.Commands.CreateOrder
{
    public class CreateOrderCommand: IRequest<bool>
    {
        public CustomerCart Cart { get; set; }

        public CreateOrderCommand(CustomerCart cart)
        {
            Cart = cart;
        }
    }
}
