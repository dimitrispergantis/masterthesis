﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.API.IntegrationEvents.Events;
using Ordering.API.IntegrationEvents.Services;
using Ordering.Domain.Aggregates.OrderAggregate;
using Ordering.Domain.Interfaces;
using Ordering.Infrastructure.Idempotency;

namespace Ordering.API.Commands.CreateOrder
{
    public class CreateOrderCommandHandler: IRequestHandler<CreateOrderCommand, bool>
    {
        private readonly IMediator _mediator;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderIntegrationEventService _orderIntegrationEventService;
        private readonly ILogger<CreateOrderCommandHandler> _logger;
        private readonly IMapper _mapper;

        public CreateOrderCommandHandler(IMediator mediator, IOrderIntegrationEventService orderIntegrationEventService, ILogger<CreateOrderCommandHandler> logger, IMapper mapper, IOrderRepository orderRepository)
        {
            _mediator = mediator;
            _orderIntegrationEventService = orderIntegrationEventService;
            _logger = logger;
            _mapper = mapper;
            _orderRepository = orderRepository;
        }

        public async Task<bool> Handle(CreateOrderCommand command, CancellationToken cancellationToken)
        {
            var paymentType = command.Cart.PaymentType;

            // Add Integration event to clean the basket
            var orderStartedIntegrationEvent = new OrderStartedIntegrationEvent(command.Cart.CustomerId);
            await _orderIntegrationEventService.AddAndSaveEventAsync(orderStartedIntegrationEvent);

            var address = _mapper.Map<Address>(command.Cart.Checkout);

            var order = new Order(command.Cart.CustomerId, command.Cart.Checkout.FirstName, command.Cart.Checkout.LastName, command.Cart.Checkout.Email,
                command.Cart.Checkout.PhoneNumber, command.Cart.Checkout.CompanyName,
                address, paymentType);

            foreach (var cartLine in command.Cart.LineCollection)
            {
                order.AddOrderItem(cartLine.ProductId, cartLine.ProductVersionId, cartLine.ProductName, cartLine.Price, cartLine.Quantity);
            }

            _logger.LogInformation("----- Creating Order - Order: {@Order}", order);

             await _orderRepository.AddOrderAsync(order);

            return await _orderRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);
        }
    }

    public class CreateOrderIdentifiedCommandHandler : IdentifiedCommandHandler<CreateOrderCommand, bool>
    {
        public CreateOrderIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager, ILogger<IdentifiedCommandHandler<CreateOrderCommand, bool>> logger)
            : base(mediator, requestManager, logger)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }

}
