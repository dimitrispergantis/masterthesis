﻿using System;
using System.IO;
using EventLogEntityFramework;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Ordering.Infrastructure;
using Serilog;
using WebHost.Customization;

namespace Ordering.API
{
    public class Program
    {

        public static readonly string Namespace = typeof(Program).Namespace;

        public static readonly string AppName =
            Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static void Main(string[] args)
        {
            var configuration = GetConfiguration();

            Log.Logger = CreateSerilogLogger(configuration);

            //Serilog.Debugging.SelfLog.Enable(Console.Error);

            try
            {

                Log.Information("Configuring web host ({ApplicationContext})...", AppName);

                var webHost = CreateWebHost(configuration, args);

                Log.Information("Applying migrations ({ApplicationContext})...", AppName);

                webHost.MigrateDbContext((OrderContext context, IServiceProvider services) =>
                    {
                        var logger = services.GetRequiredService<ILogger<OrderContextSeed>>();

                        new OrderContextSeed().SeedAsync(context, logger).Wait();
                    })
                    .MigrateDbContext<IntegrationEventLogContext>((_, __) => { });

                Log.Information("Starting web host ({ApplicationContext})...", AppName);

                webHost.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);
            }
            finally
            {
                Log.CloseAndFlush();
            }

        }


        /// <summary>
        /// Δημιουργούμε το WebHost, χρησιμοποιώντας τον default builder.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static IWebHost CreateWebHost(IConfiguration configuration, string[] args) =>
            Microsoft.AspNetCore.WebHost.CreateDefaultBuilder(args)
                .CaptureStartupErrors(false)
                .UseStartup<Startup>()
                .UseConfiguration(configuration)
                .UseSerilog()
                .Build();

        /// <summary>
        /// Δημιουργία του Serilog Logger. Γράφουμε στο console και στον SeqServer
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        private static Serilog.ILogger CreateSerilogLogger(IConfiguration configuration)
        {

            var seqServerUrl = configuration.GetSection("SeqServerUrl").Value;

            return new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .Enrich.WithProperty("ApplicationContext", AppName)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.Seq(seqServerUrl)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        /// <summary>
        /// Επιστρέφουμε το Configuration, το οποίο χρησιμοποιεί τα json files και τις Enviromental variables για να χτήσει το Configuration Root της εφαρμογής
        /// </summary>
        /// <returns></returns>
        private static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            var config = builder.Build();

            return config;
        }
    }
}
