﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ordering.API.IntegrationEvents.Models;
using Ordering.Domain.Aggregates.OrderAggregate;

namespace Ordering.API.Mapping
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Checkout, Address>();
        }
    }
}
