﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Microsoft.Extensions.Logging;
using Ordering.API.Commands.CreateOrder;

namespace Ordering.API.Validations
{
    public class CreateOrderCommandValidator : AbstractValidator<CreateOrderCommand>
    {
        public CreateOrderCommandValidator(ILogger<CreateOrderCommandValidator> logger)
        {
            RuleFor(command => command.Cart).NotEmpty();
            RuleFor(command => command.Cart.LineCollection).NotEmpty();
            RuleFor(command => command.Cart.PaymentType).NotEmpty();
            RuleFor(command => command.Cart.CustomerId).NotEmpty();

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }

    }
}