﻿using FluentValidation;
using Microsoft.Extensions.Logging;
using Ordering.API.Commands;
using Ordering.API.Commands.CreateOrder;

namespace Ordering.API.Validations
{
    public class IdentifiedCommandValidator : AbstractValidator<IdentifiedCommand<CreateOrderCommand,bool>>
    {
        public IdentifiedCommandValidator(ILogger<IdentifiedCommandValidator> logger)
        {
            RuleFor(command => command.Id).NotEmpty();

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
    }
}
