﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.API.Commands;
using Ordering.API.Commands.CreateOrder;
using Ordering.API.IntegrationEvents.Events;
using RabbitMQEventBus.Extensions;
using RabbitMQEventBus.Interfaces;
using Serilog.Context;

namespace Ordering.API.IntegrationEvents.EventHandlers
{
    public class CustomerCheckoutAcceptedIntegrationEventHandler: IIntegrationEventHandler<CustomerCheckoutAcceptedIntegrationEvent>
    {
        private readonly IMediator _mediator;
        private readonly ILogger<CustomerCheckoutAcceptedIntegrationEventHandler> _logger;

        public CustomerCheckoutAcceptedIntegrationEventHandler(IMediator mediator, ILogger<CustomerCheckoutAcceptedIntegrationEventHandler> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        public async Task Handle(CustomerCheckoutAcceptedIntegrationEvent @event)
        {
            _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, Program.AppName, @event);

            var result = false;

            var requestId = @event.CustomerCart.RequestId;

            if (requestId != Guid.Empty)
            {
                using (LogContext.PushProperty("IdentifiedCommandId", requestId))
                {
                    var createOrderCommand = new CreateOrderCommand(@event.CustomerCart);

                    var requestCreateOrder = new IdentifiedCommand<CreateOrderCommand, bool>(createOrderCommand, requestId);

                    _logger.LogInformation(
                        "----- Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                        requestCreateOrder.GetGenericTypeName(),
                        nameof(requestCreateOrder.Id),
                        requestCreateOrder.Id,
                        requestCreateOrder);

                    result = await _mediator.Send(requestCreateOrder);

                    if (result)
                    {
                        _logger.LogInformation("----- CreateOrderCommand succeeded - RequestId: {RequestId}", requestId);
                    }
                    else
                    {
                        _logger.LogWarning("CreateOrderCommand failed - RequestId: {RequestId}", requestId);
                    }
                }
            }
            else
            {
                _logger.LogWarning("Invalid IntegrationEvent - RequestId is missing - {@IntegrationEvent}", @event);
            }

        }
    }
}
