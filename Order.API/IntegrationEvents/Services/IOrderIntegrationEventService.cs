﻿using System;
using System.Threading.Tasks;
using RabbitMQEventBus.Events;

namespace Ordering.API.IntegrationEvents.Services
{
    public interface IOrderIntegrationEventService
    {
        Task AddAndSaveEventAsync(IntegrationEvent evt);
        Task PublishThroughEventBusAsync(Guid transactionId);
    }
}
