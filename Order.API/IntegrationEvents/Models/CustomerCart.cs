﻿using System;
using System.Collections.Generic;

namespace Ordering.API.IntegrationEvents.Models
{
    public class CustomerCart
    {
        public string CustomerId { get;  set; }
        public Checkout Checkout { get; set; }
        public List<CartLine> LineCollection { get; set; }
        public Guid RequestId { get; set; }
        public byte PaymentType { get; set; }

        public CustomerCart(string customerId)
        {
            CustomerId = customerId;
            Checkout = new Checkout();
            LineCollection = new List<CartLine>();
        }
    }
}
