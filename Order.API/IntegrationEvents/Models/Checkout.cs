﻿namespace Ordering.API.IntegrationEvents.Models
{
    public class Checkout
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string CompanyName { get; set; }

        public string AddressName { get; set; }

        public string AddressAdditionalInfo { get; set; }

        public string AddressCity { get; set; }

        public string AddressState { get; set; }

        public string AddressCountry { get; set; }

        public string ZipCode { get; set; }
    }
}
