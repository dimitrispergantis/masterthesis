﻿using System.Collections.Generic;
using Ordering.Domain.Aggregates.OrderAggregate;
using RabbitMQEventBus.Events;

namespace Ordering.API.IntegrationEvents.Events
{
    public class OrderStatusChangedToStockConfirmedIntegrationEvent : IntegrationEvent
    {
        public int OrderId { get; }
        public string OrderStatus { get; }
        public IReadOnlyCollection<OrderItem> OrderItems { get; }
        public int? PaymentType { get; }

        public OrderStatusChangedToStockConfirmedIntegrationEvent(int orderId, string orderStatus, int? paymentType = null, IReadOnlyCollection<OrderItem> orderItems = null)
        {
            OrderId = orderId;
            OrderStatus = orderStatus;
            PaymentType = paymentType;
            OrderItems = orderItems;
        }
    }
}