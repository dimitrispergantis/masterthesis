﻿using System.Collections.Generic;
using RabbitMQEventBus.Events;

namespace Ordering.API.IntegrationEvents.Events
{
    public class OrderStatusChangedToAwaitingValidationIntegrationEvent : IntegrationEvent
    {
        public int OrderId { get; }
        public string OrderStatus { get; }
        public IEnumerable<OrderStockItem> OrderStockItems { get; }

        public OrderStatusChangedToAwaitingValidationIntegrationEvent(int orderId, string orderStatus,
            IEnumerable<OrderStockItem> orderStockItems)
        {
            OrderId = orderId;
            OrderStockItems = orderStockItems;
            OrderStatus = orderStatus;
        }
    }

    public class OrderStockItem
    {
        public int ProductId { get; }
        public int ProductVersionId { get; }
        public int Quantity { get; }

        public OrderStockItem(int productId,int productVersionId, int quantity)
        {
            ProductId = productId;
            Quantity = quantity;
            ProductVersionId = productVersionId;
        }
    }
}