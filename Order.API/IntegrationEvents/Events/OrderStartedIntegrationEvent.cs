﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RabbitMQEventBus.Events;

namespace Ordering.API.IntegrationEvents.Events
{
    public class OrderStartedIntegrationEvent: IntegrationEvent
    {
        public string CustomerId { get; set; }

        public OrderStartedIntegrationEvent(string customerId)
            => CustomerId = customerId;
    }
}
