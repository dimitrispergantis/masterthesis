﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ordering.API.IntegrationEvents.Models;
using RabbitMQEventBus.Events;

namespace Ordering.API.IntegrationEvents.Events
{
    public class CustomerCheckoutAcceptedIntegrationEvent: IntegrationEvent
    {
        public CustomerCart CustomerCart { get;}

        public CustomerCheckoutAcceptedIntegrationEvent(CustomerCart customerCart)
        {
            CustomerCart = customerCart;
        }
    }
}
