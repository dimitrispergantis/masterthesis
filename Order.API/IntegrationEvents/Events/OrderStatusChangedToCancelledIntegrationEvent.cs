﻿using RabbitMQEventBus.Events;

namespace Ordering.API.IntegrationEvents.Events
{
    public class OrderStatusChangedToCancelledIntegrationEvent : IntegrationEvent
    {
        public int OrderId { get; }
        public string OrderStatus { get; }


        public OrderStatusChangedToCancelledIntegrationEvent(int orderId, string orderStatus)
        {
            OrderId = orderId;
            OrderStatus = orderStatus;
        }
    }
}
