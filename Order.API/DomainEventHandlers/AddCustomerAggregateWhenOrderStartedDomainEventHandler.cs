﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.API.IntegrationEvents.Events;
using Ordering.API.IntegrationEvents.Services;
using Ordering.Domain.Aggregates.CustomerAggregate;
using Ordering.Domain.DomainEvents;
using Ordering.Domain.Interfaces;

namespace Ordering.API.DomainEventHandlers
{
    public class AddCustomerAggregateWhenOrderStartedDomainEventHandler : INotificationHandler<OrderStartedDomainEvent>
    {
        private readonly ILoggerFactory _logger;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderIntegrationEventService _orderIntegrationEventService;

        public AddCustomerAggregateWhenOrderStartedDomainEventHandler(ILoggerFactory logger, ICustomerRepository customerRepository, IOrderIntegrationEventService orderIntegrationEventService)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _orderIntegrationEventService = orderIntegrationEventService;
        }

        public async Task Handle(OrderStartedDomainEvent domainEvent, CancellationToken cancellationToken)
        {
            var customer = await _customerRepository.FindCustomerAsync(domainEvent.CustomerId);

            bool customerOriginallyExisted = (customer != null);

            if (!customerOriginallyExisted)
            {
                customer = new Customer(domainEvent.CustomerId, domainEvent.FirstName, domainEvent.LastName, domainEvent.Email, domainEvent.PhoneNumber, domainEvent.CompanyName);
            }

            var customerUpdated = customerOriginallyExisted ?
                _customerRepository.UpdateCustomer(customer) :
                await _customerRepository.AddCustomerAsync(customer);

            customerUpdated.BindCustomerToOrder(domainEvent.Order.Id);

            await _customerRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);

            var orderStatusChangedToSubmittedIntegrationEvent = new OrderStatusChangedToSubmittedIntegrationEvent(domainEvent.Order.Id, domainEvent.Order.OrderStatus.Name);
            await _orderIntegrationEventService.AddAndSaveEventAsync(orderStatusChangedToSubmittedIntegrationEvent);

            _logger.CreateLogger<AddCustomerAggregateWhenOrderStartedDomainEventHandler>()
                .LogTrace("Customer {CustomerId} updated for orderId: {OrderId}.",
                    customerUpdated.Id, domainEvent.Order.Id);
        }
    }
}
