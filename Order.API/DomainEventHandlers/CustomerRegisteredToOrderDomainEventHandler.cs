﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.Domain.DomainEvents;
using Ordering.Domain.Interfaces;

namespace Ordering.API.DomainEventHandlers
{
    public class
        CustomerRegisteredToOrderDomainEventHandler : INotificationHandler<CustomerRegisteredToOrderDomainEvent>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ILoggerFactory _logger;

        public CustomerRegisteredToOrderDomainEventHandler(IOrderRepository orderRepository, ILoggerFactory logger)
        {
            _orderRepository = orderRepository;
            _logger = logger;
        }

        public async Task Handle(CustomerRegisteredToOrderDomainEvent domainEvent, CancellationToken cancellationToken)
        {
            var orderToUpdate = await _orderRepository.GetOrderAsync(domainEvent.OrderId);

            orderToUpdate.SetCustomerId(domainEvent.CustomerId);

            _logger.CreateLogger<CustomerRegisteredToOrderDomainEventHandler>()
                .LogTrace("Order with Id: {OrderId} has been successfully updated with his respective Customer ({Id})",
                    domainEvent.OrderId, domainEvent.CustomerId);
        }
    }
}

