﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Payment.API.Models;

namespace Payment.API.Services
{
    public class PaypalService
    {
        private readonly IConfiguration config;

        public PaypalService(IConfiguration configuration)
        {
            config = configuration;
        }

        public string CreateRequestJson(List<OrderItem> orderItems)
        {
            // Δημιουργώ την λίστα με τα items
            var items = new List<PaypalCreatePaymentRequestItem>();

            // Διατρέχω τα αντικείμενα της παραγγελίας
            foreach (var item in orderItems)
            {
                // Δημιουργώ το PaypalItem
                var paypalItem = new PaypalCreatePaymentRequestItem()
                {
                    Name = item.ProductName,
                    Quantity = item.Quantity.ToString(),
                    Price = item.Price.ToString(System.Globalization.CultureInfo.InvariantCulture),
                    Currency = "EUR"
                };

                // Και το προσθέτω στην λίστα
                items.Add(paypalItem);
            }

            // Θέτω την αντικείμενο ItemList του JSON
            var itemList = new PaypalCreatePaymentRequestItemList()
            {
                Items = items
            };

            // Δημιουργω το αντικείμενο Details
            var details = new PaypalCreatePaymentRequestDetails()
            {
                Subtotal = orderItems.Sum(o => o.Quantity * o.Price).ToString(System.Globalization.CultureInfo.InvariantCulture),
                Shipping = "0".ToString(System.Globalization.CultureInfo.InvariantCulture)

                //Subtotal = "30.00",
                //Shipping = "0.03"
            };

            // Δημιουργω το αντικέιμενο Amount
            var amount = new PaypalCreatePaymentRequestAmount()
            {
                Currency = "EUR",
                Total = (orderItems.Sum(o => o.Quantity * o.Price) + 0).ToString(System.Globalization.CultureInfo.InvariantCulture),
                Details = details
            };

            // Δημιουργω την λίστα με τα transactions
            List<PaypalCreatePaymentRequestTransactions> transactions = new List<PaypalCreatePaymentRequestTransactions>();

            // Δημιουργω το transaction
            PaypalCreatePaymentRequestTransactions transaction = new PaypalCreatePaymentRequestTransactions()
            {
                Amount = amount,
                ItemList = itemList
            };

            // Και το προσθέτω στην λίστα
            transactions.Add(transaction);

            // Τέλος δημιουργω το Request
            var request = new PaypalCreatePaymentRequest()
            {
                Intent = "sale",
                RedirectUrls = new PaypalCreatePaymentRequestRedirectUrls()
                {
                    ReturnUrl = config["Paypal:ReturnUrl"],
                    CancelUrl = config["Paypal:CancelUrl"]
                },
                Payer = new PaypalCreatePaymentRequestPayer(){ PaymentMethod = "paypal" },
                Transactions = transactions.AsEnumerable()
            };

            // Και επιστρέφω το JSON
            return JsonConvert.SerializeObject(request);
        }

        public HttpClient GetPaypalHttpClient()
        {
            string sandbox = config["Paypal:APIUrl"];

            var http = new HttpClient
            {
                BaseAddress = new Uri(sandbox),
                Timeout = TimeSpan.FromSeconds(30),
            };

            return http;
        }

        public async Task<PaypalAccessToken> GetPaypalAccessTokenAsync(HttpClient http)
        {
            var clientId = config["Paypal:ClientId"];
            var secret = config["Paypal:ClientSecret"];

            byte[] bytes = Encoding.GetEncoding("iso-8859-1").GetBytes($"{clientId}:{secret}");

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/v1/oauth2/token");
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(bytes));

            var form = new Dictionary<string, string>
            {
                ["grant_type"] = "client_credentials"
            };

            request.Content = new FormUrlEncodedContent(form);

            HttpResponseMessage response = await http.SendAsync(request);

            string content = await response.Content.ReadAsStringAsync();

            PaypalAccessToken accessToken = JsonConvert.DeserializeObject<PaypalAccessToken>(content);

            return accessToken;
        }

        public async Task<PaypalCreatedResponse> CreatePaypalPaymentAsync(HttpClient http, PaypalAccessToken accessToken, string json)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "v1/payments/payment");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken.AccessToken);

            request.Content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await http.SendAsync(request);

            string content = await response.Content.ReadAsStringAsync();
            PaypalCreatedResponse paypalPaymentCreated = JsonConvert.DeserializeObject<PaypalCreatedResponse>(content);
            return paypalPaymentCreated;
        }

        public async Task<PaypalExecutedResponse> ExecutePaypalPaymentAsync(HttpClient http, PaypalAccessToken accessToken, string paymentId, string payerId)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, $"v1/payments/payment/{paymentId}/execute");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken.AccessToken);

            var payment = JObject.FromObject(new
            {
                payer_id = payerId
            });

            request.Content = new StringContent(JsonConvert.SerializeObject(payment), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await http.SendAsync(request);
            string content = await response.Content.ReadAsStringAsync();
            PaypalExecutedResponse executedPayment = JsonConvert.DeserializeObject<PaypalExecutedResponse>(content);
            return executedPayment;
        }
    }
}
