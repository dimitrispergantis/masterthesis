﻿using System.Collections.Generic;
using Payment.API.Models;
using RabbitMQEventBus.Events;

namespace Payment.API.IntegrationEvents.Events
{
    public class OrderStatusChangedToStockConfirmedIntegrationEvent : IntegrationEvent
    {
        public int OrderId { get; }

        public List<OrderItem> OrderItems { get; }

        public int PaymentType { get;  }

        public OrderStatusChangedToStockConfirmedIntegrationEvent(int orderId, List<OrderItem> orderItems, int paymentType)
        {
            OrderId = orderId;
            OrderItems = orderItems;
            PaymentType = paymentType;
        }
    }
}