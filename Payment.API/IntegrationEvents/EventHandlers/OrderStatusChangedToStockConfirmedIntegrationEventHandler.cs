﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Payment.API.IntegrationEvents.Events;
using Payment.API.Models;
using Payment.API.Services;
using RabbitMQEventBus.Events;
using RabbitMQEventBus.Interfaces;
using Serilog.Context;

namespace Payment.API.IntegrationEvents.EventHandlers
{
    public class OrderStatusChangedToStockConfirmedIntegrationEventHandler :
        IIntegrationEventHandler<OrderStatusChangedToStockConfirmedIntegrationEvent>
    {
        private readonly IEventBus _eventBus;
        private readonly ILogger<OrderStatusChangedToStockConfirmedIntegrationEventHandler> _logger;
        private readonly PaypalService _paypal;
        private readonly IConfiguration _configuration;

        public OrderStatusChangedToStockConfirmedIntegrationEventHandler(
            IEventBus eventBus,
            ILogger<OrderStatusChangedToStockConfirmedIntegrationEventHandler> logger,
            PaypalService paypal, IConfiguration configuration)
        {
            _eventBus = eventBus;
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            _paypal = paypal;
            _configuration = configuration;
        }

        public async Task Handle(OrderStatusChangedToStockConfirmedIntegrationEvent @event)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{@event.Id}-{Program.AppName}"))
            {
                _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, Program.AppName, @event);

                if (@event.PaymentType == PaymentTypes.Paypal)
                {
                    HttpClient http = _paypal.GetPaypalHttpClient();

                    // Step 1: Get an access token
                    PaypalAccessToken accessToken = await _paypal.GetPaypalAccessTokenAsync(http);

                    string json = _paypal.CreateRequestJson(@event.OrderItems);

                    var createdResponse = await _paypal.CreatePaypalPaymentAsync(http, accessToken, json);

                    var paypalRedirect = Uri.EscapeDataString(createdResponse.links.First(x => x.method == "REDIRECT").href);

                    var redirect = _configuration["GatewayUrl"] + $"/payment-api/paypal/redirect?orderId={@event.OrderId}&url={paypalRedirect}";

                    var paypalRedirectUrlIntegrationEvent = new PaypalPaymentInitiatedIntegrationEvent(redirect);

                    _logger.LogInformation("----- Publishing integration event: {IntegrationEventId} from {AppName} - ({@IntegrationEvent})", paypalRedirectUrlIntegrationEvent.Id, Program.AppName, paypalRedirectUrlIntegrationEvent);

                    _eventBus.Publish(paypalRedirectUrlIntegrationEvent);
                }
                else if(@event.PaymentType == PaymentTypes.PayOnDelivery)
                {
                    IntegrationEvent orderPaymentIntegrationEvent = new OrderPaymentSucceededIntegrationEvent(@event.OrderId);;

                    _logger.LogInformation("----- Publishing integration event: {IntegrationEventId} from {AppName} - ({@IntegrationEvent})", orderPaymentIntegrationEvent.Id, Program.AppName, orderPaymentIntegrationEvent);

                    _eventBus.Publish(orderPaymentIntegrationEvent);
                }

                await Task.CompletedTask;
            }
        }
    }
}