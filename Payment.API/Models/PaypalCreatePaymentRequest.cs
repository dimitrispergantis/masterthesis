﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Payment.API.Models
{
    public class PaypalCreatePaymentRequest
    {
        [JsonProperty("intent")]
        public string Intent { get; set; }

        [JsonProperty("redirect_urls")]
        public PaypalCreatePaymentRequestRedirectUrls RedirectUrls { get; set; }

        [JsonProperty("payer")]
        public PaypalCreatePaymentRequestPayer Payer { get; set; }

        [JsonProperty("transactions")]
        public IEnumerable<PaypalCreatePaymentRequestTransactions> Transactions { get; set; }

    }

    public class PaypalCreatePaymentRequestPayer
    {
        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }
    }

    public class PaypalCreatePaymentRequestTransactions
    {
        [JsonProperty("amount")]
        public PaypalCreatePaymentRequestAmount Amount { get; set; }

        [JsonProperty("item_list")]
        public PaypalCreatePaymentRequestItemList ItemList { get; set; }
    }

    public class PaypalCreatePaymentRequestItemList
    {
        [JsonProperty("items")]
        public IEnumerable<PaypalCreatePaymentRequestItem> Items { get; set; }
    }

    public class PaypalCreatePaymentRequestItem
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("quantity")]
        public string Quantity { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }

    public class PaypalCreatePaymentRequestAmount
    {
        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("details")]
        public PaypalCreatePaymentRequestDetails Details { get; set; }
    }

    public class PaypalCreatePaymentRequestDetails
    {
        [JsonProperty("subtotal")]
        public string Subtotal { get; set; }

        [JsonProperty("shipping")]
        public string Shipping { get; set; }
    }

    public class PaypalCreatePaymentRequestRedirectUrls
    {
        [JsonProperty("return_url")]
        public string ReturnUrl { get; set; }

        [JsonProperty("cancel_url")]
        public string CancelUrl { get; set; }
    }
}
