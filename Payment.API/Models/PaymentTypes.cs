﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.API.Models
{
    public static class PaymentTypes
    {
        public static int PayOnDelivery = 1;

        public static int Paypal = 2;
    }
}
