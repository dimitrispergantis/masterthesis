﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.API.Models
{
    public class OrderItem
    {
        public int ProductId { get;  set; }

        public int ProductVersionId { get;  set; }

        public string ProductName { get;  set; }

        public int Quantity { get;  set; }

        public float Price { get;  set; }
    }
}
