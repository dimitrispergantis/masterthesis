﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Payment.API.IntegrationEvents.EventHandlers;
using Payment.API.IntegrationEvents.Events;
using Payment.API.Models;
using Payment.API.Services;
using RabbitMQEventBus.Events;
using RabbitMQEventBus.Interfaces;

namespace Payment.API.Controllers
{
    [Controller]
    public class PaypalController : Controller
    {
        private readonly PaypalService _paypal;
        private readonly IConfiguration _config;
        private readonly IEventBus _eventBus;
        private readonly ILogger<PaypalController> _logger;
        private readonly ISession _session;

        public PaypalController(PaypalService paypal, IConfiguration config, ILogger<PaypalController> logger, IEventBus eventBus, IServiceProvider services)
        {
            _paypal = paypal;
            _config = config;
            _logger = logger;
            _eventBus = eventBus;
            _session = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;
        }

        public IActionResult Redirect(int orderId, string url)
        {
            _session.SetString("OrderId", orderId.ToString());

            return Redirect(url);
        }

        public async Task<IActionResult> Success(string paymentId, string token, string PayerID)
        {
            HttpClient http = _paypal.GetPaypalHttpClient();
            PaypalAccessToken accessToken = await _paypal.GetPaypalAccessTokenAsync(http);

            var success = await _paypal.ExecutePaypalPaymentAsync(http, accessToken, paymentId, PayerID);

            if (success.id == paymentId && success.state == "approved")
            {
                var callbackUrl = _config["CallBackUrl"] + "/Order/Index";

                var orderId = _session.GetString("OrderId");

                IntegrationEvent orderPaymentIntegrationEvent = new OrderPaymentSucceededIntegrationEvent(int.Parse(orderId)); ;

                _logger.LogInformation("----- Publishing integration event: {IntegrationEventId} from {AppName} - ({@IntegrationEvent})", orderPaymentIntegrationEvent.Id, Program.AppName, orderPaymentIntegrationEvent);

                _eventBus.Publish(orderPaymentIntegrationEvent);

                return Redirect(callbackUrl);
            }
            else
            {
                return Redirect("error");
            }

        }

        public IActionResult Returned()
        {
            return Ok();
        }
    }
}