﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalog.Domain.SeedWork;
using Catalog.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Catalog.API.Filters
{
    /// <summary>
    /// Generic ActionFilter το οποίο επικαιροποιεί το κατα πόσο ενα EntityItem είναι καταχωρημένο στην βάση,
    /// με σκοπό να προχωρήσει απρόσκοπτα η ενημέρωση είτε διαγραφή του EntityItem.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ValidateEntityExistsFilter<T>: IAsyncActionFilter where T: class, IAggregateRoot
    {
        private int _id;
        private readonly IRepository<T> _repository;

        public ValidateEntityExistsFilter(IRepository<T> repository)
        {
            _repository = repository;
        }


        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            // Εαν δώσουμε τιμή στο context.Result κανω short circuit το pipeline και επιστρέφουμε
            // διαφορετικά προχωρούμε ασυγχρόνα στο ActionResult (επόμενος κόμβος στο pipeline)

            if (context.ActionArguments.ContainsKey("id"))
            {
                _id = (int) context.ActionArguments["id"];

                if(_id <= 0)
                {
                    context.Result = new BadRequestObjectResult("Invalid query id. It should be a positive integer");
                    return;
                }

                var id = await _repository.FindByIdAsync(_id);

                if (id == 0)
                {
                    context.Result = new NotFoundResult();
                }
                else
                {
                    context.HttpContext.Items.Add("id", id);

                    await next();
                }
            }
            else
            {
                context.Result = new BadRequestResult();
            }

        }

    }
}
