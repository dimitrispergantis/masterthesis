﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Catalog.API.Middlewares
{
    public static class CustomHttpContextExtensions
    {
        public static IApplicationBuilder UseHttpContext(this IApplicationBuilder app)
        {
            CustomHttpContext.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());
            
            return app;
        }
    }
}
