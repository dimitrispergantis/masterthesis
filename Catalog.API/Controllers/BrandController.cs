﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Catalog.API.Commands;
using Catalog.API.Commands.BrandCommands;
using Catalog.API.Filters;
using Catalog.API.Queries;
using Catalog.API.Queries.BrandQueries;
using Catalog.Domain.Aggregates.BrandAggregate;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Catalog.API.Controllers
{
    [Route("api/catalog/brands")]
    [ApiController]
    [Authorize("IsAuthenticatedAdministrator")]
    public class BrandController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BrandController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET api/catalog/brands
        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IActionResult> GetBrandsAsync([FromQuery] BrandQuery query)
        {
            return Ok(await _mediator.Send(query));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("getall")]
        public async Task<IActionResult> GetBrandsAsync()
        {
            return Ok(await _mediator.Send(new AllBrands()));
        }

        // GET api/catalog/brands/{id}
        [HttpGet]
        [AllowAnonymous]
        [Route("{id:int}")]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Brand>))]
        public async Task<IActionResult> GetBrandByIdAsync([FromRoute] int id)
        {
            var query = new BrandById(id);

            return Ok(await _mediator.Send(query));
        }

        // POST api/catalog/brands/create?
        [HttpPost]
        [Route("create")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateBrandAsync([FromBody] CreateBrandCommand command)
        {
            int id = await _mediator.Send(command);

            return CreatedAtAction(nameof(GetBrandByIdAsync), new {id}, null);
        }

        // PUT api/catalog/brands/update/{id}?
        [HttpPut]
        [Route("update/{id:int}")]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Brand>))]
        public async Task<IActionResult> UpdateBrandAsync(int id, [FromBody] UpdateBrandCommand command)
        {
            command.Id = id;

            await _mediator.Publish(command);

            return CreatedAtAction(nameof(GetBrandByIdAsync), new {id}, null);
        }

        // DELETE api/catalog/brands/delete/{id}
        [HttpDelete]
        [Route("delete/{id:int}")]
        [ProducesResponseType((int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Brand>))]
        public async Task<IActionResult> DeleteBrandAsync(int id)
        {
            await _mediator.Publish(new DeleteBrandCommand(id));

            return Ok();
        }


    }
}