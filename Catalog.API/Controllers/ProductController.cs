﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Catalog.API.Commands.ProductCommands;
using Catalog.API.Filters;
using Catalog.API.Queries.ProductQueries;
using Catalog.API.ViewModels;
using Catalog.Domain.Aggregates.ProductAggregate;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Catalog.API.Controllers
{
    [Route("api/catalog/products")]
    [ApiController]
    [Authorize("IsAuthenticatedAdministrator")]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("full_data")]
        public async Task<IActionResult> GetProductsAsync([FromQuery] GetProductsFullDataQuery query)
        {
            var viewModel = await _mediator.Send(query);

            return Ok(viewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("full_data/{id}")]
        public async Task<IActionResult> GetProductFullDataAsync([FromRoute] int id)
        {
            var query = new GetSpecificProductFullData(id);

            var viewModel = await _mediator.Send(query);

            return Ok(viewModel);
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<ProductSummaryViewModel>), (int) HttpStatusCode.OK)]
        public async Task<IActionResult> GetProductsAsync([FromQuery] ProductQuery query)
        {
            var viewModel = await _mediator.Send(query);

            return Ok(viewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{id}")]
        [ProducesResponseType(typeof(ProductViewModel),(int) HttpStatusCode.OK)]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Product>))]
        public async Task<IActionResult> GetProductAsync([FromRoute] int id)
        {
            var query = new ProductById(id);

            var viewModel = await _mediator.Send(query);

            return Ok(viewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{id}/product-versions")]
        [ProducesResponseType(typeof(IReadOnlyList<ProductVersionViewModel>),(int) HttpStatusCode.OK)]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Product>))]
        public async Task<IActionResult> GetProductVersions([FromRoute] int id)
        {
            var query = new ProductVersionsQuery(id);

            var viewModel = await _mediator.Send(query);

            return Ok(viewModel);
        }

        [HttpPost]
        [Route("")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateProductAsync([FromBody] CreateProductCommand command)
        {
            int id = await _mediator.Send(command);

            return CreatedAtAction(nameof(GetProductAsync), new {id}, null);
        }

        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Product>))]
        public async Task<IActionResult> UpdateProductAsync([FromRoute] int id, [FromBody] UpdateProductCommand command)
        {
            command.Id = id;

            await _mediator.Publish(command);

            return CreatedAtAction(nameof(GetProductAsync), new {id}, null);
        }

        [HttpPut]
        [Route("{id}/configure-product-versions")]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Product>))]
        public async Task<IActionResult> ConfigureProductVersionsAsync([FromRoute] int id, [FromBody] ConfigureProductVersionsCommand command)
        {
            command.Id = id;

            await _mediator.Send(command);

            return CreatedAtAction(nameof(GetProductVersions), new {id}, null);
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Product>))]
        public async Task<IActionResult> DeleteProductAsync([FromRoute] int id)
        {
            var command = new DeleteProductCommand(id);

            await _mediator.Publish(command);

            return Ok();

        }
    }
}