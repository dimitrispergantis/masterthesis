﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Catalog.API.Commands.PictureCommands;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;
using System.Text.RegularExpressions;
using Catalog.API.Filters;
using Catalog.API.Queries.PictureQueries;
using Catalog.Domain.Aggregates.ProductAggregate;
using MediatR;
using Microsoft.AspNetCore.Authorization;

namespace Catalog.API.Controllers
{
    [Route("api/catalog/pictures")]
    [ApiController]
    [Authorize("IsAuthenticatedAdministrator")]
    public class PictureController : ControllerBase
    {

        private readonly IMediator _mediator;

        public PictureController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("product/{id}")]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Product>))]
        public async Task<IActionResult> GetProductPicturesAsync([FromRoute] int id)
        {
            var query = new PictureQuery(id);

            return Ok(await _mediator.Send(query));
        }

        [HttpPost]
        [Route("product/{id}")]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Product>))]
        public async Task<IActionResult> UploadProductPicturesAsync([FromRoute] int id, [FromBody] ConfigureProductPicturesCommand command)
        {
            command.Id = id;

            await _mediator.Publish(command);

            return Ok();
        }

    }
}