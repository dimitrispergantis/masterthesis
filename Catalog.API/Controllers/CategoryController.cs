﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Catalog.API.Commands.CategoryCommands;
using Catalog.API.Filters;
using Catalog.API.Queries;
using Catalog.API.Queries.CategoryQueries;
using Catalog.Domain.Aggregates.CategoryAggregate;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace Catalog.API.Controllers
{
    [Route("api/catalog/categories")]
    [ApiController]
    [Authorize("IsAuthenticatedAdministrator")]
    public class CategoryController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CategoryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("{id}")]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Category>))]
        public async Task<IActionResult> GetCategoryAsync([FromRoute] int id)
        {
            var query = new CategoryById(id);

            return Ok(await _mediator.Send(query));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IActionResult> GetCategoriesAsync([FromQuery] CategoryQuery query)
        {
            return Ok(await _mediator.Send(query));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("available-children/parent/{id}")]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Category>))]
        public async Task<IActionResult> GetAvailableChildrenCategoriesPerId([FromRoute] int? id)
        {
            var query = new AvailableChildren(id);

            return Ok(await _mediator.Send(query));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("available-children/")]
        public async Task<IActionResult> GetAvailableChildrenCategories()
        {
            var query = new AvailableChildren();

            return Ok(await _mediator.Send(query));
        }

        [HttpPost]
        [Route("create")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateCategoryAsync([FromBody] CreateCategoryCommand command)
        {
            int id = await _mediator.Send(command);

            return CreatedAtAction(nameof(GetCategoryAsync), new {id}, null);
        }

        [HttpPut]
        [Route("update/{id:int}")]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Category>))]
        public async Task<IActionResult> UpdateCategoryAsync([FromRoute] int id, UpdateCategoryCommand command)
        {
            command.Id = id;

            await _mediator.Publish(command);

            return CreatedAtAction(nameof(GetCategoryAsync), new {id}, null);
        }

        [HttpDelete]
        [Route("delete/{id:int}")]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ServiceFilter(typeof(ValidateEntityExistsFilter<Category>))]
        public async Task<IActionResult> DeleteCategoryAsync([FromRoute] int id)
        {
            var command = new DeleteCategoryCommand(id);

            await _mediator.Publish(command);

            return Ok();
        }
    }
}