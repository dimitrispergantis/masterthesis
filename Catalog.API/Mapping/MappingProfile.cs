﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.Commands.ProductCommands;
using Catalog.API.ViewModels;
using Catalog.Domain.Aggregates.BrandAggregate;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.Aggregates.ProductAggregate;
using Microsoft.Extensions.Hosting;

namespace Catalog.API.Mapping
{
    public class MappingProfile: Profile
    {


        public MappingProfile()
        {
            CreateMap<Brand, BrandViewModel>();

            CreateMap<Category, CategoryViewModel>().ForMember(c => c.Children,
                opt => opt.MapFrom(c => c.ChildrenCategories));
            CreateMap<Category, ChildCategoryViewModel>().ForMember(d => d.Title,
                opt => opt.MapFrom(c => c.GetBreadcrumbCategoryTitle()));

            CreateMap<Product, ProductViewModel>();

            CreateMap<ProductVersionViewModel, ProductVersion>()
                .ConstructUsing(v => new ProductVersion(v.Mpn, v.Size, v.Color, v.Weight, v.InitialPrice, v.Discount,
                    v.AvailableStock, v.RestockThreshold, v.MaxStockThreshold))
                .ForMember(d => d.Id, opt => opt.MapFrom(vm => vm.Id));

            CreateMap<ProductVersion, ProductVersionViewModel>()
                .ForMember(v => v.Mpn, opt => opt.MapFrom(vm => vm.ProductSpecification.Mpn))
                .ForMember(v => v.Size, opt => opt.MapFrom(vm => vm.ProductSpecification.Size))
                .ForMember(v => v.Color, opt => opt.MapFrom(vm => vm.ProductSpecification.Color))
                .ForMember(v => v.Weight, opt => opt.MapFrom(vm => vm.ProductSpecification.Weight))
                .ForMember(v => v.Discount, opt => opt.MapFrom(vm => vm.ProductPrice.Discount))
                .ForMember(v => v.InitialPrice, opt => opt.MapFrom(vm => vm.ProductPrice.InitialPrice))
                .ForMember(v => v.MaxStockThreshold, opt => opt.MapFrom(vm => vm.ProductStock.MaxStockThreshold))
                .ForMember(v => v.AvailableStock, opt => opt.MapFrom(vm => vm.ProductStock.AvailableStock))
                .ForMember(v => v.RestockThreshold, opt => opt.MapFrom(vm => vm.ProductStock.RestockThreshold));

            CreateMap<ProductVersion, ProductVersionSummarizedViewModel>()
                .ForMember(v => v.Specification, opt => opt.MapFrom(vm => vm.ProductSpecification.ToString()))
                .ForMember(v => v.Discount, opt => opt.MapFrom(vm => vm.ProductPrice.Discount))
                .ForMember(v => v.InitialPrice, opt => opt.MapFrom(vm => vm.ProductPrice.InitialPrice))
                .ForMember(v => v.HasStock, opt => opt.MapFrom(vm => vm.ProductStock.IsOnStock()));

            CreateMap<UpdateProductCommand, Product>()
                .ForMember(dest => dest.Name, opt => opt.Ignore())
                .ForMember(dest => dest.Description, opt => opt.Ignore())
                .ForMember(dest => dest.BrandId, opt => opt.Ignore())
                .ForMember(dest => dest.CategoryId, opt => opt.Ignore())
                .ForMember(dest => dest.IsBest, opt => opt.Ignore())
                .AfterMap((src, dest) => dest.SetName(src.Name))
                .AfterMap((src, dest) => dest.SetDescription(src.Description))
                .AfterMap((src, dest) => dest.SetCategory(src.CategoryId))
                .AfterMap((src, dest) => dest.SetBrand(src.BrandId))
                .AfterMap((src, dest) => dest.SetIsBestProduct(src.IsBest));

            CreateMap<Picture, ProductPictureUriViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(v => v.Id))
                .ForMember(dest => dest.PictureTypeId, opt => opt.MapFrom(v => v.PictureType.Id))
                .ForMember(dest => dest.FullPictureUri, opt => opt.MapFrom<MappingToPicturePublicUriResolver>())
                .ForMember(dest => dest.ThumbnailPictureUri, opt => opt.MapFrom<MappingToThumbnailPicturePublicUriResolver>());
        }
    }
}
