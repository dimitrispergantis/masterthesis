﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.Interfaces;

namespace Catalog.API.Mapping
{
    public class MappingToThumbnailPicturePublicUriResolver : IValueResolver<Picture, ProductPictureUriViewModel, string>
    {
        private readonly IImageManipulationService _service;

        public MappingToThumbnailPicturePublicUriResolver(IImageManipulationService service)
        {
            _service = service;
        }

        public string Resolve(Picture source, ProductPictureUriViewModel destination, string destMember,
            ResolutionContext context)
        {
            return _service.GetThumbnailPublicUri(source.PictureName);
        }
    }
}
