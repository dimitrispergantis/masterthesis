#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src

# Caching Docker Compose
COPY MasterThesis.sln .
COPY docker-compose.dcproj /src/

COPY ["OcelotApiGateway/OcelotApiGateway.csproj", "OcelotApiGateway/"]
COPY ["Web.Admin.HttpAggregator/Web.Admin.HttpAggregator.csproj", "Web.Admin.HttpAggregator/"]
COPY ["WebHost.Customization/WebHost.Customization.csproj","WebHost.Customization/"]
COPY ["Catalog.API/Catalog.API.csproj", "Catalog.API/"]
COPY ["Catalog.Domain/Catalog.Domain.csproj", "Catalog.Domain/"]
COPY ["Catalog.Infrastacture/Catalog.Infrastucture.csproj", "Catalog.Infrastacture/"]
COPY ["Catalog.UnitTests/Catalog.UnitTests.csproj","Catalog.UnitTests/"]
COPY ["WebMvc/WebMvc.csproj", "WebMvc/"]
COPY ["Identity.API/Identity.API.csproj", "Identity.API/"]
COPY ["Cart.API/Cart.API.csproj", "Cart.API/"]
COPY ["RabbitMQEventBus/RabbitMQEventBus.csproj" "RabbitMQEventBus/"]
COPY ["EventLogEntityFramework/EventLogEntityFramework.csproj" "EventLogEntityFramework/"]
COPY ["Order.API/Ordering.API.csproj", "Ordering.API/"]
COPY ["Order.Domain/Ordering.Domain.csproj", "Ordering.Domain/"]
COPY ["Order.Infrastructure/Ordering.Infrastructure.csproj", "Ordering.Infrastructure/"]
COPY ["Ordering.BackgroundJobs/Ordering.BackgroundJobs.csproj", "Ordering.BackgroundJobs/"]
COPY ["Payment.API/Payment.API.csproj", "Payment.API/"]
COPY ["Ordering.Notifications/Ordering.Notifications.csproj", "Ordering.Notifications/"]
COPY ["Payment.Notifications/Payment.Notifications.csproj", "Payment.Notifications/"]

RUN dotnet restore MasterThesis.sln
# End Caching Docker Compose

COPY . .
WORKDIR "/src/Catalog.API"
RUN dotnet build "Catalog.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Catalog.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Catalog.API.dll"]
