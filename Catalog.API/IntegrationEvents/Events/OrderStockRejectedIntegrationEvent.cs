﻿using System.Collections.Generic;
using RabbitMQEventBus.Events;

namespace Catalog.API.IntegrationEvents.Events
{
    public class OrderStockRejectedIntegrationEvent : IntegrationEvent
    {
        public int OrderId { get; }

        public List<ConfirmedOrderStockItem> OrderStockItems { get; }

        public OrderStockRejectedIntegrationEvent(int orderId,
            List<ConfirmedOrderStockItem> orderStockItems)
        {
            OrderId = orderId;
            OrderStockItems = orderStockItems;
        }
    }

    public class ConfirmedOrderStockItem
    {
        public int ProductId { get; }
        public int ProductVersionId { get; }
        public bool HasStock { get; }

        public ConfirmedOrderStockItem(int productId, int productVersionId, bool hasStock)
        {
            ProductId = productId;
            HasStock = hasStock;
            ProductVersionId = productVersionId;
        }
    }
}