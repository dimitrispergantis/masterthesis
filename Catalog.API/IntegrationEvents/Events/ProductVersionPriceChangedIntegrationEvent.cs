﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RabbitMQEventBus.Events;

namespace Catalog.API.IntegrationEvents.Events
{
    public class ProductVersionPriceChangedIntegrationEvent: IntegrationEvent
    {
        public int ProductId { get; private set; }

        public int ProductVersionId { get; private set; }

        public float NewPrice { get; private set; }

        public ProductVersionPriceChangedIntegrationEvent(int productId, int productVersionId, float newPrice)
        {
            ProductId = productId;
            NewPrice = newPrice;
            ProductVersionId = productVersionId;
        }
    }
}
