﻿using System.Threading.Tasks;
using Catalog.API.IntegrationEvents.Events;
using Catalog.Infrastructure;
using Microsoft.Extensions.Logging;
using RabbitMQEventBus.Interfaces;
using Serilog.Context;

namespace Catalog.API.IntegrationEvents.EventHandlers
{
    public class OrderStatusChangedToPaidIntegrationEventHandler : IIntegrationEventHandler<OrderStatusChangedToPaidIntegrationEvent>
    {
        private readonly CatalogContext _catalogContext;
        private readonly ILogger<OrderStatusChangedToPaidIntegrationEventHandler> _logger;

        public OrderStatusChangedToPaidIntegrationEventHandler(
            CatalogContext catalogContext,
            ILogger<OrderStatusChangedToPaidIntegrationEventHandler> logger)
        {
            _catalogContext = catalogContext;
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
        }

        public async Task Handle(OrderStatusChangedToPaidIntegrationEvent @event)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{@event.Id}-{Program.AppName}"))
            {
                _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, Program.AppName, @event);

                //we're not blocking stock/inventory
                foreach (var orderStockItem in @event.OrderStockItems)
                {
                    var catalogItem = _catalogContext.ProductVersions.Find(orderStockItem.ProductVersionId);

                    catalogItem.RemoveStock(orderStockItem.Quantity);
                }

                await _catalogContext.SaveChangesAsync();

            }
        }
    }
}