﻿using System;
using System.Threading.Tasks;
using RabbitMQEventBus.Events;

namespace Catalog.API.IntegrationEvents.Services
{
    public interface ICatalogIntegrationEventService
    {
        Task AddAndSaveEventAsync(IntegrationEvent evt);
        Task PublishThroughEventBusAsync(Guid transactionId);

        Task SaveEventAndCatalogContextChangesAsync(IntegrationEvent evt);
        Task PublishThroughEventBusAsync(IntegrationEvent evt);
    }
}
