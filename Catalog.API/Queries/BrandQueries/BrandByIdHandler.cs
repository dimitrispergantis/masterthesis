﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.BrandQueries
{
    public class BrandByIdHandler : IRequestHandler<BrandById, BrandViewModel>
    {
        private readonly IBrandRepository _repository;
        private readonly IMapper _mapper;

        public BrandByIdHandler(IBrandRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<BrandViewModel> Handle(BrandById query, CancellationToken cancellationToken)
        {
            return _mapper.Map<BrandViewModel>(await _repository.GetBrandAsync(query.Id, false));
        }
    }
}
