﻿using System.Collections.Generic;
using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.BrandQueries
{
    public class AllBrands: IRequest<IEnumerable<BrandViewModel>>
    {
    }
}
