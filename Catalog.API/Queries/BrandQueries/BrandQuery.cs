﻿using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.BrandQueries
{
    public class BrandQuery : IRequest<PaginatedItemsViewModel<BrandViewModel>>
    {

        public string SortOrder { get; set; }

        public string SearchString { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }

    }
}
