﻿using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.BrandQueries
{
    public class BrandById : IRequest<BrandViewModel>
    {
        public int Id { get; set; }

        public BrandById(int id)
        {
            Id = id;
        }
    }
}
