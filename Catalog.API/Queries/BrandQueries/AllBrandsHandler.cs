﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.BrandQueries
{
    public class AllBrandsHandler : IRequestHandler<AllBrands, IEnumerable<BrandViewModel>>
    {
        private readonly IBrandRepository _repository;
        private readonly IMapper _mapper;

        public AllBrandsHandler(IBrandRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<BrandViewModel>> Handle(AllBrands request, CancellationToken cancellationToken)
        {
            var brands = await _repository.GetBrandsAsync();

            return _mapper.Map<IEnumerable<BrandViewModel>>(brands);
        }
    }
}
