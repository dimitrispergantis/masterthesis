﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.BrandQueries
{
    public class BrandQueryHandler: IRequestHandler<BrandQuery,PaginatedItemsViewModel<BrandViewModel>>
    {
        private readonly IBrandRepository _repository;
        private readonly IMapper _mapper;

        public BrandQueryHandler(IBrandRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<PaginatedItemsViewModel<BrandViewModel>> Handle(BrandQuery query, CancellationToken cancellationToken)
        {
            var brands = await _repository.GetBrandsAsync(query.SortOrder, query.SearchString, query.Page, query.PageSize);

            var totalItems = await _repository.CountBrandsAsync();

            var result = new PaginatedItemsViewModel<BrandViewModel>(query.Page, query.PageSize, totalItems, 
                _mapper.Map<IEnumerable<BrandViewModel>>(brands));

            return result;
        }
    }
}
