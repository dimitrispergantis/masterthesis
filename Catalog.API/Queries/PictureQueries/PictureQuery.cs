﻿using System.Collections.Generic;
using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.PictureQueries
{
    public class PictureQuery: IRequest<IReadOnlyList<ProductPictureUriViewModel>>
    {
        public int ProductId { get; set; }

        public PictureQuery(int productId)
        {
            ProductId = productId;
        }
    }
}
