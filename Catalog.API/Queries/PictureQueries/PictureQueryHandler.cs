﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.Middlewares;
using Catalog.API.ViewModels;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;

namespace Catalog.API.Queries.PictureQueries
{
    public class PictureQueryHandler: IRequestHandler<PictureQuery, IReadOnlyList<ProductPictureUriViewModel>>
    {
        private readonly IPictureRepository _pictureRepository;
        private readonly IMapper _mapper;

        public PictureQueryHandler(IPictureRepository pictureRepository, IMapper mapper)
        {    
            _pictureRepository = pictureRepository;
            _mapper = mapper;
        }

        public async Task<IReadOnlyList<ProductPictureUriViewModel>> Handle(PictureQuery query, CancellationToken cancellationToken)
        {
            var pictures = await _pictureRepository.GetProductPicturesAsync(query.ProductId, false);

            return _mapper.Map<IReadOnlyList<Picture>,IReadOnlyList<ProductPictureUriViewModel>>(pictures);
        }
    }
}
