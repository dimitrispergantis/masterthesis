﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.CategoryQueries
{
    public class CategoryQueryHandler: IRequestHandler<CategoryQuery, PaginatedItemsViewModel<CategoryViewModel>>
    {
        private readonly ICategoryRepository _repository;
        private readonly IMapper _mapper;

        public CategoryQueryHandler(ICategoryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<PaginatedItemsViewModel<CategoryViewModel>> Handle(CategoryQuery query, CancellationToken cancellationToken)
        {
            var categories =
                await _repository.GetCategoriesAsync(query.SortOrder, query.SearchString, query.Page, query.PageSize);

            var totalCategories = await _repository.CountCategoriesAsync();

            var response = 
                new PaginatedItemsViewModel<CategoryViewModel>(query.PageSize, query.PageSize,
                    totalCategories, _mapper.Map<IEnumerable<CategoryViewModel>>(categories) );

            return response;

        }
    }
}
