﻿using System.Collections.Generic;
using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.CategoryQueries
{
    public class AvailableChildren: IRequest<IReadOnlyList<ChildCategoryViewModel>>
    {
        public int? ParentId { get; set; }

        public AvailableChildren(int? parentId)
        {
            ParentId = parentId;
        }

        public AvailableChildren()
        {
            
        }
    }
}
