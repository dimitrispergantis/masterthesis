﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.CategoryQueries
{
    public class AvailableChildrenHandler: IRequestHandler<AvailableChildren, IReadOnlyList<ChildCategoryViewModel>>
    {
        private readonly ICategoryRepository _repository;
        private readonly IMapper _mapper;

        public AvailableChildrenHandler(ICategoryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IReadOnlyList<ChildCategoryViewModel>> Handle(AvailableChildren query, CancellationToken cancellationToken)
        {
            // Οταν κανω update κατηγορία θα πρέπει να εξαιρέσω απο τα αποτελέσματα την παρούσα κατηγορία (περίπτωση update)
            if (query.ParentId.HasValue)
            {
                var category = await _repository.GetCategoryAsync(query.ParentId.Value, false);
            
                // Εαν ειναι κατηγορία παιδί δεν μπορεί να δεί τα διαθέσιμα παιδία, καθώς δεν επιτρέπεται δέντρο μεγαλύτερο του βαθους 2
                if (!category.IsChildCategory())
                {
                    var availableChildren = await _repository.GetAvailableChildrenCategories(query.ParentId);
                    return _mapper.Map<IReadOnlyList<ChildCategoryViewModel>>(availableChildren);
                }
            }
            // Διαφορετικά συμπεριλαμβάνω ολές τις κατηγορίες παιδία (περίπτωση add).
            else
            {
                var availableChildren = await _repository.GetAvailableChildrenCategories(query.ParentId);
                return _mapper.Map<IReadOnlyList<ChildCategoryViewModel>>(availableChildren);
            }
            
            return new List<ChildCategoryViewModel>();
        }
    }
}
