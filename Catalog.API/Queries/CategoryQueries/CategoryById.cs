﻿using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.CategoryQueries
{
    public class CategoryById: IRequest<CategoryViewModel>
    {
        public int Id { get; set; }

        public CategoryById(int id)
        {
            Id = id;
        }
    }
}
