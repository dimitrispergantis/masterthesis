﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.CategoryQueries
{
    public class CategoryByIdHandler: IRequestHandler<CategoryById,CategoryViewModel>
    {
        private readonly ICategoryRepository _repository;
        private readonly IMapper _mapper;

        public CategoryByIdHandler(ICategoryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<CategoryViewModel> Handle(CategoryById query, CancellationToken cancellationToken)
        {
            var category = await _repository.GetCategoryAsync(query.Id, false);

            return _mapper.Map<CategoryViewModel>(category) ;
        }
    }
}
