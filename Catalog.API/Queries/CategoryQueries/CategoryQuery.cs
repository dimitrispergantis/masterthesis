﻿using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.CategoryQueries
{
    public class CategoryQuery: IRequest<PaginatedItemsViewModel<CategoryViewModel>>
    {
        public string SortOrder { get; set; }

        public string SearchString { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
