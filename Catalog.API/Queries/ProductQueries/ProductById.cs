﻿using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.ProductQueries
{
    public class ProductById: IRequest<ProductViewModel>
    {
        public int Id { get; set; }

        public ProductById(int id)
        {
            Id = id;
        }
    }
}
