﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.ProductQueries
{
    public class ProductVersionsQueryHandler: IRequestHandler<ProductVersionsQuery,IReadOnlyList<ProductVersionViewModel>>
    {
        private readonly IProductRepository _repository;
        private readonly IMapper _mapper;

        public ProductVersionsQueryHandler(IProductRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }


        public async Task<IReadOnlyList<ProductVersionViewModel>> Handle(ProductVersionsQuery query, CancellationToken cancellationToken)
        {
            var versions = await _repository.GetProductVersions(query.Id);

            return _mapper.Map<IReadOnlyList<ProductVersionViewModel>>(versions);
        }
    }
}
