﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.Aggregates.ProductAggregate;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.ProductQueries
{
    public class GetSpecificProductFullDataHandler: IRequestHandler<GetSpecificProductFullData, ProductFullDataViewModel>
    {
        private readonly IProductRepository _productRepository;
        private readonly IPictureRepository _pictureRepository;
        private readonly IMapper _mapper;

        public GetSpecificProductFullDataHandler(IProductRepository productRepository, IPictureRepository pictureRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _pictureRepository = pictureRepository;
            _mapper = mapper;
        }

        public async Task<ProductFullDataViewModel> Handle(GetSpecificProductFullData query, CancellationToken cancellationToken)
        {
            var product =
                await _productRepository.GetProductAsync(query.Id, false);

            var pictures = await _pictureRepository.GetProductPicturesAsync(product.Id, false);
            var picturesViewModel =
                _mapper.Map<IReadOnlyList<Picture>, IReadOnlyList<ProductPictureUriViewModel>>(pictures);

            var versionsViewModel =
                _mapper.Map<IReadOnlyList<ProductVersion>, IReadOnlyList<ProductVersionSummarizedViewModel>>(
                    product.ProductVersions);

            return new ProductFullDataViewModel(product.Id, product.Name, product.Description, product.CategoryId,
                product.BrandId, product.IsBest, versionsViewModel, picturesViewModel);


        }
    }
}
