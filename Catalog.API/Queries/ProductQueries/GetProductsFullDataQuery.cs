﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.ProductQueries
{
    public class GetProductsFullDataQuery: IRequest<PaginatedItemsViewModel<ProductFullDataViewModel>>
    {
        public string SortOrder { get; set; }

        public string SearchString { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }

        public int? CategoryId { get; set; }

        public int? BrandId { get; set; }

        public int? MinPrice { get; set; }

        public int? MaxPrice { get; set; }
    }
}
