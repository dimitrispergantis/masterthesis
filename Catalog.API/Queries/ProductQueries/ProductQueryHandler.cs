﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.ProductQueries
{
    public class ProductQueryHandler: IRequestHandler<ProductQuery,PaginatedItemsViewModel<ProductSummaryViewModel>>
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IBrandRepository _brandRepository;
        private readonly IMapper _mapper;

        public ProductQueryHandler(IProductRepository productRepository, ICategoryRepository categoryRepository, IBrandRepository brandRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _brandRepository = brandRepository;
            _mapper = mapper;
        }

        public async Task<PaginatedItemsViewModel<ProductSummaryViewModel>> Handle(ProductQuery query, CancellationToken cancellationToken)
        {
            var products =
                await _productRepository.GetProductsAsync(query.SortOrder, query.SearchString, query.Page,
                    query.PageSize);

            var productsCount = await _productRepository.CountProducts();

            var categories = await _categoryRepository.GetCategoriesAsync(products.Select(p => p.CategoryId));

            var brands = await _brandRepository.GetBrandsAsync(products.Select(p => p.BrandId));

            var productsSummary = from product in products
                join category in categories on product.CategoryId equals category.Id
                join brand in brands on product.BrandId equals brand.Id
                select new ProductSummaryViewModel(product.Id ,product.Name, product.IsDraft, category.GetBreadcrumbCategoryTitle(),
                    brand.Title, product.IsBest,
                    product.ProductVersions.Count(v => v.ProductStock.IsOnStock()));

            return new PaginatedItemsViewModel<ProductSummaryViewModel>(query.Page, query.PageSize, productsCount, productsSummary);
        }
    }
}
