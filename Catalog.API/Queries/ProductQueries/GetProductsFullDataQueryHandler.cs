﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.Aggregates.ProductAggregate;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.ProductQueries
{
    public class GetProductsFullDataQueryHandler : IRequestHandler<GetProductsFullDataQuery, PaginatedItemsViewModel<ProductFullDataViewModel>>
    {
        private readonly IProductRepository _productRepository;
        private readonly IPictureRepository _pictureRepository;
        private readonly IMapper _mapper;

        public GetProductsFullDataQueryHandler(IProductRepository productRepository, IPictureRepository pictureRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _pictureRepository = pictureRepository;
            _mapper = mapper;
        }

        public async Task<PaginatedItemsViewModel<ProductFullDataViewModel>> Handle(GetProductsFullDataQuery query, CancellationToken cancellationToken)
        {
            var products =
                await _productRepository.GetProductsAsync(query.SortOrder, query.SearchString, query.Page,
                    query.PageSize, query.CategoryId, query.BrandId, query.MinPrice, query.MaxPrice);

            var productsCount = await _productRepository.CountProducts();

            var versionProducts = new List<ProductFullDataViewModel>();

            foreach (var product in products)
            {
                var pictures = await _pictureRepository.GetProductPicturesAsync(product.Id, false);
                var picturesViewModel =
                    _mapper.Map<IReadOnlyList<Picture>, IReadOnlyList<ProductPictureUriViewModel>>(pictures);

                var versionsViewModel =
                    _mapper.Map<IReadOnlyList<ProductVersion>, IReadOnlyList<ProductVersionSummarizedViewModel>>(
                        product.ProductVersions);

                versionProducts.Add(new ProductFullDataViewModel(product.Id, product.Name, product.Description, product.CategoryId, product.BrandId,
                        product.IsBest, versionsViewModel, picturesViewModel));

            }
            return new PaginatedItemsViewModel<ProductFullDataViewModel>(query.Page, query.PageSize, productsCount, versionProducts);
        }
    }
}
