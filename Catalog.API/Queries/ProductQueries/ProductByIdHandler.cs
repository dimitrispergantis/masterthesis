﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.ViewModels;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Queries.ProductQueries
{
    public class ProductByIdHandler: IRequestHandler<ProductById, ProductViewModel>
    {
        private readonly IProductRepository _repository;
        private readonly IMapper _mapper;

        public ProductByIdHandler(IProductRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ProductViewModel> Handle(ProductById query, CancellationToken cancellationToken)
        {
            var product = await _repository.GetProductAsync(query.Id, false);

            return _mapper.Map<ProductViewModel>(product);
        }
    }
}
