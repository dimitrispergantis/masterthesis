﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.ProductQueries
{
    public class GetSpecificProductFullData: IRequest<ProductFullDataViewModel>
    {
        public int Id { get; set; }

        public GetSpecificProductFullData(int id)
        {
            Id = id;
        }
    }
}
