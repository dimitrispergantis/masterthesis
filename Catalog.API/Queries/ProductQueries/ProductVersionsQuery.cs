﻿using System.Collections.Generic;
using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.ProductQueries
{
    public class ProductVersionsQuery: IRequest<IReadOnlyList<ProductVersionViewModel>>
    {
        public int Id { get; set; }

        public ProductVersionsQuery(int id)
        {
            Id = id;
        }
    }
}
