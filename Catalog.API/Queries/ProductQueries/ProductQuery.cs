﻿using Catalog.API.ViewModels;
using MediatR;

namespace Catalog.API.Queries.ProductQueries
{
    public class ProductQuery: IRequest<PaginatedItemsViewModel<ProductSummaryViewModel>>
    {
        public string SortOrder { get; set; }

        public string SearchString { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
