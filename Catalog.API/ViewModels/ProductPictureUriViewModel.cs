﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.API.ViewModels
{
    public class ProductPictureUriViewModel
    {
        public int Id { get; set; }

        public int PictureTypeId { get; set; }

        public string FullPictureUri { get; set; }

        public string ThumbnailPictureUri { get; set; }
    }
}
