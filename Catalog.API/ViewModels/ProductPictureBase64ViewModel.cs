﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Catalog.API.ViewModels
{
    public class ProductPictureBase64ViewModel
    {
        public int Id { get; set; }

        public int PictureTypeId { get; set; }

        public string Base64String { get; set; }
    }
}
