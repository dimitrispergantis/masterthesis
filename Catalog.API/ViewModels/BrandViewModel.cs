﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.API.ViewModels
{
    public class BrandViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public float Discount { get; set; }
    }
}
