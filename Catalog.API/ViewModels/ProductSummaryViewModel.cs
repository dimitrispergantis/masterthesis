﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.API.ViewModels
{
    public class ProductSummaryViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CategoryTitle { get; set; }

        public string BrandTitle { get; set; }

        public bool IsDraft { get; set; }

        public bool IsBestProduct { get; set; }

        public int VersionsOnStock { get; set; }

        public ProductSummaryViewModel(int id,string name, bool isDraft, string categoryTitle, string brandTitle, bool isBestProduct, int versionsOnStock)
        {
            Id = id;
            Name = name;
            IsDraft = isDraft;
            CategoryTitle = categoryTitle;
            BrandTitle = brandTitle;
            IsBestProduct = isBestProduct;
            VersionsOnStock = versionsOnStock;
        }
    }
}
