﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Catalog.API.ViewModels
{
    public class ProductVersionSummarizedViewModel
    {
        public int Id { get; set; }

        public string Specification { get; set; }

        public float InitialPrice { get; set; }

        public float Discount { get; set; }

        public bool HasStock { get; set; }
    }
}
