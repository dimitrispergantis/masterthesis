﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.API.ViewModels
{
    public class ProductFullDataViewModel
    {
        public int ProductId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }

        public int BrandId { get; set; }

        public bool IsBest { get; set; }

        public int VersionId { get; set; }

        public string Specification { get; set; }

        public float InitialPrice { get; set; }

        public float Discount { get; set; }

        public bool HasStock { get; set; }

        public IReadOnlyList<ProductVersionSummarizedViewModel> Versions { get; set; }

        public IReadOnlyList<ProductPictureUriViewModel> Pictures { get; set; }

        public ProductFullDataViewModel(int productId, string name, string description, int categoryId, int brandId, bool isBest,
            IReadOnlyList<ProductVersionSummarizedViewModel> versions ,IReadOnlyList<ProductPictureUriViewModel> pictures)
        {
            ProductId = productId;
            Name = name;
            Description = description;
            CategoryId = categoryId;
            BrandId = brandId;
            IsBest = isBest;
            Versions = versions;
            Pictures = pictures;
        }
    }
}
