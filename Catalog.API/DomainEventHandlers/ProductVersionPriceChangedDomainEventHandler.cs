﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Catalog.API.IntegrationEvents.Events;
using Catalog.API.IntegrationEvents.Services;
using Catalog.Domain.DomainEvents;
using MediatR;

namespace Catalog.API.DomainEventHandlers
{
    public class ProductVersionPriceChangedDomainEventHandler: INotificationHandler<ProductVersionPriceChangedDomainEvent>
    {
        private readonly ICatalogIntegrationEventService _catalogIntegrationEventService;

        public ProductVersionPriceChangedDomainEventHandler(ICatalogIntegrationEventService catalogIntegrationEventService)
        {
            _catalogIntegrationEventService = catalogIntegrationEventService;
        }

        public async Task Handle(ProductVersionPriceChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            //Create Integration Event to be published through the Event Bus
            var priceChangedEvent = new ProductVersionPriceChangedIntegrationEvent(notification.ProductId, notification.ProductVersionId, notification.Price);

            // Achieving atomicity between original Catalog database operation and the IntegrationEventLog thanks to a local transaction
            await _catalogIntegrationEventService.AddAndSaveEventAsync(priceChangedEvent);

        }
    }
}
