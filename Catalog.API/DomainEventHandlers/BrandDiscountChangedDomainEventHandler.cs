﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.DomainEvents;
using MediatR;

namespace Catalog.API.DomainEventHandlers
{
    public class BrandDiscountChangedDomainEventHandler: INotificationHandler<BrandDiscountChangedDomainEvent>
    {
        public Task Handle(BrandDiscountChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
