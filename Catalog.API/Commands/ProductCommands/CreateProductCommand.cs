﻿using MediatR;

namespace Catalog.API.Commands.ProductCommands
{
    public class CreateProductCommand: IRequest<int>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }

        public int BrandId { get; set; }

        public bool IsBest { get; set; }
    }
}
