﻿using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.Exceptions;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Commands.ProductCommands
{
    public class CreateProductCommandHandler: IRequestHandler<CreateProductCommand, int>
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IBrandRepository _brandRepository;

        public CreateProductCommandHandler(IProductRepository productRepository, ICategoryRepository categoryRepository, IBrandRepository brandRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _brandRepository = brandRepository;
        }

        public async Task<int> Handle(CreateProductCommand command, CancellationToken cancellationToken)
        {
            var category = await _categoryRepository.GetCategoryAsync(command.CategoryId, false);
            var brand = await _brandRepository.GetBrandAsync(command.BrandId, false);

            if(category == null || brand == null)
                throw new CatalogDomainException("Δεν ειναι δυνατή η δημιουργία προϊοντος. Η δοθείσα κατηγορία ή κατασκεύαστης δεν υπάρχουν.");
            
            return await _productRepository.CreateProductAsync(command.Name, command.Description, command.IsBest, command.CategoryId, command.BrandId);
        }
    }
}
