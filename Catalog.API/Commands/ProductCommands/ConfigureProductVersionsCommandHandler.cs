﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.Domain.Aggregates.ProductAggregate;
using Catalog.Domain.Interfaces;
using Catalog.Infrastructure.Idempotency;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Catalog.API.Commands.ProductCommands
{
    public class ConfigureProductVersionsCommandHandler: IRequestHandler<ConfigureProductVersionsCommand,bool>
    {
        private readonly IProductCommandService _service;
        private readonly IMapper _mapper;

        public ConfigureProductVersionsCommandHandler(IProductCommandService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public async Task<bool> Handle(ConfigureProductVersionsCommand command, CancellationToken cancellationToken)
        {
            var productVersions = _mapper.Map<List<ProductVersion>>(command.ProductVersionsList);

            await _service.ConfigureProductVersions(command.Id, productVersions);

            return true;

        }
    }
}
