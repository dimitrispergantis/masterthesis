﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Commands.ProductCommands
{
    public class DeleteProductCommandHandler: INotificationHandler<DeleteProductCommand>
    {
        private readonly IProductRepository _repository;

        public DeleteProductCommandHandler(IProductRepository repository)
        {
            _repository = repository;
        }


        public async Task Handle(DeleteProductCommand command, CancellationToken cancellationToken)
        {
            var product = await _repository.GetProductAsync(command.Id);

            await _repository.DeleteProductAsync(product);
        }
    }
}
