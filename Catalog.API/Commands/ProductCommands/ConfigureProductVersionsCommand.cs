﻿using System.Collections.Generic;
using Catalog.API.Behaviors;
using Catalog.API.ViewModels;
using MediatR;
using Newtonsoft.Json;

namespace Catalog.API.Commands.ProductCommands
{
    public class ConfigureProductVersionsCommand: IRequest<bool>
    {
        [JsonIgnore]
        public int Id { get; set; }

        public List<ProductVersionViewModel> ProductVersionsList { get; set; }
    }
}
