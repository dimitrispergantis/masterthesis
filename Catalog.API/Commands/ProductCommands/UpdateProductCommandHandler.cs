﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.Domain.Aggregates.ProductAggregate;
using Catalog.Domain.Exceptions;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Commands.ProductCommands
{
    public class UpdateProductCommandHandler: INotificationHandler<UpdateProductCommand>
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IBrandRepository _brandRepository;
        private readonly IMapper _mapper;

        public UpdateProductCommandHandler(IProductRepository productRepository, ICategoryRepository categoryRepository, IBrandRepository brandRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _brandRepository = brandRepository;
            _mapper = mapper;
        }

        public async Task Handle(UpdateProductCommand command, CancellationToken cancellationToken)
        {
            var product = await _productRepository.GetProductAsync(command.Id);

            var category = await _categoryRepository.GetCategoryAsync(command.CategoryId, false);
            var brand = await _brandRepository.GetBrandAsync(command.BrandId, false);

            if(category == null || brand == null)
                throw new CatalogDomainException("Δεν ειναι δυνατή η ενημέρωση προϊοντος. Η δοθείσα κατηγορία ή κατασκεύαστης δεν υπάρχουν.");
            
            _mapper.Map(command, product);

            await _productRepository.UpdateProductAsync(product);
        }
    }
}
