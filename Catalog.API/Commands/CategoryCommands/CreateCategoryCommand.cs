﻿using System.Collections.Generic;
using MediatR;

namespace Catalog.API.Commands.CategoryCommands
{
    public class CreateCategoryCommand: IRequest<int>
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsFrontPageCategory { get; set; }

        public IEnumerable<int> ChildrenIds { get; set; }
    }
}
