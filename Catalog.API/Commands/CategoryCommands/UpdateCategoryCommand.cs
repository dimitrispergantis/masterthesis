﻿using System.Collections.Generic;
using MediatR;
using Newtonsoft.Json;

namespace Catalog.API.Commands.CategoryCommands
{
    public class UpdateCategoryCommand: INotification
    {
        [JsonIgnore]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsFrontPageCategory { get; set; }

        public IEnumerable<int> ChildrenIds { get; set; }


    }
}
