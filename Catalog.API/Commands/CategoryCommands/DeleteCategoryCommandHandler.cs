﻿using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Commands.CategoryCommands
{
    public class DeleteCategoryCommandHandler: INotificationHandler<DeleteCategoryCommand>
    {
        private readonly ICategoryCommandService _service;

        public DeleteCategoryCommandHandler(ICategoryCommandService service)
        {
            _service = service;
        }

        public async Task Handle(DeleteCategoryCommand command, CancellationToken cancellationToken)
        {
            await _service.DeleteCategoryAsync(command.Id);
        }
    }
}
