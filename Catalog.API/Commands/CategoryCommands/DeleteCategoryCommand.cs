﻿using MediatR;

namespace Catalog.API.Commands.CategoryCommands
{
    public class DeleteCategoryCommand: INotification
    {
        public int Id { get; set; }

        public DeleteCategoryCommand(int id)
        {
            Id = id;
        }
    }
}
