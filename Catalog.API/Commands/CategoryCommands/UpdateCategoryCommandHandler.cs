﻿using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Commands.CategoryCommands
{
    public class UpdateCategoryCommandHandler: INotificationHandler<UpdateCategoryCommand>
    {
        private readonly ICategoryCommandService _service;
        public UpdateCategoryCommandHandler(ICategoryCommandService service)
        {
            _service = service;
        }

        public async Task Handle(UpdateCategoryCommand command, CancellationToken cancellationToken)
        {
           await _service.UpdateExistingCategoryAsync(command.Id, command.Title, command.Description, command.IsFrontPageCategory, command.ChildrenIds);
        }
    }
}
