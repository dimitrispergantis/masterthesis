﻿using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Commands.CategoryCommands
{
    public class CreateCategoryCommandHandler: IRequestHandler<CreateCategoryCommand, int>
    {
        public readonly ICategoryCommandService _service;


        public CreateCategoryCommandHandler(ICategoryCommandService service)
        {
            _service = service;
        }

        public async Task<int> Handle(CreateCategoryCommand command, CancellationToken cancellationToken)
        {
            return await _service.CreateNewCategoryAsync(command.Title, command.Description, command.IsFrontPageCategory, command.ChildrenIds);
        }
    }
}
