﻿using MediatR;

namespace Catalog.API.Commands.BrandCommands
{
    public class CreateBrandCommand: IRequest<int>
    {
        public string Title { get; set; }

        public float Discount { get; set; }
    }
}
