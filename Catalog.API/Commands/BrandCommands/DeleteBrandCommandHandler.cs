﻿using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Commands.BrandCommands
{
    public class DeleteBrandCommandHandler: INotificationHandler<DeleteBrandCommand>
    {
        private readonly IBrandCommandService _service;

        public DeleteBrandCommandHandler(IBrandCommandService service)
        {
            _service = service;
        }

        public async Task Handle(DeleteBrandCommand command, CancellationToken cancellationToken)
        {
            await _service.DeleteBrandAsync(command.Id);
        }
    }
}
