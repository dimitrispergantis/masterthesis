﻿using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Commands.BrandCommands
{
    public class CreateBrandCommandHandler: IRequestHandler<CreateBrandCommand,int>
    {
        private readonly IBrandCommandService _service;
        
        public CreateBrandCommandHandler(IBrandCommandService service)
        {
            _service = service;
        }    

        public async Task<int> Handle(CreateBrandCommand command, CancellationToken cancellationToken)
        {
            int id = await _service.CreateNewBrandAsync(command.Title, command.Discount);

            return id;
        }
    }
}
