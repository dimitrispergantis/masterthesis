﻿using MediatR;
using Newtonsoft.Json;

namespace Catalog.API.Commands.BrandCommands
{
    public class UpdateBrandCommand: INotification
    {
        [JsonIgnore]
        public int Id { get; set; }

        public string Title { get; set; }

        public float Discount { get; set; }

        public UpdateBrandCommand(int id, string title, float discount)
        {
            Id = id;
            Title = title;
            Discount = discount;
        }

        
    }
}
