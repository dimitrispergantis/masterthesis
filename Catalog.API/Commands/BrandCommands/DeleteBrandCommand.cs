﻿using MediatR;

namespace Catalog.API.Commands.BrandCommands
{
    public class DeleteBrandCommand : INotification
    {
        public int Id { get; set; }

        public DeleteBrandCommand(int id)
        {
            Id = id;
        }
    }
}
