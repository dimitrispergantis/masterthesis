﻿using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Commands.BrandCommands
{
    public class UpdateBrandCommandHandler: INotificationHandler<UpdateBrandCommand>
    {
        private readonly IBrandCommandService _service;
        
        public UpdateBrandCommandHandler(IBrandCommandService service)
        {
            _service = service;
        }

        public async Task Handle(UpdateBrandCommand command, CancellationToken cancellationToken)
        {
            await _service.UpdateExistingBrandAsync(command.Id, command.Title, command.Discount);
        }
    }
}
