﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalog.API.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace Catalog.API.Commands.PictureCommands
{
    public class ConfigureProductPicturesCommand: INotification
    {
        [BindNever] [JsonIgnore]
        public int Id { get; set; }

        public List<ProductPictureBase64ViewModel> ProductPicturesViewModel { get; set; }

    }

    
}
