﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.Exceptions;
using Catalog.Domain.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;

namespace Catalog.API.Commands.PictureCommands
{
    public class ConfigureProductPicturesCommandHandler: INotificationHandler<ConfigureProductPicturesCommand>
    {
        private readonly IImageManipulationService _manipulationService;
        private readonly IPictureCommandService _pictureCommandService;
        private readonly IPictureRepository _pictureRepository;
        private readonly IProductRepository _productRepository;

        public ConfigureProductPicturesCommandHandler(IImageManipulationService manipulationService, IPictureRepository pictureRepository,
            IProductRepository productRepository, IPictureCommandService pictureCommandService)
        {
            _manipulationService = manipulationService;
            _pictureRepository = pictureRepository;
            _productRepository = productRepository;
            _pictureCommandService = pictureCommandService;
        }


        public async Task Handle(ConfigureProductPicturesCommand command, CancellationToken cancellationToken)
        {
            // Αρχικοποιώ μια λίστα τύπου Picture. Οι φωτογραφίες θα χρειάστει να μεταφραστούν στο domain model
            // ωστε να εφαρμοστούν τα business rules
            var pictures = new List<Picture>();

            // Βρίσκώ το προϊον στο οποίο θα προστεθούν οι φωτογραφίες
            var product = await _productRepository.GetProductAsync(command.Id, false);

            // Δημιουργω τα Picture domain models
            foreach (var pictureViewModel in command.ProductPicturesViewModel)
            {
                var databasePicture = await _pictureRepository.GetPictureAsync(pictureViewModel.Id);

                // Ελέγχώ αν αναφέρομαι σε ήδη υπάρχουσες φωτογραφίες, τοτε σε αυτη την περίπτωση ενημερώνω τον τύπο τους
                if (databasePicture != null)
                {
                    databasePicture.SetPictureType(pictureViewModel.PictureTypeId);
                    pictures.Add(databasePicture);
                }
                // Διαφορετικά αναφέρομαι σε κανουργία φωτογραφία
                else
                {
                    var bytes = _manipulationService.GetImageBytes(pictureViewModel.Base64String);
                    var mimeType = _manipulationService.GetImageMimeType(pictureViewModel.Base64String);
                    var image = Image.Load(bytes, out IImageFormat format);

                    var newPicture = new Picture(product.Id, product.Name, Guid.NewGuid().ToString() , image.Width, image.Height, pictureViewModel.PictureTypeId,  mimeType );

                    _manipulationService
                        .ImportPictureSpecificsInAddList(newPicture.PictureName, image, format, 
                            newPicture.Width, newPicture.Height, newPicture.ThumbnailWidth, newPicture.ThumbnailHeight);

                    pictures.Add(newPicture);
                }
            }

            // Αποθηκεύω στην βάση την νέα κατάσταση
            await _pictureCommandService.ConfigureProductPictures(product.Id, pictures);

            // Προσθέτω στην private μεταβλητή τoυ filesystem service τις φωτογραφίες για διαγραφή
            foreach (var picture in _pictureCommandService.PicturesToDeleteFromFileSystem())
            {
               _manipulationService.ImportPictureSpecificsInRemoveList(picture.PictureName);
            }
            
        }
    }
}
