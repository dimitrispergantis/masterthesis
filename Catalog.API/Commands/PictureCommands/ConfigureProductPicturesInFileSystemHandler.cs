﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Catalog.Domain.Interfaces;
using MediatR;

namespace Catalog.API.Commands.PictureCommands
{
    public class ConfigureProductPicturesInFileSystemHandler: INotificationHandler<ConfigureProductPicturesCommand>
    {
        private readonly IImageManipulationService _imageManipulationService;

        public ConfigureProductPicturesInFileSystemHandler(IImageManipulationService imageManipulationService)
        {
            _imageManipulationService = imageManipulationService;
        }

        public Task Handle(ConfigureProductPicturesCommand command, CancellationToken cancellationToken)
        {
            _imageManipulationService.SaveImagesToFileSystem();

            _imageManipulationService.DeleteImagesFromFileSystem();

            return Task.CompletedTask;
        }
    }
}
