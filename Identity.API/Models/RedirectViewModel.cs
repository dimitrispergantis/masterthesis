﻿namespace Identity.API.Models
{
    public class RedirectViewModel
    {
        public string RedirectUrl { get; set; }
    }
}
