﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Identity.API.Login;
using Identity.API.Logout;
using Identity.API.Models;
using Identity.API.Subscribe;
using IdentityServer4.Extensions;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Identity.API.Controllers
{
    public class AccountController : Controller
    {
        private readonly IMediator _mediator;
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountController(IMediator mediator, UserManager<ApplicationUser> userManager)
        {
            _mediator = mediator;
            _userManager = userManager;
        }

        /// <summary>
        /// Action η οποία χείριζεται το login του χρήστη είτε τοπικά είτε μέσω External Provider
        /// </summary>
        /// <param name="returnUrl">Περιέχει ολές τις πληροφορίες για το authentication flow</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            // Δημιουργούμε το login model βασισμένοι στο ReturnUrl που μας στέλνει ο client
            var loginFlow = new BuildLoginFlowBasedOnReturnUrl(returnUrl);

            var vm = await _mediator.Send(loginFlow);

            if (vm.IsExternalLoginOnly)
            {
                // Κανουμε Challenge τον external provider όταν έχουμε συνδεθεί με εκείνον (Google Facebook)
                // return RedirectToAction("Challenge", "External", new { provider = vm.ExternalLoginScheme, returnUrl });
            }

            ViewData["ReturnUrl"] = returnUrl;

            return View(vm);
        }

        /// <summary>
        /// Χειρισμός postback για το login
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            // Δημιουργώ το request object
            var request = new SignInUserAndReturnToCaller(model.Email, model.Password, model.ReturnUrl);

            // To περνάω στον αντίστοιχο handler. O handler επιστρέφει το returnUrl, μετά το sign in του χρήστη
            var returnUrl = await _mediator.Send(request);

            // Ελεγχώ το returnUrl. Αν ειναι valid, κάνω το redirect
            if (!returnUrl.IsNullOrEmpty())
                return Redirect(returnUrl);
            else // Διαφορετικά εχω invalid name ή password
            {
                //TODO Επιστρόφη error στο UI invalid name ή password
                ViewData["ReturnUrl"] = model.ReturnUrl;

                ModelState.AddModelError(string.Empty, "Invalid name ή password");

                return View(model);
            }
        }

        /// <summary>
        /// Χρησιμοποιέιται όταν ο χρήστης άκυρώσει το sign in.
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public async Task<IActionResult> RejectAuthentication(string returnUrl)
        {

            var rejectAuthentication = new RejectAuthenticationOnLoginFlow(returnUrl);

            var redirectUrl = await _mediator.Send(rejectAuthentication);

            return Redirect(redirectUrl);

        }

        /// <summary>
        /// Επιστρεφώ το prompt για logout οπού απαιτείται.
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Logout(string logoutId)
        {
            var logoutFlow = new BuildLogoutFlow(logoutId);

            var vm = await _mediator.Send(logoutFlow);

            // Εαν ο handler επιστρέψει logout promt false σημαίνει οτί έρχομαι είτε απο mobile είτε ως κανονικός χρήστης
            if (vm.ShowLogoutPrompt == false)
            {
                // Εαν το request έχει γίνει σωστα authenticated απο τον IdentityServer
                // απλά κάνω sign out τον χρήστη χωρίς prompt
                return await Logout(vm);
            }

            // Επιστρέφω το prompt view. (Προστασία στην περίπτωση που έχω malicious redirect)
            return View(vm);
        }

        /// <summary>
        /// Εκτελώ το postback για το logout.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout(LogoutInputModel model)
        {
            var signOutUser = new SignOutUserBasedOnProvider(model.LogoutId);

            // Κανω log out τον χρήστη και επιστρέφω το view model.
            var vm = await _mediator.Send(signOutUser);

            // Javascript view το οποίο δημιουργει ενα i-frame με το postlogouturl (κοβει το session στο server για τον χρήστη)
            // και επιστρέφει αμέσως με javascript για καλύτερο UX.
            return View("LoggedOut", vm);
        }

        public async Task<IActionResult> DeviceLogOut(string redirectUrl)
        {
            // Σβηνω το authentication cookie
            await HttpContext.SignOutAsync();

            // Θέτω το UI ώστε να φαίνεται ενα ανώνυμος χρήστης
            HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity());

            return Redirect(redirectUrl);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        /// <summary>
        /// Register Action του χρήστη. Εκτελεί το oidc flow, κάνει register τον χρήστη
        /// και στέλνει email επιβεβαίωσης.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            var registerUser = new RegisterUserAndRedirectHimToLogin(model.Email, model.Password, returnUrl);

            var (redirectUrl, errors) = await _mediator.Send(registerUser);

            // Προσθέτουμε τα errors ωστε να τα διορθώσει ο χρήστης
            if (errors.Any())
            {
                //TODO Model State errors στο View
                AddErrors(errors);
                return View(model);
            }

            if (redirectUrl != null)
            {
                return Redirect(redirectUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Action επιβεβαίωσης email. Ολοκληρώνει την εγγραφή χρήστη με το oidc login.
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="code"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userid, string code, string returnUrl)
        {
            var confirmEmail = new ConfirmUserEmail(userid, code, returnUrl);

            var returnValidUrl = await _mediator.Send(confirmEmail);

            return Redirect(returnValidUrl);
        }

        /// <summary>
        /// To redirect με javascript για καλύτερο UX.
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult RedirectWithJavascriptCall(string returnUrl)
        {
            return View("Redirect", new RedirectViewModel { RedirectUrl = returnUrl });;
        }

        [HttpGet]
        public IActionResult Redirecting()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet] //TODO Forgot password flow απο το mobile app. Ιδια υλοποίηση με το confirm email.
        public IActionResult ForgotPassword()
        {
            return View();
        }

        private void AddErrors(List<string> errors)
        {
            foreach (var error in errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }


    }
}