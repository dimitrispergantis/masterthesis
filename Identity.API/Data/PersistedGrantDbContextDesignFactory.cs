﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Identity.API.Data
{
    public class PersistedGrantDbContextDesignFactory : IDesignTimeDbContextFactory<PersistedGrantDbContext>
    {
        public PersistedGrantDbContext CreateDbContext(string[] args)
        {
            string migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            var optionsBuilder =  new DbContextOptionsBuilder<PersistedGrantDbContext>()
                .UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=IdentityDatabase;Trusted_Connection=True;MultipleActiveResultSets=true",
                    x => x.MigrationsAssembly(migrationsAssembly));

            return new PersistedGrantDbContext(optionsBuilder.Options, new OperationalStoreOptions());
        }
    }
}
