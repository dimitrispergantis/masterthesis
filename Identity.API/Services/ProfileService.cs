﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Identity.API.Models;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;

namespace Identity.API.Services
{
    public class ProfileService: IProfileService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserClaimsPrincipalFactory<ApplicationUser> _claimsFactory;

        public ProfileService(UserManager<ApplicationUser> userManager, IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory)
        {
            _userManager = userManager;
            _claimsFactory = claimsFactory;
        }

        /// <summary>
        /// Συνάρτηση η οποία καλείται κάθε φορά που ζήτουνται δεδομένα χρήστη κατα την δημιουργία access-token είτε στο get connect/userinfo .
        /// Σε περίπτωση που θα ήθελα να καλέσω το connect/userinfo χρησιμοποιώντας το access-token, θα πρέπει να θέσω ως scope το openid
        /// για να περάσει το request.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            // Βρίσκω το subscriber id. Είναι το μοναδικο Guid του χρήστη που βρίσκεται στο token
            var sub = context.Subject.GetSubjectId();

            // Βρισκω τον χρήστη απο την βάση δεδομένων χρησιμοποιώντας το id του
            var user = await _userManager.FindByIdAsync(sub) ??  throw new ArgumentException("Invalid subject identifier");

            // Δημιουργώ ενα claims principal, το οποίο περιέχει ολά τα claims του χρήστη όπως ειναι καταχωρημένα στην βάση
            var principal = await _claimsFactory.CreateAsync(user);

            // Παίρνω τα claims του χρήστη και τα φιλτράρω συμφωνα με εκείνα τα οποία έχει κάνει request o client
            var claims = principal.Claims.ToList();
            claims = claims.Where(claim => context.RequestedClaimTypes.Contains(claim.Type)).ToList();

            // Στην συνέχεια προσθέτω τα standard claims του προτόκωλλου ανάλογα με τα δεδομένα που θέλουμε να περάσουμε στον client.
            claims.Add(new Claim(JwtClaimTypes.GivenName, user.UserName));
            claims.Add(new Claim(IdentityServerConstants.StandardScopes.Email, user.Email));

            // Θέτω τα claims στο access token.
            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var subject = context.Subject ?? throw new ArgumentNullException(nameof(context.Subject));

            var subjectId = subject.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Subject)?.Value;

            var user = await _userManager.FindByIdAsync(subjectId);

            context.IsActive = false;

            if (user != null)
            {
                if (_userManager.SupportsUserSecurityStamp)
                {
                    var securityStamp = subject.Claims.Where(c => c.Type == "security_stamp").Select(c => c.Value).SingleOrDefault();

                    if (securityStamp != null)
                    {
                        var dbSecurityStamp = await _userManager.GetSecurityStampAsync(user);
                        if (dbSecurityStamp != securityStamp)
                            return;
                    }
                }

                context.IsActive =
                    !user.LockoutEnabled ||
                    !user.LockoutEnd.HasValue ||
                    user.LockoutEnd <= DateTime.Now;
            }
        }

    }
}
