﻿using System.Threading.Tasks;

namespace Identity.API.Services
{
    public interface IRedirectService
    {
        string ExtractRedirectUriFromReturnUrl(string url);

        Task<string> ExtractOpenEmailAppRedirectUrl(string url);
    }
}
