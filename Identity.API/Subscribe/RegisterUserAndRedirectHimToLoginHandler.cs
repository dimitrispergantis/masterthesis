﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Identity.API.Models;
using Identity.API.Services;
using IdentityServer4.Extensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;

namespace Identity.API.Subscribe
{
    public class RegisterUserAndRedirectHimToLoginHandler : IRequestHandler<RegisterUserAndRedirectHimToLogin, (string,List<string>)>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IRedirectService _redirect;
        private readonly IHttpContextAccessor _accessor;
        private readonly LinkGenerator _generator;


        public RegisterUserAndRedirectHimToLoginHandler(UserManager<ApplicationUser> userManager, IHttpContextAccessor accessor,
            LinkGenerator generator, IRedirectService redirect)
        {
            _userManager = userManager;
            _accessor = accessor;
            _generator = generator;
            _redirect = redirect;
        }

        public async Task<(string,List<string>)> Handle(RegisterUserAndRedirectHimToLogin request, CancellationToken cancellationToken)
        {
            var redirectUrl = String.Empty;
            var context = _accessor.HttpContext;
            var errors = new List<string>();

            // Θετω τις σωστές τιμές σε username και email. Το username μπορεί να αλλάξει μετά μέσω της εφαρμογής
            var newUser = new ApplicationUser { UserName = request.Email, Email = request.Email};

            // Δημιουργώ τον καινούργιο χρήστη
            var result = await _userManager.CreateAsync(newUser, request.Password);

            // Στην περιπτώση επιτυχούς δημιουργίας του, θα πρέπει να στείλω email επιβεβαίωσης διεύθυνσης
            if (result.Succeeded)
            {
                if (request.ReturnUrl != null)
                {
                    if (context.User.IsAuthenticated())
                    {
                        // Κανω redirect στο redirectUrl, είμαι ήδη logged in
                        redirectUrl = request.ReturnUrl;
                    }
                    //θα πρέπει να στείλω email επιβεβαίωσης διεύθυνσης και
                    //στην συνεχεία να ανακατευθύνω τον χρήστη στον login με ορισμα το redirect στον client
                    else
                    {
                        // Το redirectUrl επιστρέφει τον χρήστη στο login app ώστε να συνεχίσει απο εκεί
                        // το authentication flow
                        redirectUrl = _generator
                            .GetPathByAction(context, "Login", "Account",
                                new {returnUrl = request.ReturnUrl});

                    }
                }
            }
            else // Εχω errors στο subscribe, τα επιστρέφω στον χρήστη ώστε να τα διορθώσει.
            {
                errors = SubscriptionErrors(result);
            }

            return (redirectUrl, errors);

        }

        private List<string> SubscriptionErrors(IdentityResult result)
        {
            var errorList = new List<string>();

            if (result.Errors.Any(e => e.Code.Equals(nameof(IdentityErrorDescriber.InvalidUserName))))
                errorList.Add("To username είναι ακύρο. Μπορεί να περιέχει μονο γράμματα ή ψηφία. Προσπαθήστε ξάνα.");

            if (result.Errors.Any(e => e.Code.Equals(nameof(IdentityErrorDescriber.DuplicateEmail))))
                errorList.Add("To email σας είναι ήδη καταχωρημένο. Δοκιμάστε να εγγραφείτε με ενα διαφορετικό email.");


            if (result.Errors.Any(e => e.Code.Equals(nameof(IdentityErrorDescriber.DuplicateUserName))))
                errorList.Add("To username σας είναι ήδη καταχωρημένο. Δοκιμάστε ξανά.");


            if (result.Errors.Any(e => e.Code.Equals(nameof(IdentityErrorDescriber.PasswordTooShort))))
                errorList.Add("To password θα πρέπει να έχει το λιγότερο 6 χαρακτήρες. Δοκιμάστε ξανά.");

            return errorList;
        }

    }
}
