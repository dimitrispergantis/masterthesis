﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Identity.API.Models;
using IdentityServer4.Services;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Identity.API.Subscribe
{
    public class ConfirmUserEmailHandler: IRequestHandler<ConfirmUserEmail,string>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUrlHelper _helper;
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IHttpContextAccessor _accessor;

        public ConfirmUserEmailHandler(UserManager<ApplicationUser> userManager, IUrlHelper helper, IIdentityServerInteractionService interaction, IHttpContextAccessor accessor)
        {
            _userManager = userManager;
            _helper = helper;
            _interaction = interaction;
            _accessor = accessor;
        }

        public async Task<string> Handle(ConfirmUserEmail request, CancellationToken cancellationToken)
        {
            // Βρίσκουμε τον χρήστη ασύγχρόνα
            var user = _userManager.FindByIdAsync(request.UserId).Result;

            // Και επιβεβαιώνουμε το email του, το token έχει lifetime 1 μέρα (default)
            var result = await _userManager.ConfirmEmailAsync(user, request.Code);

            if(result.Errors.Any(e => e.Code.Equals(nameof(IdentityErrorDescriber.InvalidToken))))
            {
                // Do nothing προς το παρόν
            }

            // Ελέγχω επίσης αν το returnUrl ειναι local και είναι valid όσο αφορά το redirect του
            if (_helper.IsLocalUrl(request.ReturnUrl) == false && _interaction.IsValidReturnUrl(request.ReturnUrl) == false)
            {
                // O χρήστης ακολουθησε malicious link. Καλο είναι να μπεί στα logs.
                throw new Exception("Invalid Url");
            }

            return request.ReturnUrl;

        }
    }
}
