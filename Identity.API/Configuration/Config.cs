﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper.Mappers;
using IdentityServer4;
using IdentityServer4.Models;

namespace Identity.API.Configuration
{
    public class Config
    {
        public static IEnumerable<ApiResource> Apis => new List<ApiResource>()
        {
            new ApiResource("catalog", "Catalog Service", new List<string>(){"openid", "profile", "admin_access", "is.administrator"})
        };

        public static IEnumerable<IdentityResource> Resources => new List<IdentityResource>()
        {
            new IdentityResources.OpenId(),
            new IdentityResources.Profile(),
            new IdentityResource("admin_access", new List<string>(){"is.administator"})
        };

        public static IEnumerable<Client> Clients(Dictionary<string, string> clientsUrl)
        {
            return new List<Client>()
            {
                new Client()
                {
                    ClientId = "webmvc",
                    ClientName = "Web MVC Client",
                    ClientSecrets = new List<Secret>(){new Secret("secret".Sha256())},
                    ClientUri = $"{clientsUrl["WebMVC"]}",
                    AllowedGrantTypes = GrantTypes.Hybrid,
                    AllowAccessTokensViaBrowser = false,
                    RequireConsent = false,
                    AllowOfflineAccess = true,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    RedirectUris = new List<string>(){$"{clientsUrl["WebMVC"]}/signin-oidc"},
                    PostLogoutRedirectUris = new List<string>(){$"{clientsUrl["WebMVC"]}/signout-callback-oidc"},
                    AllowedScopes = new List<string>()
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        "catalog"
                    },
                    AccessTokenLifetime = 60*60*2,
                    IdentityTokenLifetime = 60*60*2
                }
            };
        }
    }
}
