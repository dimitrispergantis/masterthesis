﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Identity.API.Models;
using IdentityModel;
using IdentityServer4.Extensions;
using IdentityServer4.Services;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;

namespace Identity.API.Logout
{
    public class SignOutUserBasedOnProviderHandler : IRequestHandler<SignOutUserBasedOnProvider, LoggedOutViewModel>
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly IIdentityServerInteractionService _interaction;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly LinkGenerator _generator;

        public SignOutUserBasedOnProviderHandler(IIdentityServerInteractionService interaction, IHttpContextAccessor accessor,
            SignInManager<ApplicationUser> signInManager, LinkGenerator generator)
        {
            _interaction = interaction;
            _accessor = accessor;
            _signInManager = signInManager;
            _generator = generator;
        }

        public async Task<LoggedOutViewModel> Handle(SignOutUserBasedOnProvider request, CancellationToken cancellationToken)
        {
            var logoutId = request.LogoutId;

            var context = _accessor.HttpContext;

            // Παρίνουμε πληροφορία για τον client (client name, post logout redirect URI and iframe for federated signout)
            var logout = await _interaction.GetLogoutContextAsync(logoutId);

            var vm = new LoggedOutViewModel
            {
                AutomaticRedirectAfterSignOut = true, // Θελούμε αυτοματο redirect χωρίς prompt σελίδα
                PostLogoutRedirectUri = logout?.PostLogoutRedirectUri,
                ClientName = string.IsNullOrEmpty(logout?.ClientName) ? logout?.ClientId : logout?.ClientName,
                SignOutIframeUrl = logout?.SignOutIFrameUrl,
            };

            // Ελέγχώ αν ο χρήστης ειναι διαπιστευμένος μέσω external provider
            if (context.User?.Identity.IsAuthenticated == true)
            {
                var idp = context.User.FindFirst(JwtClaimTypes.IdentityProvider)?.Value;

                if (idp != null && idp != IdentityServer4.IdentityServerConstants.LocalIdentityProvider)
                {
                    // Στηνω το fedareted signout. Οι Facebook και Google providers δεν υποστηρίζούν federated sign out.
                    var providerSupportsSignout = await context.GetSchemeSupportsSignOutAsync(idp);

                    if (providerSupportsSignout)
                    {
                        if (logoutId == null)
                        {
                            // Αν δεν υπάρχει logoutId, δημιουργούμε ενα. Με αυτο τον τρόπο αποκτούμε τις
                            // πληροφοριές που χρειάζεται ο external provider για το signout
                            logoutId = await _interaction.CreateLogoutContextAsync();
                        }

                        // Δημιουργώ το redirect url
                        var redirectUrl = _generator.GetPathByAction(context, "Logout", "Account", new {logoutId});

                        try
                        {
                            await context.SignOutAsync(idp, new AuthenticationProperties {RedirectUri = redirectUrl});
                        }
                        catch (Exception e)
                        {
                            // ignored
                        }
                    }
                }

                // Καθαρίζω τα cookies (sign-out ο χρήστης)
                await _signInManager.SignOutAsync();

            }

            return vm;
        }
    }
}
