﻿using System.Threading;
using System.Threading.Tasks;
using Identity.API.Models;
using IdentityServer4.Services;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Identity.API.Logout
{
    public class BuildLogoutFlowHandler: IRequestHandler<BuildLogoutFlow, LogoutViewModel>
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly IIdentityServerInteractionService _interaction;

        public BuildLogoutFlowHandler(IHttpContextAccessor accessor, IIdentityServerInteractionService interaction)
        {
            _accessor = accessor;
            _interaction = interaction;
        }

        public async Task<LogoutViewModel> Handle(BuildLogoutFlow request, CancellationToken cancellationToken)
        {
            var logoutId = request.LogoutId;

            var user = _accessor.HttpContext.User.Identity;

            var vm = new LogoutViewModel {LogoutId = logoutId};

            //Περίπτωση Xamarin. Είναι ασφαλές να κάνω sign out εφόσον έρχομαι απο την διαπιστεύμενη mobile εφαρμογή
            var context = await _interaction.GetLogoutContextAsync(logoutId);

            // Εαν ο χρήστης δεν ειναι authenticated, τoτε απλά δείχνουμε το logout page.
            // Αποφεύγω επιθέσεις οπού ο χρήστης γίνεται αυτόματα sign out απο malicious web page
            vm.ShowLogoutPrompt = (user.IsAuthenticated != false && context?.ShowSignoutPrompt != false);

            return vm;
        }
    }
}
