﻿using Identity.API.Models;
using MediatR;

namespace Identity.API.Logout
{
    public class BuildLogoutFlow: IRequest<LogoutViewModel>
    {
        public string LogoutId { get; set; }

        public BuildLogoutFlow(string logoutId)
        {
            LogoutId = logoutId;
        }
    }
}
