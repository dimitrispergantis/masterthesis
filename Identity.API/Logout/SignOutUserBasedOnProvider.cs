﻿using Identity.API.Models;
using MediatR;

namespace Identity.API.Logout
{
    public class SignOutUserBasedOnProvider: IRequest<LoggedOutViewModel>
    {
        public string LogoutId { get; set; }

        public SignOutUserBasedOnProvider(string logoutId)
        {
            LogoutId = logoutId;
        }
    }
}
