﻿using System.Threading;
using System.Threading.Tasks;
using Identity.API.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace Identity.API.Login
{
    public class RejectAuthenticationOnLoginFlowHandler: IRequestHandler<RejectAuthenticationOnLoginFlow, string>
    {
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IClientStore _clientStore;
        private readonly IHttpContextAccessor _accessor;
        private readonly LinkGenerator _generator;

        public RejectAuthenticationOnLoginFlowHandler(IIdentityServerInteractionService interaction, IClientStore clientStore, IHttpContextAccessor accessor, LinkGenerator generator)
        {
            _interaction = interaction;
            _clientStore = clientStore;
            _accessor = accessor;
            _generator = generator;
        }

        public async Task<string> Handle(RejectAuthenticationOnLoginFlow request, CancellationToken cancellationToken)
        {
            var returnUrl = request.ReturnUrl;

            // Ελέγχω αν το call εγίνε μέσω authorization request
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl);

            if (context != null)
            {
                // Αν ο user πατήσει το cancel, επιστρέφουμε το consent denied, ακόμα και αν ο client δεν ζητα consent.
                // Αυτη η κίνηση θα στείλει ενα OIDC error access denied στον client.
                await _interaction.GrantConsentAsync(context, ConsentResponse.Denied);

                // Mπορούμε να εμπιστεύτουμε το return url αφού η GetAuthorizationContextAsync επεστρέψε non-null
                if (await _clientStore.IsPkceClientAsync(context.ClientId))
                {
                    // Εαν ο client χρησιμοποιεί PKCE τότε είναι native και επιστρέφουμε με javascript για καλύτερο UX flow
                    var redirectUrlUsingJS = _generator
                        .GetPathByAction(_accessor.HttpContext, "RedirectWithJavascriptCall", "Account", new {returnUrl});

                    return redirectUrlUsingJS;
                }

                // Στην περίπτωση που δεν έχουμε native client, απλά επιστρέφουμε το returnUrl.
                return returnUrl;
            }
            else
            {
                // Αν το context δεν ειναι valid, επιστρέφουμε στην αρχική σελίδα.
                return "~/";
            }
        }
    }
}
