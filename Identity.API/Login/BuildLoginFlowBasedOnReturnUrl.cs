﻿using Identity.API.Models;
using MediatR;

namespace Identity.API.Login
{
    public class BuildLoginFlowBasedOnReturnUrl : IRequest<LoginViewModel>
    {
        public string ReturnUrl { get; set; }

        public BuildLoginFlowBasedOnReturnUrl(string returnUrl)
        {
            ReturnUrl = returnUrl;
        }
    }
}
