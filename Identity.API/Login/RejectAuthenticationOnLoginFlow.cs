﻿using MediatR;

namespace Identity.API.Login
{
    public class RejectAuthenticationOnLoginFlow: IRequest<string>
    {
        public string ReturnUrl { get; set; }

        public RejectAuthenticationOnLoginFlow(string returnUrl)
        {
            ReturnUrl = returnUrl;
        }
    }
}
