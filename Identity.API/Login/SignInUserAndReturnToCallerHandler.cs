﻿using System.Threading;
using System.Threading.Tasks;
using Identity.API.Models;
using Identity.API.Services;
using IdentityServer4.Services;
using MediatR;

namespace Identity.API.Login
{

    public class SignInUserAndReturnToCallerHandler: IRequestHandler<SignInUserAndReturnToCaller, string>
    {
        private readonly ILoginService<ApplicationUser> _loginService;
        private readonly IIdentityServerInteractionService _interaction;

        public SignInUserAndReturnToCallerHandler(ILoginService<ApplicationUser> loginService, IIdentityServerInteractionService interaction)
        {
            _loginService = loginService;
            _interaction = interaction;
        }

        public async Task<string> Handle(SignInUserAndReturnToCaller request, CancellationToken cancellationToken)
        {
            // Βρίσκω τον χρήστη χρησιμοποιώντας το email του
            var user = await _loginService.FindByUsername(request.Email);

            // Αν έχει δώσει σωστό password
            if (await _loginService.ValidateCredentials(user, request.Password))
            {
                // Ορίζω τα default properties του login
                var props = _loginService.SetupDefaultProperties(request.ReturnUrl, true);

                // Και τον συνδέω γράφοντας τις πληροφορίες default cookie του asp.net core
                await _loginService.SignInAsync(user, props);

                // Αν το return Url ειναι ακόμα valid, επέστρεψε στο authorize endpoint (mobile app)
                if (_interaction.IsValidReturnUrl(request.ReturnUrl))
                {
                    return request.ReturnUrl;
                }

                return "~/";
            }

            // Σε περίπτωση που τα credentials ειναι λάθος, επιστρέφω null
            return null;
        }
    }
}
