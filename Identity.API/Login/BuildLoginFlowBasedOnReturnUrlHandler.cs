﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Identity.API.Models;
using IdentityServer4;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using MediatR;
using Microsoft.AspNetCore.Authentication;

namespace Identity.API.Login
{
    public class BuildLoginFlowBasedOnReturnUrlHandler: IRequestHandler<BuildLoginFlowBasedOnReturnUrl, LoginViewModel>
    {
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IClientStore _clientStore;
        private readonly IAuthenticationSchemeProvider _schemeProvider;

        public BuildLoginFlowBasedOnReturnUrlHandler(IIdentityServerInteractionService interaction, IClientStore clientStore, IAuthenticationSchemeProvider schemeProvider)
        {
            _interaction = interaction;
            _clientStore = clientStore;
            _schemeProvider = schemeProvider;
        }

        public async Task<LoginViewModel> Handle(BuildLoginFlowBasedOnReturnUrl request, CancellationToken cancellationToken)
        {
            var returnUrl = request.ReturnUrl;

            // Παίρνω το authorization context σύμφωνα με το return url
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl);

            // Ελέγχω αν προσπαθεί να γίνει σύνδεση απο τον identity provider όπως Google ή Facebook
            // Θα πρέπει να βρώ αυτη την πληροφορία στο request context και στον scheme provider του Identity Server
            // Για να κάνω bypass το login flow θα πρέπει να όρισω στο front channel του request το idp στο acr values.
            if (context?.IdP != null && await _schemeProvider.GetSchemeAsync(context.IdP) != null)
            {
                var local = context.IdP == IdentityServerConstants.LocalIdentityProvider;

                // Χρησιμοποιείται για να κάνει short circuit το UI και να κάνει trigger τον external provider απεύθειας
                var vm = new LoginViewModel()
                {
                    EnableLocalLogin = local,
                    ReturnUrl = returnUrl
                };

                if (!local)
                {
                    vm.ExternalProviders = new[] {new ExternalProvider {AuthenticationScheme = context.IdP}};
                }

                return vm;
            }

            // Βρισκομαι στην περίπτωση που δημιουργώ το flow για κλασικό login απο την html του server.
            // Βρισκω ολά τα authentication schemes
            var schemes = await _schemeProvider.GetAllSchemesAsync();

            // Βρισκούμε την λίστα με τους διαθέσιμους external providers (Google, Facebook)
            var providers = schemes.Where(x => x.DisplayName != null)
                .Select(x => new ExternalProvider
                {
                    DisplayName = x.DisplayName ?? x.Name,
                    AuthenticationScheme = x.Name
                }).ToList();

            var allowLocal = true;

            // Εαν το request έχει ενα διαπιστεύμένο client πχ οι mobile client μας
            if (context?.ClientId != null)
            {
                var client = await _clientStore.FindEnabledClientByIdAsync(context.ClientId);
                if (client != null)
                {
                    allowLocal = client.EnableLocalLogin;
                }
            }

            return new LoginViewModel
            {
                EnableLocalLogin = allowLocal,
                ReturnUrl = returnUrl,
                ExternalProviders = providers.ToArray()
            };
        }
    }
}
