﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Catalog.API.Controllers;
using Catalog.API.Filters;
using Catalog.Domain.Aggregates.BrandAggregate;
using Catalog.Domain.SeedWork;
using Catalog.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Routing;
using Moq;
using Xunit;

namespace Catalog.UnitTests.Application.Filters
{
    public class ValidateEntityExistsUnitTests
    {

        public CatalogContext SeedTestDatabase(string databaseName)
        {
            var ctx = InMemoryDbContextFactory.GetCatalogDbContext(databaseName);
            ctx.Brands.Add(new Brand("TestBrand", 0));
            ctx.SaveChanges();

            return ctx;
        }

        [Fact]
        public async Task ValidateEntityExistsFilter_IfEntityExists_SetHttpContextItem()
        {
            // Arrange

            var ctx = SeedTestDatabase("Entity Exists");

            var mockRepo = new Mock<IRepository<Brand>>();
            mockRepo.Setup(m => m.FindByIdAsync(It.IsAny<int>())).ReturnsAsync(1);

            var controller = new BrandController(Mock.Of<IMediator>());

            var actionContext = new ActionContext(
                new DefaultHttpContext(), Mock.Of<RouteData>(),
                Mock.Of<ActionDescriptor>(), Mock.Of<ModelStateDictionary>());

            var filterMetadata = new Dictionary<string,object>()
            {
                {"id",1}
            };

            var actionExecutingContext = new ActionExecutingContext(actionContext, new List<IFilterMetadata>(),
                filterMetadata, controller);
            
           var context = new ActionExecutedContext(actionContext, new List<IFilterMetadata>(), controller);

           var filter = new ValidateEntityExistsFilter<Brand>(mockRepo.Object);

            // Act

            await filter.OnActionExecutionAsync(actionExecutingContext,  () => { return Task.FromResult(context); });

            // Assert
            Assert.Equal(1, actionExecutingContext.HttpContext.Items["id"]);
            
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task ValidateEntityExistsFilter_IfIdIsNegativeOrZero_ReturnBadRequest(int id)
        {
            // Arrange

            var ctx = InMemoryDbContextFactory.GetCatalogDbContext("Id Is Negative Or Zero");

            var mockRepo = new Mock<IRepository<Brand>>();
            mockRepo.Setup(m => m.FindByIdAsync(It.IsAny<int>())).ReturnsAsync(1);

            var controller = new BrandController(Mock.Of<IMediator>());

            var actionContext = new ActionContext(
                new DefaultHttpContext(), Mock.Of<RouteData>(),
                Mock.Of<ActionDescriptor>(), Mock.Of<ModelStateDictionary>());

            
            var filterMetadata = new Dictionary<string,object>()
            {
                {"id", id}
            };

            var actionExecutingContext = new ActionExecutingContext(actionContext, new List<IFilterMetadata>(),
                filterMetadata, controller);
            
            var context = new ActionExecutedContext(actionContext, new List<IFilterMetadata>(), controller);

            var filter = new ValidateEntityExistsFilter<Brand>(mockRepo.Object);

            // Act

            await filter.OnActionExecutionAsync(actionExecutingContext,  () => { return Task.FromResult(context); });

            // Assert
            Assert.IsType<BadRequestObjectResult>(actionExecutingContext.Result);
        }

        

        [Fact]
        public async Task ValidateEntityExistsFilter_IfIdIsntExistInRouteValues_ReturnBadRequest()
        {
            
            // Arrange

            var ctx = InMemoryDbContextFactory.GetCatalogDbContext("No Route Values");

            var mockRepo = new Mock<IRepository<Brand>>();
            mockRepo.Setup(m => m.FindByIdAsync(It.IsAny<int>())).ReturnsAsync(1);

            var controller = new BrandController(Mock.Of<IMediator>());

            var actionContext = new ActionContext(
                new DefaultHttpContext(), Mock.Of<RouteData>(),
                Mock.Of<ActionDescriptor>(), Mock.Of<ModelStateDictionary>());

            
            var actionExecutingContext = new ActionExecutingContext(actionContext, new List<IFilterMetadata>(),
                new Dictionary<string, object>(), controller);
            
            var context = new ActionExecutedContext(actionContext, new List<IFilterMetadata>(), controller);

            var filter = new ValidateEntityExistsFilter<Brand>(mockRepo.Object);

            // Act

            await filter.OnActionExecutionAsync(actionExecutingContext,  () => { return Task.FromResult(context); });

            // Assert
            Assert.IsType<BadRequestResult>(actionExecutingContext.Result);
        }

        [Fact]
        public async Task ValidateEntityExistsFilter_IfObjectAfterQueryIsNull_ReturnNotFound()
        {
            // Arrange

            var ctx = SeedTestDatabase("Query Is Returning Null");

            var mockRepo = new Mock<IRepository<Brand>>();
            mockRepo.Setup(m => m.FindByIdAsync(It.IsAny<int>())).ReturnsAsync(0);

            var controller = new BrandController(Mock.Of<IMediator>());

            var actionContext = new ActionContext(
                new DefaultHttpContext(), Mock.Of<RouteData>(),
                Mock.Of<ActionDescriptor>(), Mock.Of<ModelStateDictionary>());

            
            var filterMetadata = new Dictionary<string,object>()
            {
                {"id", 2}
            };

            var actionExecutingContext = new ActionExecutingContext(actionContext, new List<IFilterMetadata>(),
                filterMetadata, controller);
            
            var context = new ActionExecutedContext(actionContext, new List<IFilterMetadata>(), controller);

            var filter = new ValidateEntityExistsFilter<Brand>(mockRepo.Object);

            // Act

            await filter.OnActionExecutionAsync(actionExecutingContext,  () => { return Task.FromResult(context); });

            // Assert
            Assert.IsType<NotFoundResult>(actionExecutingContext.Result);
        }
    }
}
