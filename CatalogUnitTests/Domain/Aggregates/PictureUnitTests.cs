﻿using System;
using System.Collections.Generic;
using System.Text;
using Catalog.Domain.Aggregates.PictureAggregate;
using Xunit;

namespace Catalog.UnitTests.Domain.Aggregates
{
    public class PictureUnitTests
    {
        [Theory]
        [InlineData("Test Picture","guid", 1, "image/jpeg", "test_picture_guid.jpeg")]
        [InlineData("Test Picture","guid", 1, "image/png", "test_picture_guid.png")]
        public void SetPictureName_WhenNameIsValid_SetsTheNameInProperForm(string name, string guid, int typeId, string mimeType, string expected)
        {
            // Arrange
            var picture = new Picture(1, "test", "guid" , 1000, 1000, 1, "image/jpeg");
            
            // Act
            picture.SetPictureName(name, guid , mimeType);

            // Assert
            Assert.Equal(expected, picture.PictureName);
        }
    }
}
