﻿using System;
using Catalog.Domain.Aggregates.BrandAggregate;
using Catalog.Domain.Exceptions;
using Xunit;

namespace Catalog.UnitTests.Domain.Aggregates
{
    public class BrandUnitTests
    {
        [Fact]
        public void SetTitle_WhenTitleIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            var brand = new Brand("Test Brand",0);

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => brand.SetTitle(null));

        }

        [Fact]
        public void SetTitle_WhenTitleIsEmpty_ThrowDomainException()
        {
            // Arrange
            var brand = new Brand("Test Brand",0);

            // Act & Assert
            Assert.Throws<CatalogDomainException>(() => brand.SetTitle("   "));
        }

        [Theory]
        [InlineData("Test Brand","Test Brand")]
        [InlineData("test brand", "Test Brand")]
        [InlineData("test Brand", "Test Brand")]
        [InlineData("Test brand", "Test Brand")]
        [InlineData("tEst bRAND", "Test Brand")]
        [InlineData("test","Test")]

        public void SetTitle_WhenTitleIsBadFormated_FormatHimProperly(string title, string expected)
        {
            // Arrange
            var brand = new Brand("Test Brand", 0);

            // Act
            brand.SetTitle(title);

            // Assert
            Assert.Equal(expected,brand.Title);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(2)]
        public void SetDiscount_WhenDiscountIsOutOfRange_ThrowDomainException(float discount)
        {
            // Arrange 
            var brand = new Brand("Test Brand", 0);

            // Act & Assert
            Assert.Throws<CatalogDomainException>((() => { brand.SetDiscount(discount); }));
        }

        [Fact]
        public void SetDiscount_WhenDiscountIsInRange_SetDiscount()
        {
            // Arrange
            var brand = new Brand("Test Brand", 0);

            // Act
            brand.SetDiscount((float) 0.1);

            // Assert
            Assert.Equal((float) 0.1, brand.Discount);

        }
        
    }
}