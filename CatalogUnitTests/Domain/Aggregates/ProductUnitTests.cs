﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Catalog.Domain.Aggregates.ProductAggregate;
using Catalog.Domain.Exceptions;
using Catalog.Domain.SeedWork;
using Xunit;

namespace Catalog.UnitTests.Domain.Aggregates
{
    public class ProductUnitTests
    {
        [Theory]
        [InlineData("Test Brand","Test Brand")]
        [InlineData("test brand", "Test Brand")]
        [InlineData("test Brand", "Test Brand")]
        [InlineData("Test brand", "Test Brand")]
        [InlineData("tEst bRAND", "Test Brand")]
        [InlineData("test","Test")]
        public void SetName_WhenNameHasInvalidFormat_FixIt(string name, string expected)
        {
            // Arrange
            var product = new Product("Test Name", "Test", 1, 1, false);

            // Act
            product.SetName(name);

            // Assert
            Assert.Equal(expected,product.Name);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(2)]
        public void SetDiscount_WhenDiscountIsOutOfRange_ThrowException(float discount)
        {
            // Arrange
            var product = new Product("Test Name", "Test", 1, 1, false);
            
            // Act & Assert
            Assert.Throws<CatalogDomainException>(() => { product.SetDiscount(discount); });
        }

        [Fact]
        public void SetDiscount_WhenDiscountIsInRange_AppliesTheDiscountToAllItems()
        {
            // Arrange
            var product = new Product("test", "test", 1, 1, false);
            var versionOne = new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 10);
            var versionTwo = new ProductVersion("test", "test", null, null, 10, (float) 0.1, 10, 10, 10);
            product.AddProductVersion(versionOne);
            product.AddProductVersion(versionTwo);
            
            // Act
            product.SetDiscount((float) 0.2);

            // Assert
            Assert.Equal((float) 8, product.ProductVersions.First().ProductPrice.GetPrice());
            Assert.Equal((float) 8, product.ProductVersions.Last().ProductPrice.GetPrice());
            
        }

        [Fact]
        public void SetIsDraft_WhenProductVersionsAreZero_ReturnTrue()
        {
            // Arrange
            var product = new Product("test", "test", 1, 1,false);

            // Act
            product.SetIsDraft();

            // Assert
            Assert.True(product.IsDraft);
        }

        [Fact]
        public void SetIsDraft_WhenProductHasVersions_DraftIsFalse()
        {
            // Arrange
            var product = new Product("test", "test", 1, 1, false);
            product.AddProductVersion(new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 10));

            // Act
            product.SetIsDraft();

            // Assert
            Assert.False(product.IsDraft);
        }

        [Fact]
        public void AddProductVersion_WhenThereIsDuplicateSpecification_ThrowException()
        {
            // Arrange
            var product = new Product("test", "test", 1, 1, false);
            product.AddProductVersion(new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 10));

            // Act & Assert
            Assert.Throws<CatalogDomainException>(() =>
            {
                product.AddProductVersion(new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 10));
            });

        }

        [Fact]
        public void AddProductVersion_WhenWeHaveANewVersion_AddIt()
        {
            // Arrange
            var product = new Product("test", "test", 1, 1, false);
            
            // Act
            product.AddProductVersion(new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 10));

            // Assert
            Assert.Equal(1, product.ProductVersions.Count);
        }

        [Fact]
        public void UpdateProductVersion_WhenVersionIsNotExists_ThrowException()
        {
            // Arrange
            var product = new Product("test", "test", 1, 1, false);
            product.AddProductVersion(new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 10));
            
            // Reflection to set private id
            typeof(ProductVersion).GetProperty("Id").SetValue(product.ProductVersions.SingleOrDefault(), 1, null);
            
            // Act & Assert
            Assert.Throws<CatalogDomainException>(() =>
            {
                product.UpdateProductVersion(new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 20));
            });
        }

        [Fact]
        public void UpdateProductVersion_WhenVersionExists_UpdateTheCorrespondingValues()
        {
            // Arrange
            var product = new Product("test", "test", 1, 1, false);
            product.AddProductVersion(new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 10));

            var updated = new ProductVersion("MPN New", "XL", "Red", "10gr", 10, (float) 0.1, 10, 10, 20);
            
            // Reflection to set private id
            typeof(ProductVersion).GetProperty("Id").SetValue(product.ProductVersions.SingleOrDefault(), 1, null);
            typeof(ProductVersion).GetProperty("Id").SetValue(updated, 1, null);

            // Act
            product.UpdateProductVersion(updated);

            // Assert
            Assert.Equal("MPN New", product.ProductVersions.First().ProductSpecification.Mpn);

        }

        [Fact]
        public void RemoveProductVersion_WhenVersionDoesntExist_ThrowException()
        {
            // Arrange
            var product = new Product("test", "test", 1, 1, false);
            product.AddProductVersion(new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 10));
            
            // Reflection to set private id
            typeof(ProductVersion).GetProperty("Id").SetValue(product.ProductVersions.SingleOrDefault(), 1, null);
            
            // Act & Assert
            Assert.Throws<CatalogDomainException>(() =>
            {
                product.RemoveProductVersion(2);
            });
        }

        [Fact]
        public void RemoveProductVersion_WhenProductVersionIsOne_RemoveVersionAndSetDraftToTrue()
        {
            // Arrange
            var product = new Product("test", "test", 1, 1, false);
            product.AddProductVersion(new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 10));
            
            // Reflection to set private id
            typeof(ProductVersion).GetProperty("Id").SetValue(product.ProductVersions.SingleOrDefault(), 1, null);
            
            // Act & Assert
            product.RemoveProductVersion(1);

            // Assert
            Assert.Equal(0, product.ProductVersions.Count);
            Assert.True(product.IsDraft);
        }

        [Fact]
        public void RemoveProductVersion_WhenProductVersionsAreGreaterThanOne_JustRemoveTheVersion()
        {
            // Arrange
            var product = new Product("test", "test", 1, 1, false);
            var versionOne = new ProductVersion(null, null, null, null, 10, (float) 0.1, 10, 10, 10);
            var versionTwo = new ProductVersion("test", "test", null, null, 10, (float) 0.1, 10, 10, 10);
            product.AddProductVersion(versionOne);
            product.AddProductVersion(versionTwo);
            
            // Reflection to set private id
            typeof(ProductVersion).GetProperty("Id").SetValue(product.ProductVersions.FirstOrDefault(), 1, null);
            
            // Act & Assert
            product.RemoveProductVersion(1);

            // Assert
            Assert.Equal(1, product.ProductVersions.Count);
            Assert.False(product.IsDraft);
        }

    }


}
