﻿using System.Collections.Generic;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Catalog.Domain.Exceptions;
using Xunit;

namespace Catalog.UnitTests.Domain.Aggregates
{
    public class CategoryUnitTests
    {
        [Fact]
        public void SetParentCategory_IfCategoryExistsInChildren_ThrowException()
        {
            // Arrange
            var child = new Category("Child Title", "Child Description", true, null, null);

            var childrenList = new List<Category> {child};

            var category = new Category("Test Title", "Test Description", true, null, childrenList);

            // Act & Assert          
            Assert.Throws<CatalogDomainException>(() => { category.SetParentCategory(child); });
        }

        [Fact]
        public void SetParentCategory_WhenCategoryIsValid_SetsParent()
        {
            // Arrange
            var parent = new Category("Parent Title", "Parent Description", true, null, null);
            
            var category = new Category("Test Title", "Test Description", true, null, null);

            // Act
            category.SetParentCategory(parent);

            // Assert
            Assert.Equal(parent, category.ParentCategory);
        }

        [Fact]
        public void SetParentCategory_WhenParentIsNull_SetsNull()
        {
            // Arrange
            var parent = new Category("Parent Title", "Parent Description", true, null, null);
            
            var category = new Category("Test Title", "Test Description", true, null, null);

            // Act
            category.SetParentCategory(null);

            // Assert
            Assert.Null(category.ParentCategory);
        }

        [Fact]
        public void AddChildCategory_IfObjectIsNull_ThrowException()
        {
            // Arrange
            var category = new Category("Test Title", "Test Description", true, null, null);

            // Act & Assert
            Assert.Throws<CatalogDomainException>(() => category.AddChildCategory(null));
        }

        [Fact]
        public void AddChildCategory_IfChildExistsAlready_ThrowException()
        {
            // Arrange
            var child = new Category("Child Title", "Child Description", true, null, null);

            var childrenList = new List<Category> {child};

            var category = new Category("Test Title", "Test Description", true, null, childrenList);

            // Act & Assert
            Assert.Throws<CatalogDomainException>(() => category.AddChildCategory(child));
            
        }

        [Fact]
        public void AddChildCategory_IfChildIsNodeParent_ThrowsException()
        {
            // Arrange
            var parent = new Category("Parent Title", "Parent Description", true, null, null);

            var category = new Category("Test Title", "Test Description", true, parent, null);

            // Act & Assert
            Assert.Throws<CatalogDomainException>(() => category.AddChildCategory(parent));
        }

        [Fact]
        public void AddChildCategory_IfChildIsValid_AddChild()
        {
            // Arrange
            var child = new Category("Child Title", "Child Description", true, null, null);
            var category = new Category("Test Title", "Test Description", true, null, null);

            // Act
            category.AddChildCategory(child);

            // Assert
            Assert.Equal(1, category.ChildrenCategories.Count);
        }

        [Fact]
        public void RemoveChildCategory_RemovesChild()
        {
            // Arrange
            var child = new Category("Child Title", "Child Description", true, null, null);

            var childrenList = new List<Category> {child};

            var category = new Category("Test Title", "Test Description", true, null, childrenList);

            // Act
            category.RemoveChildCategory(child);

            // Assert
            Assert.Equal(0, category.ChildrenCategories.Count);
        }

        [Fact]
        public void ClearChildrenCategories_EmptiesTheList()
        {
            // Arrange
            var child = new Category("Child Title", "Child Description", true, null, null);

            var childrenList = new List<Category> {child};

            var category = new Category("Test Title", "Test Description", true, null, childrenList);
            
            // Act
            category.ClearChildrenCategories();
            
            // Assert
            Assert.Equal(0, category.ChildrenCategories.Count);
        }

        [Fact]
        public void IsParentCategory_WhenCategoryHasChildren_ReturnsTrue()
        {
            // Arrange
            var child = new Category("Child Title", "Child Description", true, null, null);

            var childrenList = new List<Category> {child};

            var category = new Category("Test Title", "Test Description", true, null, childrenList);

            //  Act & Assert
            Assert.True(category.IsParentCategory());
        }

        [Fact]
        public void IsParentCategory_WhenChildrenAreNullOrEmpty_ReturnsFalse()
        {
            // Arrange
            var category = new Category("Test Title", "Test Description", true, null, null);

            //  Act & Assert
            Assert.False(category.IsParentCategory());
        }


    }
}
