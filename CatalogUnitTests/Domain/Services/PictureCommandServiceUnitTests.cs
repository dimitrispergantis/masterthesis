﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.PictureAggregate;
using Catalog.Domain.Aggregates.ProductAggregate;
using Catalog.Domain.Exceptions;
using Catalog.Domain.Interfaces;
using Catalog.Domain.Services;
using Catalog.Infrastructure.Repositories;
using Moq;
using Xunit;

namespace Catalog.UnitTests.Domain.Services
{
    public class PictureCommandServiceUnitTests
    {
        [Fact]
        public async Task ConfigureProductPictures_WhenListIsEmpty_DeleteAllPictures()
        {
            // Arrange
            var picture = new Picture(1, "Test", "test", 1000, 1000, 1, "image/png");
            typeof(Picture).GetProperty("Id").SetValue(picture, 1, null);

            var pictureList = new List<Picture>();

            var mockRepo = new Mock<IPictureRepository>();
            mockRepo.Setup(r => r.GetProductPicturesAsync(It.IsAny<int>(), It.IsAny<bool>()))
                .ReturnsAsync((new List<Picture>() {picture}).AsReadOnly);

            var service = new PictureCommandService(mockRepo.Object);

            // Act

            await service.ConfigureProductPictures(1, pictureList);

            // Assert
            Assert.Equal(1, service.PicturesToDeleteFromFileSystem().Count);
        }

        [Fact]
        public async Task ConfigureProductPictures_WhenThereIsAComplexList_PerformAllOperationsProperly()
        {
            // Arrange
            var pictureOne = new Picture(1, "Test", "test", 1000, 1000, 1, "image/png");
            var pictureTwo = new Picture(1, "Test", "test", 1000, 1000, 2, "image/png");
            var pictureThree = new Picture(1, "Test", "test", 1000, 1000, 2, "image/png");
            
            typeof(Picture).GetProperty("Id").SetValue(pictureOne, 1, null);
            typeof(Picture).GetProperty("Id").SetValue(pictureTwo, 2, null);
            typeof(Picture).GetProperty("Id").SetValue(pictureThree, 3, null);

            var mockRepo = new Mock<IPictureRepository>();
            mockRepo.Setup(r => r.GetProductPicturesAsync(It.IsAny<int>(), It.IsAny<bool>()))
                .ReturnsAsync((new List<Picture>() {pictureOne, pictureTwo, pictureThree}).AsReadOnly);

            var pictureOneUpdated = new Picture(1, "Test", "test", 1000, 1000, 2, "image/png");
            typeof(Picture).GetProperty("Id").SetValue(pictureOneUpdated, 1, null);
            
            
            var pictureForInsert = new Picture(1, "Test", "test", 1000, 1000, 1, "image/png");

            var pictures = new List<Picture>(){pictureOneUpdated, pictureForInsert};

            var service = new PictureCommandService(mockRepo.Object);

            // Act
            await service.ConfigureProductPictures(1, pictures);

            // Assert
            mockRepo.Verify(r => r.DeletePicturesAsync(It.Is<IEnumerable<Picture>>(p => p.Count() == 2)));
            mockRepo.Verify(r => r.UpdatePicturesAsync(It.Is<IEnumerable<Picture>>(p => p.SingleOrDefault().GetPictureType().Id == 2)));
            mockRepo.Verify(r => r.AddPicturesAsync(It.Is<IEnumerable<Picture>>(p => p.SingleOrDefault().GetPictureType().Id == 1)));
        }
        
        [Fact]
        public async Task ConfigureProductPictures_WhenThereIsAComplexListWithInvalidFinalState_ThrowException()
        {
            // Arrange
            var pictureOne = new Picture(1, "Test", "test", 1000, 1000, 1, "image/png");
            var pictureTwo = new Picture(1, "Test", "test", 1000, 1000, 2, "image/png");
            var pictureThree = new Picture(1, "Test", "test", 1000, 1000, 2, "image/png");
            
            typeof(Picture).GetProperty("Id").SetValue(pictureOne, 1, null);
            typeof(Picture).GetProperty("Id").SetValue(pictureTwo, 2, null);
            typeof(Picture).GetProperty("Id").SetValue(pictureThree, 3, null);

            var mockRepo = new Mock<IPictureRepository>();
            mockRepo.Setup(r => r.GetProductPicturesAsync(It.IsAny<int>(), It.IsAny<bool>()))
                .ReturnsAsync((new List<Picture>() {pictureOne, pictureTwo, pictureThree}).AsReadOnly);

            var pictureOneUpdated = new Picture(1, "Test", "test", 1000, 1000, 1, "image/png");
            typeof(Picture).GetProperty("Id").SetValue(pictureOneUpdated, 1, null);
            
            
            var pictureForInsert = new Picture(1, "Test", "test", 1000, 1000, 1, "image/png");

            var pictures = new List<Picture>(){pictureOneUpdated, pictureForInsert};

            var service = new PictureCommandService(mockRepo.Object);

            // Act & Assert
            await Assert.ThrowsAsync<CatalogDomainException>(async () => { await service.ConfigureProductPictures(1, pictures); });
        }
    }
}
