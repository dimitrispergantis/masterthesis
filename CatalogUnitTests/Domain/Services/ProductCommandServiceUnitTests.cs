﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Catalog.Domain.Aggregates.ProductAggregate;
using Catalog.Domain.Interfaces;
using Catalog.Domain.Services;
using Moq;
using Xunit;

namespace Catalog.UnitTests.Domain.Services
{
    public class ProductCommandServiceUnitTests
    {
        private readonly Product _testProduct;

        public ProductCommandServiceUnitTests()
        {
            _testProduct = new Product("Test Product", "Test Description", 1, 1, true);
            var versionOne = new ProductVersion("Test 1", "Test 1", "Test", "Test", (float) 0.0, (float) 0.0, 100, 100,
                100);
            var versionTwo = new ProductVersion("Test 2", "Test 2", "Test", "Test", (float) 0.0, (float) 0.0, 100, 100,
                100);
            var versionThree = new ProductVersion("Test 3", "Test 3", "Test", "Test", (float) 0.0, (float) 0.0, 100,
                100, 100);
            
            typeof(ProductVersion).GetProperty("Id").SetValue(versionOne, 1, null);
            typeof(ProductVersion).GetProperty("Id").SetValue(versionTwo, 2, null);
            typeof(ProductVersion).GetProperty("Id").SetValue(versionThree, 3, null);
            
            
            var versions = new List<ProductVersion>() {versionOne, versionTwo, versionThree};
            foreach (var productVersion in versions)
            {
                _testProduct.AddProductVersion(productVersion);
            }
        }

        [Fact]
        public async Task ConfigureProductVersions_WhenProductVersionsAreEmpty_RemoveProductVersionsAndSetProductToDraft()
        {
            // Arrange
            Mock<IProductRepository> productRepo = new Mock<IProductRepository>();
            productRepo.Setup(p => p.GetProductAsync(It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(_testProduct);

            var service  = new ProductCommandService(productRepo.Object);

            // Act
            await service.ConfigureProductVersions(1, new List<ProductVersion>());

            // Assert
            productRepo.Verify(p => p.UpdateProductAsync(It.Is<Product>(v => v.ProductVersions.Count == 0)));
        }

        
        [Fact]
        public async Task ConfigureProductVersions_WhenNewVersionsAreAboutToInserted_UpdateTheVersions()
        {
            // Arrange
            var newVersions = new List<ProductVersion>()
            {
                new ProductVersion("Test 2", "Test 2", "Test 2", "Test 2", (float) 0.0, (float) 0.0, 100, 100, 100)
            };

            Mock<IProductRepository> productRepo = new Mock<IProductRepository>();
            productRepo.Setup(p => p.GetProductAsync(It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(_testProduct);

            var service  = new ProductCommandService(productRepo.Object);

            // Act
            await service.ConfigureProductVersions(1, newVersions);

            // Assert
            productRepo.Verify(p => p.UpdateProductAsync(It.Is<Product>(v => v.ProductVersions.Count == 4)));
        }

        [Fact]
        public async Task ConfigureProductVersions_WhenOldVersionsAreDeleted_UpdateTheVersions()
        {
            // Arrange
            var versionTwo = new ProductVersion("Test 2", "Test 2", "Test", "Test", (float) 0.0, (float) 0.0, 100, 100,
                100);
            var versionThree = new ProductVersion("Test 3", "Test 3", "Test", "Test", (float) 0.0, (float) 0.0, 100,
                100, 100);
            typeof(ProductVersion).GetProperty("Id").SetValue(versionTwo, 2, null);
            typeof(ProductVersion).GetProperty("Id").SetValue(versionThree, 3, null);

            var oldTruncatedVersions = new List<ProductVersion>(){versionTwo, versionThree};

            Mock<IProductRepository> productRepo = new Mock<IProductRepository>();
            productRepo.Setup(p => p.GetProductAsync(It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(_testProduct);

            var service  = new ProductCommandService(productRepo.Object);

            // Act
            await service.ConfigureProductVersions(1, oldTruncatedVersions);


            // Assert
            productRepo.Verify(p => p.UpdateProductAsync(It.Is<Product>(v => v.ProductVersions.Count == 2)));
        }

        [Fact]
        public async Task ConfigureProductVersions_WhenOldVersiosAreGoingToUpdate_UpdateTheVersions()
        {
            // Arrange
            var versionTwo = new ProductVersion("Test Updated", "Test 2", "Test", "Test", (float) 10, (float) 0.0, 100, 100,
                100);
            typeof(ProductVersion).GetProperty("Id").SetValue(versionTwo, 2, null);

            var updatedVersions = new List<ProductVersion>(){versionTwo};

            Mock<IProductRepository> productRepo = new Mock<IProductRepository>();
            productRepo.Setup(p => p.GetProductAsync(It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(_testProduct);

            var service  = new ProductCommandService(productRepo.Object);

            // Act
            await service.ConfigureProductVersions(1, updatedVersions);

            // Assert
            productRepo.Verify(p => 
                p.UpdateProductAsync(It.Is<Product>(v => v.ProductVersions.Where(pv => pv.Id == 2).SingleOrDefault().ProductPrice.InitialPrice == 10)));
        }

        [Fact]
        public async Task ConfigureProductsVersions_WhenThereIsAMixOfCommands_UpdateTheVersions()
        {
            // Arrange
            var newVersions = new List<ProductVersion>()
            {
                new ProductVersion("Test New", "Test New", "Test New", "Test New", (float) 0.0, (float) 0.0, 100, 100, 100)
            };

            var updateVersionThree = new ProductVersion("Test 3", "Test 3", "Test", "Test", (float) 0.0, (float) 0.0, 100, 100, 100);
            typeof(ProductVersion).GetProperty("Id").SetValue(updateVersionThree, 3, null);

            var updateVersionOne = new ProductVersion("Test 1", "Test 1", "Test", "Test", (float) 15, (float) 0.15, 100, 100, 100);
            typeof(ProductVersion).GetProperty("Id").SetValue(updateVersionOne, 1, null);

            var postVersions = (new List<ProductVersion>() {updateVersionOne, updateVersionThree}).Concat(newVersions).ToList();
            
            Mock<IProductRepository> productRepo = new Mock<IProductRepository>();
            productRepo.Setup(p => p.GetProductAsync(It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(_testProduct);

            var service  = new ProductCommandService(productRepo.Object);

            // Act
            await service.ConfigureProductVersions(1, postVersions);

            // Assert
            productRepo.Verify(p => 
                p.UpdateProductAsync(It.Is<Product>(v => v.ProductVersions.SingleOrDefault(pv => pv.Id == 1).ProductPrice.Equals(new ProductPrice((float) 15, (float) 0.15)))));
            productRepo.Verify(p => 
                p.UpdateProductAsync(It.Is<Product>(v => v.ProductVersions.Count == 3)));
            productRepo.Verify(p =>
                p.UpdateProductAsync(It.Is<Product>(pv => pv.ProductVersions.All(v => v.Id != 2))));
        }


       
    }
}
