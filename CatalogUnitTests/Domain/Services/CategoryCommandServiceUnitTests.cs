﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain.Aggregates.CategoryAggregate;
using Catalog.Domain.Exceptions;
using Catalog.Domain.Interfaces;
using Catalog.Domain.Services;
using Microsoft.AspNetCore.Mvc.TagHelpers.Cache;
using Moq;
using Xunit;

namespace Catalog.UnitTests.Domain.Services
{
    public class CategoryDomainServiceUnitTests
    {
        [Fact]
        public async void CreateNewCategoryAsync_WhenChildIsAlreadyAParent_ThrowException()
        {
            // Arrange
            var childrenList = new List<Category>()
            {
                new Category("Child Category", "Child", true, null, null)
            };
            var wannabeChild = new Category("Wannabe", "Wannabe", true, null, childrenList);
            
            var category = new Category("Test Category", "Test", true, null, null);

            Mock<ICategoryRepository> mockRepository = new Mock<ICategoryRepository>();
            mockRepository.Setup(c => c.GetCategoriesAsync(It.IsAny<IEnumerable<int>>(), It.IsAny<bool>()))
                .ReturnsAsync(new List<Category>() {wannabeChild});
            mockRepository.Setup(c => c.GetCategoriesAsync()).ReturnsAsync(new List<Category>());

            var service = new CategoryCommandService(mockRepository.Object);
            
            // Act & Assert
            await Assert.ThrowsAsync<CatalogDomainException>(async () =>
            {
                await service.CreateNewCategoryAsync("Test Category", "Test", true, new List<int>() {1});
            });
        }

        [Fact]
        public async void CreateNewCategoryAsync_WhenTitleIsDuplicate_ThrowException()
        {
            // Arrange
            var wannabeChild = new Category("Test Category", "Wannabe", true, null, null);
            
            var category = new Category("Test Category", "Test", true, null, null);

            Mock<ICategoryRepository> mockRepository = new Mock<ICategoryRepository>();
            mockRepository.Setup(c => c.GetCategoriesAsync(It.IsAny<IEnumerable<int>>(), It.IsAny<bool>()))
                .ReturnsAsync(new List<Category>() {wannabeChild});
            mockRepository.Setup(c => c.GetCategoriesAsync()).ReturnsAsync(new List<Category>() {wannabeChild});

            var service = new CategoryCommandService(mockRepository.Object);

            // Act & Assert
            await Assert.ThrowsAsync<CatalogDomainException>(async () =>
            {
                await service.CreateNewCategoryAsync("Test Category", "Test", true, new List<int>() {1});
            });
        }

        [Fact]
        public async void CreateNewCategoryAsync_WhenChildrenHasOtherParent_UpdateTheTree()
        {
            // Arrange
            
            var child1 = new Category("Child Category 1", "Child 1", true, null, null);
            var child2 = new Category("Child Category 2", "Child 2", true, null, null);
            
            // Reflection to set private id
            typeof(Category).GetProperty("Id").SetValue(child1, 1, null);
            typeof(Category).GetProperty("Id").SetValue(child2, 2, null);

            var childrenList = new List<Category>() {child1, child2};
            
            var oldParent = new Category("Old Parent", "Old Description", true, null, childrenList);
            child1.SetParentCategory(oldParent);
            child2.SetParentCategory(oldParent);

            var categoryBeforeTreeUpdate = new Category("Title", "Description", true, null, null);

            Mock<ICategoryRepository> mockRepository = new Mock<ICategoryRepository>();
            mockRepository.Setup(c => c.GetCategoriesAsync(It.IsAny<IEnumerable<int>>(), It.IsAny<bool>())).ReturnsAsync(new List<Category>() {child1, child2});
            mockRepository.Setup(c => c.GetCategoryAsync(It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(categoryBeforeTreeUpdate);
            mockRepository.Setup(c => c.GetCategoriesAsync()).ReturnsAsync(new List<Category>());

            var service = new CategoryCommandService(mockRepository.Object);
            
            // Act 
            await service.CreateNewCategoryAsync( "Title", "Description", true, new List<int>() {1,2});

            // Assert
            mockRepository.Verify(c =>  
                c.CreateCategoryAsync(It.Is<Category>(v => 
                    v.ChildrenCategories.FirstOrDefault().Equals(child1)
                    && 
                    v.ChildrenCategories.LastOrDefault().Equals(child2))
                ), Times.Exactly(1));
        }

        [Fact]
        public async void CreateNewCategoryAsync_WhenAllCategoriesAreFree_AddCategoryAndUpdateTree()
        {
            // Arrange
            
            var child1 = new Category("Child Category 1", "Child 1", true, null, null);
            var child2 = new Category("Child Category 2", "Child 2", true, null, null);
            
            // Reflection to set private id
            typeof(Category).GetProperty("Id").SetValue(child1, 1, null);
            typeof(Category).GetProperty("Id").SetValue(child2, 2, null);

            var categoryBeforeTreeUpdate = new Category("Title", "Description", true, null, null);

            Mock<ICategoryRepository> mockRepository = new Mock<ICategoryRepository>();
            mockRepository.Setup(c => c.GetCategoriesAsync(It.IsAny<IEnumerable<int>>(), It.IsAny<bool>())).ReturnsAsync(new List<Category>() {child1, child2});
            mockRepository.Setup(c => c.GetCategoryAsync(It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(categoryBeforeTreeUpdate);
            mockRepository.Setup(c => c.GetCategoriesAsync()).ReturnsAsync(new List<Category>());

            var service = new CategoryCommandService(mockRepository.Object);
            
            // Act 
            await service.CreateNewCategoryAsync( "Title", "Description", true, new List<int>() {1,2});

            // Assert
            mockRepository.Verify(c =>  
                c.CreateCategoryAsync(It.Is<Category>(v => 
                    v.ChildrenCategories.FirstOrDefault().Equals(child1)
                    && 
                    v.ChildrenCategories.LastOrDefault().Equals(child2))
                ), Times.Exactly(1));
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async void UpdateExistingCategoryAsync_WhenDescriptionIsNullOrEmpty_ThrowException(string desc)
        {
            // Arrange
            var category = new Category("Test Category", "Test", true, null, null);

            Mock<ICategoryRepository> mockRepository = new Mock<ICategoryRepository>();
            mockRepository.Setup(c => c.GetCategoriesAsync(It.IsAny<IEnumerable<int>>(), It.IsAny<bool>()))
                .ReturnsAsync(new List<Category>());
            mockRepository.Setup(c => c.GetCategoriesAsync()).ReturnsAsync(new List<Category>());

            var service = new CategoryCommandService(mockRepository.Object);

            // Act & Assert
            await Assert.ThrowsAsync<CatalogDomainException>(async () =>
            {
                await service.UpdateExistingCategoryAsync(1 ,"Test Category", desc, true, new List<int>() {1});
            });

        }

        [Fact]
        public async void UpdateExistingCategoryAsync_WhenThereIsChangeInCategoriesTree_UpdateCorrectTheTree()
        {
            // Arrange
            
            var child = new Category("Child Category", "Child", true, null, null);
            
            // Reflection to set private id
            typeof(Category).GetProperty("Id").SetValue(child, 1, null);

            var childrenList = new List<Category>()
            {
                child
            };
            var oldParent = new Category("Old Parent", "Old Description", true, null, childrenList);
            child.SetParentCategory(oldParent);

            var category = new Category("Test Category", "Test", true, null, null);

            Mock<ICategoryRepository> mockRepository = new Mock<ICategoryRepository>();
            
            mockRepository.Setup(c => c.GetCategoriesAsync(It.IsAny<IEnumerable<int>>(), It.IsAny<bool>()))
                .ReturnsAsync(new List<Category>() {child});

            mockRepository.Setup(c => c.GetCategoryAsync(It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(category);
            
            mockRepository.Setup(c => c.UpdateCategoryAsync(category)).Verifiable();

            mockRepository.Setup(c => c.GetCategoriesAsync()).ReturnsAsync(new List<Category>());

            var service = new CategoryCommandService(mockRepository.Object);
            
            // Act 
            await service.UpdateExistingCategoryAsync(1, "Title", "Description", true, new List<int>() {2});

            // Assert
            Assert.Equal(child, category.ChildrenCategories.SingleOrDefault());
            
        }

        [Fact]
        public async void UpdateExistingCategoryAsync_WhenChildrenIdsAreEmpty_ShouldRemoveChildren()
        {
            // Arrange
            
            var child = new Category("Child Category", "Child", true, null, null);
            
            // Reflection to set private id
            typeof(Category).GetProperty("Id").SetValue(child, 1, null);

            var childrenList = new List<Category>() { child };

            var category = new Category("Test", "Description", true, null, childrenList);
            child.SetParentCategory(category);

            Mock<ICategoryRepository> mockRepository = new Mock<ICategoryRepository>();
            
            mockRepository.Setup(c => c.GetCategoriesAsync(It.IsAny<IEnumerable<int>>(), It.IsAny<bool>())).ReturnsAsync(new List<Category>() {});
            mockRepository.Setup(c => c.GetCategoryAsync(It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(category);
            mockRepository.Setup(c => c.GetCategoriesAsync()).ReturnsAsync(new List<Category>());

            var service = new CategoryCommandService(mockRepository.Object);
            
            // Act 
            await service.UpdateExistingCategoryAsync(1, "Title", "Description", true, new List<int>() {2});

            // Assert
            mockRepository.Verify(r => r.UpdateCategoryAsync(It.Is<Category>( c => !c.IsParentCategory() )), Times.Exactly(1));
        }

        [Fact]
        public async void DeleteCategoryAsync_WhenTheDeletedCategoryIsParent_ReleaseTheChildren()
        {
            // Arrange
            var child = new Category("Child Category", "Child", true, null, null);
            var childrenList = new List<Category>() {child};
            var parent = new Category("Wannabe", "Wannabe", true, null, childrenList);
            
            Mock<ICategoryRepository> mockRepository = new Mock<ICategoryRepository>();
            mockRepository.Setup(c => c.GetCategoryAsync(It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(parent);

            var service = new CategoryCommandService(mockRepository.Object);

            // Act
            await service.DeleteCategoryAsync(1);

            // Assert
            Assert.False(child.IsChildCategory());
        }


    }
}
