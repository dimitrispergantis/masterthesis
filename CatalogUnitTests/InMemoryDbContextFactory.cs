﻿using System;
using System.Collections.Generic;
using System.Text;
using Catalog.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Catalog.UnitTests
{
    public static class InMemoryDbContextFactory
    {
        public static CatalogContext GetCatalogDbContext(string databaseName)
        {
            // H In Memory Database θα πρέπει να αρχικοποιείται με διαφορετικό όνομα μέσα σε κάθε unit test
            // διαφορετικά το seed με κοινά δεδομένα θα χτυπάει και τα unit tests failάρουν.
            var options = new DbContextOptionsBuilder<CatalogContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
            var dbContext = new CatalogContext(options);
 
            return dbContext;
        }
    }
}
