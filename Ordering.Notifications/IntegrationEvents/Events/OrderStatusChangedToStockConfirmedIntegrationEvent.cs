﻿using RabbitMQEventBus.Events;

namespace Ordering.Notifications.IntegrationEvents.Events
{
    public class OrderStatusChangedToStockConfirmedIntegrationEvent : IntegrationEvent
    {
        public int OrderId { get; }
        public string OrderStatus { get; }

        public OrderStatusChangedToStockConfirmedIntegrationEvent(int orderId, string orderStatus)
        {
            OrderId = orderId;
            OrderStatus = orderStatus;
        }
    }
}
