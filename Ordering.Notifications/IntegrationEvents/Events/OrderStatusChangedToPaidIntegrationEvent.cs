﻿using RabbitMQEventBus.Events;

namespace Ordering.Notifications.IntegrationEvents.Events
{
    public class OrderStatusChangedToPaidIntegrationEvent : IntegrationEvent
    {
        public int OrderId { get; }
        public string OrderStatus { get; }


        public OrderStatusChangedToPaidIntegrationEvent(int orderId, string orderStatus)
        {
            OrderId = orderId;
            OrderStatus = orderStatus;
        }
    }
}
