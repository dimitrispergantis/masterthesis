﻿using RabbitMQEventBus.Events;

namespace Ordering.Notifications.IntegrationEvents.Events
{
    public class OrderStatusChangedToSubmittedIntegrationEvent : IntegrationEvent
    {
        public int OrderId { get; }
        public string OrderStatus { get; }

        public OrderStatusChangedToSubmittedIntegrationEvent(int orderId, string orderStatus)
        {
            OrderId = orderId;
            OrderStatus = orderStatus;
        }
    }
}
