﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

namespace OcelotApiGateway
{

        public class Startup
        {
            private readonly IConfiguration _cfg;

            public Startup(IConfiguration configuration)
            {
                _cfg = configuration;
            }

            public void ConfigureServices(IServiceCollection services)
            {
                services.AddCors(options =>
                {
                    options.AddPolicy("CorsPolicy",
                        builder => builder
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .SetIsOriginAllowed((host) => true)
                            .AllowCredentials());
                });

                services.AddAuthentication()
                    .AddJwtBearer("IdentityApiKey", x =>
                    {
                        x.Authority = _cfg.GetValue<string>("IdentityUrl");
                        x.RequireHttpsMetadata = false;
                        x.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                        {
                            ValidAudiences = new[] { "catalog" }
                        };
                        x.Events = new Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerEvents()
                        {
                            OnAuthenticationFailed = async ctx =>
                            {
                                int i = 0;
                            },
                            OnTokenValidated = async ctx =>
                            {
                                int i = 0;
                            },

                            OnMessageReceived = async ctx =>
                            {
                                int i = 0;
                            }
                        };
                    });


                services.AddOcelot(_cfg);
            }

            public void Configure(IApplicationBuilder app, IHostingEnvironment env)
            {
                var pathBase = _cfg["PATH_BASE"];

                if (!string.IsNullOrEmpty(pathBase))
                {
                    app.UsePathBase(pathBase);
                }

                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                }

                app.UseCors("CorsPolicy");

                app.UseMvc();

                app.UseOcelot().Wait();
            }
        }
}
